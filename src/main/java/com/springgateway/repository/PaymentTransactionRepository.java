package com.springgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.Invoice;
import com.springgateway.entity.PaymentTransaction;

public interface PaymentTransactionRepository extends JpaRepository<PaymentTransaction,Integer>{

}
