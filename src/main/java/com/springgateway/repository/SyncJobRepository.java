package com.springgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.SyncJob;


public interface SyncJobRepository extends JpaRepository<SyncJob,Integer>{

	SyncJob findById(long jobId);

}
