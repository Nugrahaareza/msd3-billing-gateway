package com.springgateway.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.PaymentMessageAuditTrail;

public interface PaymentMessageAuditTrailRepository extends JpaRepository<PaymentMessageAuditTrail, Integer> {


	

	List<PaymentMessageAuditTrail> findTop10000ByRequestTimestampBetweenOrderByRequestTimestampDesc(
			LocalDateTime startLocalDateTime, LocalDateTime endLocalDateTime);

	

}
