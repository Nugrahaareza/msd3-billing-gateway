package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;

public interface BillingSchemeTemplateChargeRepository extends JpaRepository<BillingSchemeTemplateCharge, Integer> {
	
	List<BillingSchemeTemplateCharge> findBybillingschemetemplateBillingTemplateId(long id);
	
	@Query(value="SELECT * from billing_scheme_template_charge bstc WHERE bstc.billing_template_charge_id=?1 and bstc.billing_template_pk=?2",nativeQuery = true)
	BillingSchemeTemplateCharge findBybillingtemplatechargeidAndbillingtemplatep(int id,String biltmppk);
	
	@Transactional 
	Long deleteBybillingschemetemplate(BillingSchemeTemplate id);

	

	BillingSchemeTemplateCharge findBybillingTemplateChargeId(long billingTemplateChargeId);

	BillingSchemeTemplateCharge findBybillingschemetemplate(BillingSchemeTemplate billlingschemetemplate);

	//void deleteById(long billingTemplateChargeId);


	
	
}
