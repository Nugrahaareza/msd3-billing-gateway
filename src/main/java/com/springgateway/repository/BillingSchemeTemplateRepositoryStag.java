package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.entity.UserEntity;

public interface BillingSchemeTemplateRepositoryStag extends JpaRepository<BillingSchemeTemplateStag, Integer> {

	BillingSchemeTemplateStag findBybillingTemplateId(long id);

	void deleteBybillingTemplateId(long id);

	@Query(value = "SELECT * from billing_scheme_template_stag bsts WHERE bsts.old_billing_id = 0", nativeQuery = true)
	List<BillingSchemeTemplateStag> findAllAndOldBillingIdIsNull();

	BillingSchemeTemplateStag findAllByOldBillingId(long id);

	@Query(value = "SELECT * from billing_scheme_template_stag where is_deleted = 0 and billing_template_approval_status<5", nativeQuery = true)
	List<BillingSchemeTemplateStag> findAllNotDeleted();

}
