package com.springgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springgateway.entity.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, Integer>{
	   
	Organization findByorganizationId(long id);

	List<Organization> findByApprovalStatusNot(int stat);
}
