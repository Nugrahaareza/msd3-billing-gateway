package com.springgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.Organization;
import com.springgateway.entity.OrganizationStag;

public interface OrganizationStagRepository extends JpaRepository<OrganizationStag, Integer>{
	   
	OrganizationStag findByorganizationId(long id);

	@Transactional
	void deleteByorganizationId(long organizationId);

	List<OrganizationStag> findByApprovalStatusNot(int stat);
}
