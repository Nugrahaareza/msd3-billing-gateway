package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;
import com.springgateway.entity.BillingSchemeTemplateStag;

public interface BillingSchemeTemplateChargeRepositoryStag extends JpaRepository<BillingSchemeTemplateChargeStag, Integer> {

	List<BillingSchemeTemplateChargeStag> findBybillingschemetemplatestagBillingTemplateId(long id);

	void deleteBybillingschemetemplatestag(BillingSchemeTemplateStag bss);

	List<BillingSchemeTemplateChargeStag> findAllBybillingschemetemplatestag(BillingSchemeTemplateStag bst);

	
	void deleteByBillingTemplateChargeId(long l);
	
	@Transactional
	BillingSchemeTemplateChargeStag findBybillingTemplateChargeId(long billingTemplateChargeId);
	

	
}
