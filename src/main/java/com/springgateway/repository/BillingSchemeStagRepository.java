package com.springgateway.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeStag;


public interface BillingSchemeStagRepository extends JpaRepository<BillingSchemeStag, Integer> {
	
	
	BillingSchemeStag findAllByid(long id);

	Optional<BillingSchemeStag> findById(long l);

	BillingSchemeStag findAllByOldBillingId(long id);
	

	@Modifying
	@Transactional 
	@Query(value="Delete from  billing_scheme_stag  WHERE id =?1",nativeQuery = true)
	void deleteById(long id);
}
