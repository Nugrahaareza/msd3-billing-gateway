package com.springgateway.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.Pages;

public interface PageRepository extends JpaRepository<Pages, Integer>  {

	Pages findBylabel(String label);

	Optional<Pages> findById(long l);

	Pages findAllByGroupPage(String group);

}
