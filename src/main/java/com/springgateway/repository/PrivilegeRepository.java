package com.springgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.Privilege;
import com.springgateway.entity.Role;

public interface PrivilegeRepository  extends JpaRepository<Privilege, Integer>{

	Privilege findByName(String name);

}
