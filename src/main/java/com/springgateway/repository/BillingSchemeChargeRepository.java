package com.springgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeCharge;



public interface BillingSchemeChargeRepository extends JpaRepository<BillingSchemeCharge, Integer> {
	@Modifying
	@Transactional 
	@Query(value="Delete from  billing_scheme_charge  WHERE billing_pk =?1",nativeQuery = true)
	void deleteByBillingId(long id);

	//@Query(value="Select *  from  billingschemecharge  WHERE billing_pk =?1",nativeQuery = true)
	List<BillingSchemeCharge> findAllBybilllingschemeId(long id);
}
