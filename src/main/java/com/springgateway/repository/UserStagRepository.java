package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.UserEntity;
import com.springgateway.entity.UserStag;
import com.springgateway.model.UserModel;
import com.springgateway.service.UserService;;

public interface UserStagRepository extends JpaRepository<UserStag, Integer> {

	@Query(value = "SELECT u.id AS id , u.username AS username,u.password AS password,u.email AS email,u.wrong_pass_count AS loginAttempt, u.approval_status AS approvalStat , r.role_name as roleName FROM users_roles_stag ur\r\n"
			+ "JOIN user_stag u ON ur.user_id =  u.id\r\n"
			+ "JOIN role r ON  ur.role_id = r.id WHERE u.approval_status !=5 ORDER BY u.id desc", nativeQuery = true)
	List<UserModel> getAllWithRoles();

	UserStag findById(Long id);
	
	@Transactional
	Object deleteById(long id);

	UserStag findByUsername(String username);
	@Transactional
	void deleteByUsername(String username);

}
