package com.springgateway.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.springgateway.entity.JobTrail;

public interface JobTrailRepository extends JpaRepository<JobTrail,Integer> {

	List<JobTrail> findAllByStartDateBetween(LocalDate startLocalDate, LocalDate endLocalDate);

	List<JobTrail> findAllByStartDateBetween(LocalDateTime startLocalDate, LocalDateTime endLocalDate);

}
