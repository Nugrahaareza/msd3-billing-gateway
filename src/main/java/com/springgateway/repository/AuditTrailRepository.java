package com.springgateway.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.AuditTrail;

public interface AuditTrailRepository extends JpaRepository<AuditTrail, Integer> {

	List<AuditTrail> findTop10000ByDateBetweenOrderByDateDesc(LocalDateTime startLocalDateTime,
			LocalDateTime endLocalDateTime);

}
