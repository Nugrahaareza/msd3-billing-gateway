package com.springgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.AuthorityPage;
import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.Role;
import com.springgateway.model.PageRole;

public interface AuthorityPageRepository extends JpaRepository<AuthorityPage, Integer> {

	@Query(value = "SELECT p.label,p.path, p.group_page FROM authority_page ap\r\n"
			+ "JOIN role r ON ap.role_pk = r.id\r\n" + "JOIN pages p ON ap.pages_pk = p.id\r\n"
			+ "WHERE r.id =?1 ", nativeQuery = true)
	List<PageRole> getPagesRole(long role);

	@Query(value = "SELECT r.role_name as roleName,p.id as pageId , p.label,p.path, p.group_page FROM authority_page ap\r\n"
			+ "			JOIN role r ON ap.role_pk = r.id\r\n" + "			JOIN pages p ON ap.pages_pk = p.id\r\n"
			+ "			WHERE r.id=?1 ORDER BY p.label ASC", nativeQuery = true)
	List<PageRole> getRolesDetail(int role);

	@Transactional
	void deleteByrolePk(long id);

	@Query(value = "SELECT r.role_name FROM authority_page ap \r\n" + "JOIN pages p ON ap.pages_pk = p.id \r\n"
			+ "JOIN role r ON ap.role_pk = r.id \r\n" + "WHERE p.group_page=?1", nativeQuery = true)
	List<String> getRolesDetail(String group);

	@Query(value = "SELECT r.comment as comment, r.role_name as roleName,r.old_role_id as oldRoleId,r.approval_status as approvalStatus,p.id as pageId , p.label,p.path, p.group_page FROM authority_page ap\r\n"
			+ "			JOIN role_stag r ON ap.role_pk = r.id\r\n" + "			JOIN pages p ON ap.pages_pk = p.id\r\n"
			+ "			WHERE r.id=?1 ORDER BY p.label ASC", nativeQuery = true)
	List<PageRole> getRoleStagDetail(int id);

	List<AuthorityPage> findByRolePk(long id);

}
