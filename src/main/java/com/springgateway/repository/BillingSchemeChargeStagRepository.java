package com.springgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.BillingSchemeChargeStag;



public interface BillingSchemeChargeStagRepository extends JpaRepository<BillingSchemeChargeStag, Integer> {
	
	@Transactional
	@Modifying
	@Query(value="Delete From billing_scheme_charge_stag  WHERE billing_pk =?1",nativeQuery = true)
	void deleteBybilllingschemeId(long id);

	//@Query(value="Select *  from  billingschemecharge  WHERE billing_pk =?1",nativeQuery = true)
	List<BillingSchemeChargeStag> findAllBybilllingschemeId(long id);
	BillingSchemeChargeStag findBybilllingschemeId(long id);
}
