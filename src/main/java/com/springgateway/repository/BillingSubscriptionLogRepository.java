package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingSubscriptionLog;
import com.springgateway.entity.UserEntity;
import com.springgateway.model.UserModel;
import com.springgateway.service.UserService;;

public interface BillingSubscriptionLogRepository extends JpaRepository<BillingSubscriptionLog , Integer> {


	List<BillingSubscriptionLog> findAllByinvoiceId(Long id);



}
