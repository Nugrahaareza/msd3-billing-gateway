package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.UserEntity;

public interface BillingSchemeTemplateRepository extends JpaRepository<BillingSchemeTemplate,Integer> {
	
	
	Optional<BillingSchemeTemplate> findBybillingTemplateId(long id);
	Long deleteBybillingTemplateId(long l);
	
	@Query(value = "SELECT * from billing_scheme_template where is_deleted = 0 and billing_template_approval_status <5", nativeQuery = true)
	List<BillingSchemeTemplate> findAllNotDeleted();
	
}
