package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.Role;
import com.springgateway.model.RoleModel;

public interface RoleRepository extends JpaRepository<Role, Integer> {
	
	@Transactional
	void deleteById(Long long1);

	Role findByRoleName(String name);
	

	@Query(value = "Select id, role_name as roleName, approval_status as approvalStatus  from role where deleted = 0 and approval_status<5 order by id desc", nativeQuery = true)
	List<RoleModel> getAllRoles();

	Role findById(long id);

}
