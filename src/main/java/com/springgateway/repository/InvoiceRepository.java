package com.springgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice,Integer> {
	
 
	
	Invoice findByorganizationOrganizationId(long id);

	List<Invoice> findByorganizationOrganizationIdAndMonthAndYear(long id,int month,int year);

	Invoice findById(long id);

	List<Invoice> findByMonthAndYear(int month, int year);
}
