package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.Role;
import com.springgateway.entity.RoleStag;
import com.springgateway.model.RoleModel;

public interface RoleStagRepository extends JpaRepository<RoleStag, Integer> {

	RoleStag findById(long id);

	@Transactional
	void deleteById(Long long1);

	@Query(value = "Select id, role_name as roleName, approval_status as approvalStatus  from role_stag where deleted = 0 and approval_status <5 ORDER BY id desc ", nativeQuery = true)
	List<RoleModel> getAllRoles();

}
