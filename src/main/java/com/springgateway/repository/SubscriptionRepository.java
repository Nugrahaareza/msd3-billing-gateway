package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.DetailAppsOrg;
import com.springgateway.entity.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription,Integer> {

	Subscription findAllBysubscriptionId(String id);

	@Query(value="SELECT * from subscription where org_pk=?1",nativeQuery = true)
	List<Subscription> findAllByOrgPk(long id);

	
	//DetailAppsOrg findAllBysubscriptionId(String id);

	Subscription findAllByid(Long id);

	Subscription findAllBybillingschemeStagId(long id);

	Optional<Subscription> findById(String string);

	Subscription findBybillingscheme(BillingScheme bs);

	Subscription findBybillingschemeId(long id);

	Subscription findBybillingschemeStagId(long id);

	//Subscription findAllBysubscriptionId(String subscriptionId);


	

}
