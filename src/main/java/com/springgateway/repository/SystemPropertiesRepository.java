package com.springgateway.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springgateway.entity.Subscription;
import com.springgateway.entity.SystemProperties;

public interface SystemPropertiesRepository extends JpaRepository<SystemProperties,Integer> {

	SystemProperties findById(Long long1);
	
	SystemProperties findByPropertyName(String string);

}
