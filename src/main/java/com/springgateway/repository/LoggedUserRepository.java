package com.springgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.LoggedUser;



public interface LoggedUserRepository extends JpaRepository<LoggedUser, Integer>{

	@Modifying
	@Transactional 
	@Query(value="Delete from  logged_user  WHERE token =?1",nativeQuery = true)
	void deleteByToken(String token);

	@Query(value="Select * from  logged_user  WHERE token =?1",nativeQuery = true)
	LoggedUser findByToken(String token);

}
