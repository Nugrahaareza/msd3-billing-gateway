package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.Role;
import com.springgateway.entity.UserEntity;
import com.springgateway.model.UserModel;
import com.springgateway.model.UserRoles;
import com.springgateway.service.UserService;;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

	@Query(value = "select * from user_entity where username = ?1 &&  password = AES_ENCRYPT(?2, 'secret123')", nativeQuery = true)
	UserEntity findByUsernameandPassword(String username, String Password);

	@Query(value = "select * from user_entity where username = ?1", nativeQuery = true)
	UserEntity findByUsernameOnly(String username);

	UserEntity findByUsername(String username);

	@Transactional
	Long deleteByUsername(String username);

	Optional<UserEntity> findById(Long long1);

	@Query(value = "SELECT * FROM user_entity ue  INNER JOIN role r\r\n" + "ON ue.role_pk = r.id\r\n"
			+ "WHERE r.role_name = \"Checker\"" + "", nativeQuery = true)
	UserEntity findByEmailChecker();

	@Query(value = "SELECT u.id AS id , u.username AS username,u.password AS password,u.email AS email,u.wrong_pass_count AS loginAttempt,u.approval_status AS approvalStat , r.role_name as roleName FROM users_roles ur\r\n"
			+ "JOIN user_entity u ON ur.user_id =  u.id\r\n"
			+ "JOIN role r ON  ur.role_id = r.id WHERE u.approval_status !=5 ORDER BY u.id desc", nativeQuery = true)
	List<UserModel> getAllWithRoles();

	@Transactional
	@Modifying
	@Query(value = "delete from users_roles where user_id=?1", nativeQuery = true)
	void updateJoin(long id);

	UserEntity findByEmail(String email);
	
	@Query(value = "select * from users_roles where role_id=?1", nativeQuery = true)
	List<UserRoles> findByRoles(long id);

}
