package com.springgateway.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeStag;
import com.springgateway.entity.BillingSchemeTemplate;


public interface BillingSchemeRepository extends JpaRepository<BillingScheme, Integer> {
	
	
	BillingScheme findAllByid(long id);

	Optional<BillingScheme> findById(long l);

	
	@Modifying
	@Transactional 
	@Query(value="Delete from  billing_scheme  WHERE id =?1",nativeQuery = true)
	void deleteById(long id);

	List<BillingScheme> findByBillingSchemeTemplate(BillingSchemeTemplate existingTemplate);


}
