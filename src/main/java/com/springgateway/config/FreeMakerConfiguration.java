package com.springgateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

@Configuration
public class FreeMakerConfiguration {
	@Bean
	public FreeMarkerConfigurer freemarkerConfigs() {
		FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
		freeMarkerConfigurer.setTemplateLoaderPath("/WEB-INF/views/mail");
		//freeMarkerConfigurer.setClassForTemplateLoading(this.getClass(), "/templates/");
		return freeMarkerConfigurer;
	}

	@Bean
	public FreeMarkerViewResolver freemarkerViewResolvers() {
		FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
		resolver.setCache(true);
		resolver.setPrefix("");
		resolver.setSuffix(".ftl");
		return resolver;
	}
}
