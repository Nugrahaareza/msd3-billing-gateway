package com.springgateway.model;

public class LoginResponse {
	String jwt;
	Boolean isRandPass;
	String resetPassToken;
	
	public String getJwt() {
		return jwt;
	}
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}
	public Boolean getIsRandPass() {
		return isRandPass;
	}
	public void setIsRandPass(Boolean isRandPass) {
		this.isRandPass = isRandPass;
	}
	public String getResetPassToken() {
		return resetPassToken;
	}
	public void setResetPassToken(String resetPassToken) {
		this.resetPassToken = resetPassToken;
	}
	
	
}
