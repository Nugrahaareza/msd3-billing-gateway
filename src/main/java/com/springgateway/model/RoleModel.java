package com.springgateway.model;

public interface RoleModel {
	public long getId();
	public String getRoleName();
	public int getApprovalStatus();
	public String getDeleted();
}
