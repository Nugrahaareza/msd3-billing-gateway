package com.springgateway.model;

import java.util.List;

import com.springgateway.entity.BillingSchemeTemplateCharge;

public class BillingTemplateRequest {
	
	private long billingTemplateId;
	private String billingTemplateName;
	private String billingTemplateType;
	private List<BillingSchemeTemplateCharge> billSchemeTempCharge;
	
	
	public List<BillingSchemeTemplateCharge> getBillSchemeTempCharge() {
		return billSchemeTempCharge;
	}
	public void setBillSchemeTempCharge(List<BillingSchemeTemplateCharge> billSchemeTempCharge) {
		this.billSchemeTempCharge = billSchemeTempCharge;
	}
	public long getBillingTemplateId() {
		return billingTemplateId;
	}
	public void setBillingTemplateId(long billingTemplateId) {
		this.billingTemplateId = billingTemplateId;
	}
	public String getBillingTemplateName() {
		return billingTemplateName;
	}
	public void setBillingTemplateName(String billingTemplateName) {
		this.billingTemplateName = billingTemplateName;
	}
	public String getBillingTemplateType() {
		return billingTemplateType;
	}
	public void setBillingTemplateType(String billingTemplateType) {
		this.billingTemplateType = billingTemplateType;
	}
	
	
	
}
