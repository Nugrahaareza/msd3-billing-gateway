package com.springgateway.model;

public interface PageRole {
	public String getpageId();
	public String getoldRoleId();
	public String getroleName();
	public String getlabel();
	public String getpath();
	public String getgroup_page();
	public String getApprovalStatus();
	public String getComment();
}
