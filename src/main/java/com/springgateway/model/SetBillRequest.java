package com.springgateway.model;

import java.util.List;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.BillingSchemeChargeStag;
import com.springgateway.entity.BillingSchemeStag;

public class SetBillRequest {
	private String appId;
	private String subsId;
	private BillingScheme billingScheme;
	private SubscriptionStatus  billingStatus;
	private BillingSchemeStag billingSchemeStag;
	private Long billingTempalteId;
	private List<BillingSchemeCharge> billSchemeCharge;
	private List<BillingSchemeChargeStag> billSchemeChargeStag;
	
	
	
	public SubscriptionStatus getBillingStatus() {
		return billingStatus;
	}
	public void setBillingStatus(SubscriptionStatus billingStatus) {
		this.billingStatus = billingStatus;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public String getSubsId() {
		return subsId;
	}
	public void setSubsId(String subsId) {
		this.subsId = subsId;
	}
	public BillingScheme getBillingScheme() {
		return billingScheme;
	}
	public void setBillingScheme(BillingScheme billingScheme) {
		this.billingScheme = billingScheme;
	}
	public List<BillingSchemeCharge> getBillSchemeCharge() {
		return billSchemeCharge;
	}
	public void setBillSchemeCharge(List<BillingSchemeCharge> billSchemeCharge) {
		this.billSchemeCharge = billSchemeCharge;
	}
	public List<BillingSchemeChargeStag> getBillSchemeChargeStag() {
		return billSchemeChargeStag;
	}
	public void setBillSchemeChargeStag(List<BillingSchemeChargeStag> billSchemeChargeStag) {
		this.billSchemeChargeStag = billSchemeChargeStag;
	}
	public BillingSchemeStag getBillingSchemeStag() {
		return billingSchemeStag;
	}
	public void setBillingSchemeStag(BillingSchemeStag billingSchemeStag) {
		this.billingSchemeStag = billingSchemeStag;
	}
	public Long getBillingTempalteId() {
		return billingTempalteId;
	}
	public void setBillingTempalteId(Long billingTempalteId) {
		this.billingTempalteId = billingTempalteId;
	}
	
	
	
}
