package com.springgateway.model;

import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateStag;

public class ApproveBillingModel {
	long stagId;
	long billingId;
	public long getStagId() {
		return stagId;
	}
	public void setStagId(long stagId) {
		this.stagId = stagId;
	}
	public long getBillingId() {
		return billingId;
	}
	public void setBillingId(long billingId) {
		this.billingId = billingId;
	}
}
