package com.springgateway.model;

public interface UserModel {
	public long getId();
	public String getUsername();
	public String getRoleName();
	public String getEmail();
	public int getLoginAttempt();
	public int getApprovalStat();
}
