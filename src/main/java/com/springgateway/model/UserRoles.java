package com.springgateway.model;

public interface UserRoles {
	public Long getUserId();
	public Long getRoleId();
	
}
