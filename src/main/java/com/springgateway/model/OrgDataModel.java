package com.springgateway.model;

import com.springgateway.entity.Organization;
import com.springgateway.entity.OrganizationStag;

public class OrgDataModel {
	Organization org;
	OrganizationStag orgStag;
	public Organization getOrg() {
		return org;
	}
	public void setOrg(Organization org) {
		this.org = org;
	}
	public OrganizationStag getOrgStag() {
		return orgStag;
	}
	public void setOrgStag(OrganizationStag orgStag) {
		this.orgStag = orgStag;
	}
	
	
}
