package com.springgateway.model;

import java.util.List;

import com.springgateway.entity.DetailOrg;
import com.springgateway.entity.Organization;
import com.springgateway.entity.Subscription;

public class getOrgAppModel {
	Organization org;
	List<Subscription> orgDetail;
	public Organization getOrg() {
		return org;
	}
	public void setOrg(Organization org) {
		this.org = org;
	}
	public List<Subscription> getOrgDetail() {
		return orgDetail;
	}
	public void setOrgDetail(List<Subscription> orgDetail) {
		this.orgDetail = orgDetail;
	}
	
}
