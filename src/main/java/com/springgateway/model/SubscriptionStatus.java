package com.springgateway.model;

public enum SubscriptionStatus {
	NOT_SET, SET, SET_NOT_REQUIRED;
}
