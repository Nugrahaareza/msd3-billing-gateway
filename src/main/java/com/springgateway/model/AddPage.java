package com.springgateway.model;


import java.util.List;

import com.springgateway.entity.AuthorityPage;
import com.springgateway.entity.Pages;
import com.springgateway.entity.Role;
import com.springgateway.entity.RoleStag;

public class AddPage {
	RoleStag role;
	List<Pages> page;
	public RoleStag getRole() {
		return role;
	}
	public void setRole(RoleStag role) {
		this.role = role;
	}
	public List<Pages> getPage() {
		return page;
	}
	public void setPage(List<Pages> page) {
		this.page = page;
	}
	
	
	
}
