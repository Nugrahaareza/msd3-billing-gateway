package com.springgateway.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.springgateway.model.ApprovalStatus;
import com.springgateway.model.SubscriptionStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "subscription")
public class Subscription {
	
	


	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	private String subscriptionId;

	@ManyToOne(targetEntity = Organization.class)
	@JoinColumn(name="orgPk", referencedColumnName = "organizationId")
	private Organization organization;

	@ManyToOne(targetEntity = BillingScheme.class, fetch = FetchType.EAGER, cascade = {CascadeType.ALL, CascadeType.MERGE,CascadeType.REFRESH})
	@JoinColumn(name="billing_pk", referencedColumnName = "id")
	private BillingScheme billingscheme;

	@ManyToOne(targetEntity = BillingSchemeStag.class, fetch = FetchType.EAGER, cascade = {CascadeType.ALL, CascadeType.MERGE,CascadeType.REFRESH})
	@JoinColumn(name="billingStag_pk", referencedColumnName = "id")
	private BillingSchemeStag billingschemeStag;
	

	
	
	private String appId;

	private String appName;

	private String productId;

	private String productName;

	private String planId;
	
	private String planName;
	private SubscriptionStatus subscriptionStatus;
	private ApprovalStatus approvalStatus;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	public BillingScheme getBillingscheme() {
		return billingscheme;
	}
	public void setBillingscheme(BillingScheme billingscheme) {
		this.billingscheme = billingscheme;
	}
	public BillingSchemeStag getBillingschemeStag() {
		return billingschemeStag;
	}
	public void setBillingschemeStag(BillingSchemeStag billingschemeStag) {
		this.billingschemeStag = billingschemeStag;
	}
	
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public SubscriptionStatus getSubscriptionStatus() {
		return subscriptionStatus;
	}
	public void setSubscriptionStatus(SubscriptionStatus subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}
	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}


	
	
}
