package com.springgateway.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class PaymentMessageAuditTrail {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String virtualAccountNumber;

	@Column
	private String referenceId;

	@Column
	private String transactionType;

	@Lob
	@Column
	private String requestBody;

	@Lob
	@Column
	private String responseBody;

	@Column
	private String responseCode;

	@Column
	private int httpResponseCode;

	@Column
	private String httpResponseCodeDesc;

	@Column
	private LocalDateTime requestTimestamp;

	@Column
	private LocalDateTime responseTimestamp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVirtualAccountNumber() {
		return virtualAccountNumber;
	}

	public void setVirtualAccountNumber(String virtualAccountNumber) {
		this.virtualAccountNumber = virtualAccountNumber;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(String requestBody) {
		this.requestBody = requestBody;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	@JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	public LocalDateTime getRequestTimestamp() {
		return requestTimestamp;
	}

	public void setRequestTimestamp(LocalDateTime requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	@JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	public LocalDateTime getResponseTimestamp() {
		return responseTimestamp;
	}

	public void setResponseTimestamp(LocalDateTime responseTimestamp) {
		this.responseTimestamp = responseTimestamp;
	}

	public int getHttpResponseCode() {
		return httpResponseCode;
	}

	public void setHttpResponseCode(int httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}

	public String getHttpResponseCodeDesc() {
		return httpResponseCodeDesc;
	}

	public void setHttpResponseCodeDesc(String httpResponseCodeDesc) {
		this.httpResponseCodeDesc = httpResponseCodeDesc;
	}
}