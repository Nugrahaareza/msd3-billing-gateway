package com.springgateway.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BillingSchemeTemplateStag {
	@Id
	@GeneratedValue
    private long billingTemplateId;
	
	@Column
	private String billingSchemeTemplateName;
	
	@Column
	private String billingSchemeTemplateType;
	
	@Column
	private int billingTemplateApprovalStatus;

	@Column(nullable = true)
	private long oldBillingId;
	

	@Column
	private boolean isDeleted;
	
	@Column
	private String comment;


	public long getBillingTemplateId() {
		return billingTemplateId;
	}


	public void setBillingTemplateId(long billingTemplateId) {
		this.billingTemplateId = billingTemplateId;
	}


	public String getBillingSchemeTemplateName() {
		return billingSchemeTemplateName;
	}


	public void setBillingSchemeTemplateName(String billingSchemeTemplateName) {
		this.billingSchemeTemplateName = billingSchemeTemplateName;
	}


	public String getBillingSchemeTemplateType() {
		return billingSchemeTemplateType;
	}


	public void setBillingSchemeTemplateType(String billingSchemeTemplateType) {
		this.billingSchemeTemplateType = billingSchemeTemplateType;
	}


	public int getBillingTemplateApprovalStatus() {
		return billingTemplateApprovalStatus;
	}


	public void setBillingTemplateApprovalStatus(int billingTemplateApprovalStatus) {
		this.billingTemplateApprovalStatus = billingTemplateApprovalStatus;
	}


	public long getOldBillingId() {
		return oldBillingId;
	}


	public void setOldBillingId(long oldBillingId) {
		this.oldBillingId = oldBillingId;
	}


	public boolean isDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
