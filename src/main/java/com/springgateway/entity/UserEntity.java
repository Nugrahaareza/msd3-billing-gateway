package com.springgateway.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.Columns;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author USER
 *
 */
/**
 * @author USER
 *
 */
/**
 * @author USER
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")

public class UserEntity implements Serializable {

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, unique = true)
	private String username;

	@Column
	private String password;

	@Column
	private String email;

	@ToString.Exclude
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Collection<Role> roles;

	private int tokenExpired;
	private boolean isActive;
	private int wrongPassCount;
	
	private boolean isRandPass;
	private String tokenResetPass;
	
	/* 
	 * 0 = newuser, 
	 * 1 = approved,
	 * 2= declined , 
	 * 3= request delete ,
	 * 4= new edit from approved ,
	 * 5=edited wait for approval
	 * 6= token resetPassowrd */
	private int approvalStatus;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}

	public int getTokenExpired() {
		return tokenExpired;
	}

	public void setTokenExpired(int tokenExpired) {
		this.tokenExpired = tokenExpired;
	}

	public Boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public int getWrongPassCount() {
		return wrongPassCount;
	}

	public void setWrongPassCount(int wrongPassCount) {
		this.wrongPassCount = wrongPassCount;
	}

	public int getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(int approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public boolean isRandPass() {
		return isRandPass;
	}

	public void setRandPass(boolean isRandPass) {
		this.isRandPass = isRandPass;
	}

	public String getTokenResetPass() {
		return tokenResetPass;
	}

	public void setTokenResetPass(String tokenResetPass) {
		this.tokenResetPass = tokenResetPass;
	}

	
	
	
	
	
}
