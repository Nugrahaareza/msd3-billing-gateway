package com.springgateway.entity;

public interface DetailAppsOrg {

	public String getapp_name();
	public String getorganization_name();
	public String getplan_name();
	public String getproduct_name();
	public String getscheme_name();
	public String getscheme_type();
	public String getapproval_status();
	public String getscheme_id();

}
