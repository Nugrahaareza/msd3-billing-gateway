package com.springgateway.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class BillingSchemeTemplateChargeStag {
	
	@Id
	@GeneratedValue
    private long billingTemplateChargeId;
	
	@Column
	private int billingTemplateCharge;
	
			@Column
	private String billingTemplateLvl;
	
	@Column(nullable = true)
				private int minCall ;
	
	@Column
	private int maxCall ;
	
	@ManyToOne(targetEntity = BillingSchemeTemplateStag.class,fetch = FetchType.EAGER, cascade = {CascadeType.ALL, CascadeType.MERGE})
	@JoinColumn(name="billingTemplateStagPk", referencedColumnName = "billingTemplateId")
	private BillingSchemeTemplateStag billingschemetemplatestag;

	public long getBillingTemplateChargeId() {
		return billingTemplateChargeId;
	}

	public void setBillingTemplateChargeId(long billingTemplateChargeId) {
		this.billingTemplateChargeId = billingTemplateChargeId;
	}

	public int getBillingTemplateCharge() {
		return billingTemplateCharge;
	}

	public void setBillingTemplateCharge(int billingTemplateCharge) {
		this.billingTemplateCharge = billingTemplateCharge;
	}

	public String getBillingTemplateLvl() {
		return billingTemplateLvl;
	}

	public void setBillingTemplateLvl(String billingTemplateLvl) {
		this.billingTemplateLvl = billingTemplateLvl;
	}

	public int getMinCall() {
		return minCall;
	}

	public void setMinCall(int minCall) {
		this.minCall = minCall;
	}

	public int getMaxCall() {
		return maxCall;
	}

	public void setMaxCall(int maxCall) {
		this.maxCall = maxCall;
	}

	public BillingSchemeTemplateStag getBillingschemetemplate() {
		return billingschemetemplatestag;
	}

	public void setBillingschemetemplate(BillingSchemeTemplateStag billlingschemetemplate) {
		this.billingschemetemplatestag = billlingschemetemplate;
	}

	
	

	

	
}
