package com.springgateway.entity;

public interface DetailOrgApp {
	public String getOrganizationName();
	public String getAppName();
	public String getBillingSchemeid();
	public int getBillingSchemeApprovalStatus();
	public String getBillingSchemeName();
	public String getBillingSchemeType();
	public int getBillingChargeId();
	public int getBillingCharge();
	public String getBillingPk();
	public String getBillingTierLvl();
	public int getMinCall();
	public int getMaxCall();
	public String getplanname();
	public String getplan_id();
}
