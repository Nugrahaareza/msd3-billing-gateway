package com.springgateway.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class BillingSchemeCharge {
	@Id
	@GeneratedValue
	private long id;

	@Column
	private long billingCharge;

	@Column
	private int billingTierLvl;

	@Column(nullable = true)
	private int minCall;

	@Column(nullable = true)
	private int maxCall;

	@ManyToOne(targetEntity = BillingScheme.class, fetch = FetchType.EAGER, cascade = { CascadeType.ALL,
			CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "billing_pk", referencedColumnName = "id")
	private BillingScheme billlingscheme;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBillingCharge() {
		return billingCharge;
	}

	public void setBillingCharge(long billingCharge) {
		this.billingCharge = billingCharge;
	}

	public int getBillingTierLvl() {
		return billingTierLvl;
	}

	public void setBillingTierLvl(int billingTierLvl) {
		this.billingTierLvl = billingTierLvl;
	}

	public int getMinCall() {
		return minCall;
	}

	public void setMinCall(int minCall) {
		this.minCall = minCall;
	}

	public int getMaxCall() {
		return maxCall;
	}

	public void setMaxCall(int maxCall) {
		this.maxCall = maxCall;
	}

	public BillingScheme getBilllingscheme() {
		return billlingscheme;
	}

	public void setBilllingscheme(BillingScheme billlingscheme) {
		this.billlingscheme = billlingscheme;
	}

}
