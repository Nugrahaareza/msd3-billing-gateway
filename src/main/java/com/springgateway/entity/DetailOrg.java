package com.springgateway.entity;

public interface DetailOrg {

	public String getAppsId();
	public String getAppsName();
	public String getSubscriptionapproval();
	public String getProductTitle();
	public String getPlantitle();
	public String getBillingName();
	public String getBillingSchemeType();
	public int getApprovalStatus();
	public String getBillingSchemeId();
	
}
