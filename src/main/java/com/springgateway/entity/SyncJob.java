package com.springgateway.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "syncJob")
public class SyncJob {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String jobName;
	
	@Column
	private String jobUrl;
	
	
	@Lob
	@Column
	private String params;

	@Lob
	@Column
	private String jobDescription;

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getJobName() {
		return jobName;
	}


	public void setJobName(String jobName) {
		this.jobName = jobName;
	}


	public String getJobUrl() {
		return jobUrl;
	}


	public void setJobUrl(String jobUrl) {
		this.jobUrl = jobUrl;
	}


	public String getParams() {
		return params;
	}


	public void setParams(String params) {
		this.params = params;
	}


	public String getJobDescription() {
		return jobDescription;
	}


	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

}
