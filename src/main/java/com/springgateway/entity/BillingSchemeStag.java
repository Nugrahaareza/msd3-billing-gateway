package com.springgateway.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class BillingSchemeStag {
	
	@Id
	@GeneratedValue
    private long id;
	
	@Column
	private String schemeName;
	
	@Column
	private String schemeType;
	
	@Column
	private int approvalStatus;
	
	@Column(nullable = true)
	private long oldBillingId;
	

	@Column
	private String comment;
	
	@ManyToOne(targetEntity = BillingSchemeTemplate.class, fetch = FetchType.EAGER, cascade = {CascadeType.ALL, CascadeType.MERGE,CascadeType.REFRESH})
	@JoinColumn(name="billing_template_pk", referencedColumnName = "billingTemplateId")
	private  BillingSchemeTemplate billingSchemeTemplate;
	
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getOldBillingId() {
		return oldBillingId;
	}

	public void setOldBillingId(long oldBillingId) {
		this.oldBillingId = oldBillingId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public String getSchemeType() {
		return schemeType;
	}

	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	public int getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(int approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public BillingSchemeTemplate getBillingSchemeTemplate() {
		return billingSchemeTemplate;
	}

	public void setBillingSchemeTemplate(BillingSchemeTemplate billingSchemeTemplate) {
		this.billingSchemeTemplate = billingSchemeTemplate;
	}

	
	

	
	
	
}
