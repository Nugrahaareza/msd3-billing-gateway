package com.springgateway.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AuditTrail {
	
	@Id
    @GeneratedValue
    private int id;
	
	@Column
	private String user;
	
	@Column
	private String action;
	
	@Column
	private String description;

	@Column
	@Lob
	private String oldData;
	
	@Column
	@Lob
	private String newData;
	@Column
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss") 
	private LocalDateTime date;
	
	@JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss")
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	
	public void setDescription(String descripion) {
		this.description = descripion;
	}

	public void CreateAudit(String user,String action, String description,LocalDateTime date) {
		this.user = user;
		this.action = action;
		this.description = description;
		this.date = date;
	}
	public void CreateAuditScheme(String user,String action, String description,LocalDateTime date,String oldData, String newData) {
		this.user = user;
		this.action = action;
		this.description = description;
		this.date = date;
		this.oldData = oldData;
		this.newData = newData;
	}
	public String getOldData() {
		return oldData;
	}

	public void setOldData(String oldData) {
		this.oldData = oldData;
	}

	public String getNewData() {
		return newData;
	}

	public void setNewData(String newData) {
		this.newData = newData;
	}
}

	
