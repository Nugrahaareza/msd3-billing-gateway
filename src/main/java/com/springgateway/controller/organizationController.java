package com.springgateway.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.BillingSchemeChargeStag;
import com.springgateway.entity.BillingSchemeStag;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.entity.DetailAppsOrg;
import com.springgateway.entity.DetailOrg;
import com.springgateway.entity.DetailOrgApp;
import com.springgateway.entity.Organization;
import com.springgateway.entity.OrganizationStag;
import com.springgateway.entity.Subscription;
import com.springgateway.entity.UserEntity;
import com.springgateway.mail.EmailServiceImpl;
import com.springgateway.model.ApprovalStatus;
import com.springgateway.model.OrgDataModel;
import com.springgateway.model.SetBillRequest;
import com.springgateway.model.SubscriptionStatus;
import com.springgateway.model.getOrgAppModel;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.BillingSchemeChargeService;
import com.springgateway.service.BillingSchemeChargeStagService;
import com.springgateway.service.BillingSchemeService;
import com.springgateway.service.BillingSchemeStagService;
import com.springgateway.service.BillingSchemeTemplateChargeService;
import com.springgateway.service.BillingSchemeTemplateService;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.OrganizationService;
import com.springgateway.service.OrganizationStagService;
import com.springgateway.service.SubscriptionService;
import com.springgateway.service.UserService;
import com.springgateway.util.Authority;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.ResponsFormatter;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/organization")
public class organizationController {

	ResponsFormatter response = new ResponsFormatter();
	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private OrganizationStagService organizationStagService;

	@Autowired
	private BillingSchemeService billSchemeService;

	@Autowired
	private BillingSchemeChargeService billSchemeChargeService;

	@Autowired
	private BillingSchemeStagService billSchemeStagService;

	@Autowired
	private BillingSchemeChargeStagService billSchemeChargeStagService;

	@Autowired
	private BillingSchemeTemplateService billTemplateService;

	@Autowired
	private BillingSchemeTemplateChargeService billTemplateChargeService;

	@Autowired
	private SubscriptionService subsService;

	@Autowired
	private AuditTrailService auditTrailService;

	@Autowired
	Authority authUtil;

	@Autowired
	private RefreshTokenGenerator rTokGen;

	public final String API_GROUP = "/api/organization";

	@CrossOrigin
	@GetMapping(path = "/getallOrg", produces = { "application/json" })
	public ResponseEntity<?> getAllOrg(HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access organization list page", date);
		auditTrailService.saveData(auditData);

		try {
			List<?> resp = null;
			List<Organization> org = organizationService.getAll(5);
			List<OrganizationStag> orgStag = organizationStagService.getAll(5);
			resp = Stream.concat(orgStag.stream(), org.stream()).collect(Collectors.toList());

			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/editOrg", produces = { "application/json" })
	public ResponseEntity<?> editOrg(@RequestBody Organization orgReq, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		System.out.println(orgReq.getOrganizationId());
		Organization org = organizationService.getAllById(orgReq.getOrganizationId());
		if (org != null) {
			OrganizationStag orgStag = new OrganizationStag();
			orgStag.setApprovalStatus(0);
			orgStag.setOrganizationId(org.getOrganizationId());
			orgStag.setNotificationEmail(orgReq.getNotificationEmail());
			orgStag.setNotificationName(orgReq.getNotificationName());
			orgStag.setOrganizationName(org.getOrganizationName());
			orgStag.setOrganizationStatus(org.getOrganizationStatus());
			orgStag.setOrganizationUrl(org.getOrganizationUrl());
			orgStag.setOrgApimId(org.getOrgApimId());
			orgStag.setOwnerEmail(org.getOwnerEmail());
			orgStag.setOwnerUsername(org.getOwnerUsername());
			orgStag.setOldOrgId(org.getOrganizationId());
			organizationStagService.saveData(orgStag);
			org.setApprovalStatus(5);
			organizationService.updateStatus(org);
			auditData.CreateAuditScheme(username, " Edit", username + " Edit Organization " + org.getOrganizationName(),
					date, String.valueOf(org), String.valueOf(orgStag));
			auditTrailService.saveData(auditData);
		} else if (org == null) {
			System.out.println("lawkdnfLKSNDAF;AOFDJSKLSDNF");
			System.out.println(orgReq.getNotificationName());
			OrganizationStag orgStag = organizationStagService.getAllById(orgReq.getOrganizationId());
			OrganizationStag oldOrgStag = orgStag;
			orgStag.setApprovalStatus(0);
			orgStag.setNotificationEmail(orgReq.getNotificationEmail());
			orgStag.setNotificationName(orgReq.getNotificationName());
			organizationStagService.updateStagData(orgStag);
			auditData.CreateAuditScheme(username, " Edit",
					username + " Edit Organization " + oldOrgStag.getOrganizationName(), date,
					String.valueOf(oldOrgStag), String.valueOf(orgStag));
			auditTrailService.saveData(auditData);
		}
		return ResponseEntity.ok().headers(header).body("ok");

	}

	@CrossOrigin
	@PostMapping(path = "/approveOrg", produces = { "application/json" })
	public ResponseEntity<?> aprroveOrg(@RequestBody Organization orgReq, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {
			OrganizationStag orgStag = organizationStagService.getAllById(orgReq.getOrganizationId());
			Organization org = new Organization();
			Organization oldOrg = org;
			if (orgStag.getOldOrgId() != 0) {
				org = organizationService.getAllById(orgStag.getOldOrgId());
				org.setNotificationEmail(orgStag.getNotificationEmail());
				org.setNotificationName(orgStag.getNotificationName());
				organizationService.approveData(org);
				organizationStagService.delete(orgStag);

			}
			auditData.CreateAuditScheme(username, " Approve",
					username + " Approve edit Organization " + org.getOrganizationName(), date, String.valueOf(oldOrg),
					String.valueOf(org));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/declineOrg", produces = { "application/json" })
	public ResponseEntity<?> declineOrg(@RequestBody Organization orgReq, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {
			OrganizationStag orgStag = organizationStagService.getAllById(orgReq.getOrganizationId());
			Organization org = new Organization();
			Organization oldOrg = org;
			if (orgStag.getOldOrgId() != 0) {
				org = organizationService.getAllById(orgStag.getOldOrgId());
				org.setApprovalStatus(1);
				organizationService.updateStatus(org);
				organizationStagService.delete(orgStag);

			}
			auditData.CreateAuditScheme(username, " Decline",
					username + " Decline edit Organization " + org.getOrganizationName(), date, String.valueOf(oldOrg),
					String.valueOf(org));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/askRevisionOrg", produces = { "application/json" })
	public ResponseEntity<?> askForRevisionOrg(@RequestBody OrganizationStag orgReq, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {
			OrganizationStag orgStag = organizationStagService.getAllById(orgReq.getOrganizationId());
			OrganizationStag oldOrg = orgStag;

			orgStag.setComment(orgReq.getComment());
			orgStag.setApprovalStatus(4);
			organizationStagService.updateData(orgStag);
			auditData.CreateAuditScheme(username, "Ask For Revision",
					username + " Ask for revision for organization " + orgStag.getOrganizationName(), date,
					String.valueOf(oldOrg), String.valueOf(orgStag));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@GetMapping(path = "/getorg/{id}", produces = { "application/json" })
	public ResponseEntity<?> getOrg(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access organization list page", date);
		auditTrailService.saveData(auditData);

		try {
			OrgDataModel resp = new OrgDataModel();
			Organization org = organizationService.getAllById(id);
			OrganizationStag orgStag = organizationStagService.getAllById(id);
			if (orgStag != null) {
				if (orgStag.getOldOrgId() != 0) {
					org = organizationService.getAllById(orgStag.getOldOrgId());
				}
			}

			resp.setOrg(org);
			resp.setOrgStag(orgStag);
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/getorgapp/{id}", produces = { "application/json" })
	public ResponseEntity<?> getOrgApp(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access organization list page", date);
		auditTrailService.saveData(auditData);

		try {
			getOrgAppModel resp = new getOrgAppModel();
			Organization organization = organizationService.getAllById(id);
			List<Subscription> subData = subsService.getAllByOrgId(id);
			if (organization == null) {
				OrganizationStag orgStag = organizationStagService.getAllById(id);
				organization = organizationService.getAllById(orgStag.getOldOrgId());
				subData = subsService.getAllByOrgId(orgStag.getOldOrgId());
			}
			resp.setOrg(organization);

			resp.setOrgDetail(subData);
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(path = "/getorgappdetail/{id}", produces = { "application/json" })
	public ResponseEntity<?> getOrgAppDetail(@PathVariable String id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Organization details page", date);
		auditTrailService.saveData(auditData);

		try {
			Subscription resp = subsService.getAppDetail(id);
			System.out.println(resp);
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/getBillingDetails/{id}", produces = { "application/json" })
	public ResponseEntity<?> getBillingDetails(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Organization details page", date);
		auditTrailService.saveData(auditData);
		try {
			List<BillingSchemeCharge> resp = billSchemeChargeService.getAllBySchemeId(id);
			if (resp.size() < 1) {
				List<BillingSchemeChargeStag> respStag = billSchemeChargeStagService.getAllBySchemeId(id);
				if (respStag.size() < 1) {
					BillingScheme bst = billSchemeService.getAllById(id);
					BillingSchemeTemplate bstm = billTemplateService
							.getAllById(bst.getBillingSchemeTemplate().getBillingTemplateId());
					List<BillingSchemeTemplateCharge> bstmc = billTemplateChargeService
							.getAllByPKId(bstm.getBillingTemplateId());
					return ResponseEntity.ok(bstmc);
				}
				return ResponseEntity.ok().headers(header).body(respStag);
			}
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/getBillingStagDetails/{id}", produces = { "application/json" })
	public ResponseEntity<?> getBillingStagDetails(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Organization details page", date);
		auditTrailService.saveData(auditData);
		try {
			List<BillingSchemeChargeStag> resp = billSchemeChargeStagService.getAllBySchemeId(id);

			if (resp.size() < 1) {
				System.out.println(resp);
				BillingSchemeStag bst = billSchemeStagService.getAllById(id);
				System.out.println(bst == null);
				System.out.println("1!!!!");
				if (bst.getBillingSchemeTemplate() == null) {
					return ResponseEntity.ok(resp);
				}
				BillingSchemeTemplate bstm = billTemplateService
						.getAllById(bst.getBillingSchemeTemplate().getBillingTemplateId());
				System.out.println(bstm);

				List<BillingSchemeTemplateCharge> bstmc = billTemplateChargeService
						.getAllByPKId(bstm.getBillingTemplateId());
				return ResponseEntity.ok().headers(header).body(bstmc);
			}
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("null")
	@CrossOrigin
	@PostMapping(path = "/setScheme", produces = { "application/json" })
	public ResponseEntity<?> setBill(@RequestBody(required = false) SetBillRequest req, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {

			if (req.getBillingStatus() == SubscriptionStatus.SET_NOT_REQUIRED
					|| req.getBillingStatus() == SubscriptionStatus.NOT_SET) {
				Subscription existingSubs = subsService.getAllBySubsId(req.getSubsId());
				existingSubs.setSubscriptionStatus(req.getBillingStatus());
				existingSubs.setApprovalStatus(ApprovalStatus.WAITING_APRROVAL);
				subsService.updateSubsStatus(existingSubs);
				return ResponseEntity.ok().headers(header).body("ok");
			} else {
				if (req.getBillingTempalteId() != null) {
					BillingSchemeStag bsst = new BillingSchemeStag();
					Subscription existingSubs = subsService.getAllBySubsId(req.getSubsId());

					BillingSchemeTemplate bst = billTemplateService.getAllById(req.getBillingTempalteId());
					bsst.setBillingSchemeTemplate(bst);
					bsst.setApprovalStatus(0);
					bsst.setSchemeType(bst.getBillingSchemeTemplateType());
					SetBillRequest newData = req;
					newData.setBillingSchemeStag(bsst);
					Subscription org = subsService.addBillSchemeStag(newData);

					auditData.CreateAuditScheme(username, " Set Scheme", username + " Add Scheme", date,
							String.valueOf(req), null);
					auditTrailService.saveData(auditData);
					return ResponseEntity.ok().headers(header).body(bsst);
				} else {
					BillingSchemeStag bst = billSchemeStagService.getAllStagById(req.getBillingSchemeStag().getId());
					Subscription org = subsService.addBillSchemeStag(req);
					List<BillingSchemeChargeStag> bsc = req.getBillSchemeChargeStag();
					for (BillingSchemeChargeStag bs : bsc) {
						bs.setBilllingscheme(org.getBillingschemeStag());
					}
					if (bst != null) {
						billSchemeChargeStagService.deleteByBillingId(bst.getId());
						billSchemeStagService.deleteById(bst.getId());
						billSchemeChargeStagService.saveDatas(bsc);
						auditData.CreateAuditScheme(username, " Add or edit Scheme", username + " Add or edit Scheme",
								date, String.valueOf(bst), String.valueOf(bsc));
					} else {

						billSchemeChargeStagService.saveDatas(bsc);
						auditData.CreateAuditScheme(username, " Add or edit Scheme", username + " Add or edit Scheme",
								date, String.valueOf(req.getBillingScheme()), String.valueOf(bsc));
					}
					if (org.getBillingscheme() != null) {

						BillingScheme bilsch = org.getBillingscheme();
						billSchemeService.unapprove(bilsch);
						List<BillingSchemeCharge> bilschc = billSchemeChargeService
								.getAllBySchemeId(org.getBillingscheme().getId());
						auditData.setOldData(String.valueOf(bilschc));
					}

					auditTrailService.saveData(auditData);
					return ResponseEntity.ok().headers(header).body("ok");
				}
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/editAppScheme", produces = { "application/json" })
	public ResponseEntity<?> editAppScheme(@RequestBody SetBillRequest req, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Edit App Scheme", username + " Organization details page", date);
		auditTrailService.saveData(auditData);

		if (req.getBillingTempalteId() == null) {
			BillingSchemeStag bst = billSchemeStagService.getAllById(req.getBillingSchemeStag().getId());
			List<BillingSchemeChargeStag> lbsc = new ArrayList();
			if (req.getBillingSchemeStag() != null) {

				lbsc = billSchemeChargeStagService.getAllBySchemeId(req.getBillingSchemeStag().getId());
			}
			billSchemeChargeStagService.deleteByBillingId(req);

			List<BillingSchemeChargeStag> bsc = req.getBillSchemeChargeStag();
			for (BillingSchemeChargeStag bs : bsc) {
				bs.setBilllingscheme(bst);
			}

			billSchemeStagService.updateStatus(req.getBillingSchemeStag());
			billSchemeChargeStagService.saveDatas(bsc);

			auditData.CreateAuditScheme(username, "Edit Scheme", username + " Edit Scheme", date, String.valueOf(lbsc),
					String.valueOf(bsc));
		} else {

			BillingSchemeTemplate bst = billTemplateService.getAllById(req.getBillingTempalteId());

			BillingSchemeStag bss = billSchemeStagService.getAllById(req.getBillingSchemeStag().getId());
			bss.setBillingSchemeTemplate(bst);
			billSchemeStagService.updateResource(bss);

			billSchemeChargeStagService.deleteByBillingSchemeId(bss.getId());

		}
		auditTrailService.saveData(auditData);
		return ResponseEntity.ok().headers(header).body("ok");

	}

	@CrossOrigin
	@PostMapping(path = "/approve", produces = { "application/json" })
	public ResponseEntity<?> approveSchemeCharge(@RequestBody BillingScheme data,
			@RequestParam(required = false) String subsId, HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Approve", username + "Approving App Scheme", date);
		auditTrailService.saveData(auditData);

		try {

			System.out.println(data);
			BillingScheme nbs = new BillingScheme();
			nbs = (BillingScheme) data;
			List<BillingSchemeCharge> bsc = new ArrayList<BillingSchemeCharge>();
			List<BillingSchemeChargeStag> lbscs = billSchemeChargeStagService.getAllBySchemeId(data.getId());
			System.out.println("0----------------");
			System.out.println(subsId);
			if (data.getApprovalStatus() == 1) {
				BillingSchemeStag bst = billSchemeStagService.getAllById(data.getId());
				Subscription subs = subsService.getAllByBillingSchemeOrBillingSchemeStag(data.getId());
				if (subs == null) {
					subs= subsService.getAllBySubsId(subsId);
					subs.setApprovalStatus(ApprovalStatus.APPROVED);
					subsService.updateApproval(subs);
				} else if (bst != null && bst.getBillingSchemeTemplate() != null) {
					data.setApprovalStatus(1);
					data.setBillingSchemeTemplate(bst.getBillingSchemeTemplate());
					BillingScheme bs = billSchemeService.saveData(data);
					subsService.moveStagToScheme(data.getId(), bs);
				} else {
					data.setApprovalStatus(1);
					BillingScheme bs = billSchemeService.saveData(data);
					for (BillingSchemeChargeStag bscsl : lbscs) {
						BillingSchemeCharge nbsc = new BillingSchemeCharge();
						nbsc.setId(bscsl.getId());
						nbsc.setBillingCharge(bscsl.getBillingCharge());
						nbsc.setBillingTierLvl(bscsl.getBillingTierLvl());
						nbsc.setBilllingscheme(bs);
						nbsc.setMaxCall(bscsl.getMaxCall());
						nbsc.setMinCall(bscsl.getMinCall());
						bsc.add(nbsc);
					}
					billSchemeChargeService.saveDatas(bsc);
					subsService.moveStagToScheme(data.getId(), bs);
					billSchemeChargeStagService.deleteByBillingSchemeId(data.getId());
					billSchemeStagService.deleteByBillingSchemeId(data.getId());
				}
				auditData.CreateAuditScheme(username, "Approve", username + " Approve", date, String.valueOf(lbscs),
						String.valueOf(bsc));
				auditTrailService.saveData(auditData);
			} else if (data.getApprovalStatus() == 3) {
				auditData.CreateAuditScheme(username, "Decline", username + " Decline Scheme", date,
						String.valueOf(lbscs), String.valueOf(data));
				Subscription subs = subsService.getAllByBillingSchemeOrBillingSchemeStag(data.getId());
				if (subs == null) {
					subs= subsService.getAllBySubsId(subsId);
				
					subs.setApprovalStatus(null);
					subsService.updateApproval(subs);
				}else {
				auditTrailService.saveData(auditData);
				subsService.decline(data);
				}

			} else if (data.getApprovalStatus() == 4) {
				BillingSchemeStag bst = billSchemeStagService.getAllById(data.getId());
				if (bst.getBillingSchemeTemplate() == null) {
					List<BillingSchemeChargeStag> bsct = billSchemeChargeStagService.getAllBySchemeId(data.getId());

					bst.setId(data.getId());
					bst.setApprovalStatus(data.getApprovalStatus());
					bst.setComment(data.getComment());
					bst.setSchemeType(data.getSchemeType());
					bst.setBillingSchemeTemplate(data.getBillingSchemeTemplate());
				} else {

					bst.setComment(data.getComment());
					bst.setApprovalStatus(4);
				}
				auditData.CreateAuditScheme(username, "Ask For Revision", username + " Ask revision Scheme", date, null,
						String.valueOf(bst) + String.valueOf(bsc));
				auditTrailService.saveData(auditData);
				billSchemeStagService.updateStatus(bst);
				subsService.decline(data);
			}

			return ResponseEntity.ok().headers(header).body("ok");

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/approveedit", produces = { "application/json" })
	public ResponseEntity<?> approveSchemeCharge(@RequestBody Subscription data, HttpServletRequest request)
			throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		List<BillingSchemeCharge> oldData = new ArrayList<BillingSchemeCharge>();
		List<BillingSchemeChargeStag> newData = new ArrayList<BillingSchemeChargeStag>();
		oldData = billSchemeChargeService.getAllBySchemeId(data.getBillingscheme().getId());
		newData = billSchemeChargeStagService.getAllBySchemeId(data.getBillingschemeStag().getId());

		BillingSchemeStag newBillData = billSchemeStagService.getAllById(data.getBillingschemeStag().getId());
		BillingScheme oldBillData = billSchemeService.getAllById(data.getBillingscheme().getId());
		BillingScheme bs = new BillingScheme();

		List<BillingSchemeCharge> lbsc = new ArrayList<BillingSchemeCharge>();

		if (data.getBillingschemeStag().getApprovalStatus() == 1) {
			if (newBillData.getBillingSchemeTemplate() != null) {
				bs.setApprovalStatus(1);
				bs.setSchemeName(newBillData.getSchemeName());
				bs.setSchemeType(newBillData.getSchemeType());
				bs.setBillingSchemeTemplate(newBillData.getBillingSchemeTemplate());
				bs = billSchemeService.saveData(bs);

				Subscription subs = subsService.getAllBySubsId(data.getSubscriptionId());
				subs.setBillingschemeStag(null);
				subs.setBillingscheme(bs);
				subs = subsService.updateAllData(subs);
				billSchemeStagService.deleteById(newBillData.getId());
				// billSchemeService.deleteById(oldBillData.getId());

				auditData.CreateAuditScheme(username, "Approve", username + " Approve Edited Scheme", date,
						String.valueOf(oldData), String.valueOf(newData));
			} else {
				BillingSchemeStag bss = newData.get(0).getBilllingscheme();
				if (bss.getBillingSchemeTemplate() != null) {
					bs.setApprovalStatus(1);
					bs.setSchemeName(bss.getSchemeName());
					bs.setSchemeType(bss.getSchemeType());
					bs.setBillingSchemeTemplate(bss.getBillingSchemeTemplate());
					bs = billSchemeService.saveData(bs);

					Subscription subs = subsService.getAllBySubsId(data.getSubscriptionId());
					subs.setBillingschemeStag(null);
					subs.setBillingscheme(bs);
					subs = subsService.updateAllData(subs);

					billSchemeService.deleteById(oldBillData.getId());

					billSchemeStagService.deleteById(newBillData.getId());
					auditData.CreateAuditScheme(username, "Approve", username + " Approve Edited Scheme", date,
							String.valueOf(oldData), String.valueOf(newData));

				} else {
					bs.setApprovalStatus(1);
					bs.setSchemeName(bss.getSchemeName());
					bs.setSchemeType(bss.getSchemeType());
					bs = billSchemeService.saveData(bs);

					for (BillingSchemeChargeStag bscs : newData) {
						BillingSchemeCharge bsc = new BillingSchemeCharge();
						bsc.setBillingCharge(bscs.getBillingCharge());
						bsc.setBillingTierLvl(bscs.getBillingTierLvl());
						bsc.setBilllingscheme(bs);
						bsc.setMaxCall(bscs.getMaxCall());
						bsc.setMinCall(bscs.getMinCall());
						lbsc.add(bsc);
					}

					billSchemeChargeService.saveDatas(lbsc);

					Subscription subs = subsService.getAllBySubsId(data.getSubscriptionId());
					subs.setBillingschemeStag(null);
					subs.setBillingscheme(bs);
					subs = subsService.updateAllData(subs);
					Long oldId;
					try {
						oldId = oldData.get(0).getBilllingscheme().getId();
					} catch (Exception e) {
						oldId = null;
					}
					Long newId = newData.get(0).getBilllingscheme().getId();

					System.out.println(oldId);
					System.out.println(newId);
					if (oldId != null) {
						billSchemeChargeService.deleteByBillingId(oldData.get(0).getBilllingscheme().getId());
						billSchemeService.deleteById(oldData.get(0).getBilllingscheme().getId());
					}
					if (newId != null) {
						billSchemeChargeStagService.deleteByBillingId(newData.get(0).getBilllingscheme().getId());
						billSchemeStagService.deleteById(newData.get(0).getBilllingscheme().getId());
					}
				}
				auditData.CreateAuditScheme(username, "Approve", username + " Approve Edited Scheme", date,
						String.valueOf(oldData), String.valueOf(newData));
			}
			auditTrailService.saveData(auditData);
		} else if (data.getBillingschemeStag().getApprovalStatus() == 4) {

			billSchemeStagService.updateStatus(data.getBillingschemeStag());
			auditData.CreateAuditScheme(username, "Ask For Revision", username + " Ask For Revision Edited Scheme",
					date, String.valueOf(oldData), String.valueOf(newData));
			auditTrailService.saveData(auditData);
		} else if (data.getBillingschemeStag().getApprovalStatus() == 3) {
			subsService.removeBillStagService(data.getSubscriptionId());
			billSchemeStagService.decline(data.getBillingschemeStag());
			// billSchemeStagService.updateStatus(data.getBillingschemeStag());
			// billSchemeService.approve(data.getBillingscheme());
			System.out.print("paosdpao");
			System.out.print(data.getBillingschemeStag().getId());
			// System.out.print(data.getBillingschemeStag());
			auditData.CreateAuditScheme(username, "Decline", username + " Decline Edited Scheme", date,
					String.valueOf(oldData), String.valueOf(newData));
			auditTrailService.saveData(auditData);
		}

		return ResponseEntity.ok().headers(header).body("ok");

	}

}
