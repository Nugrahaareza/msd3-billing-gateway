package com.springgateway.controller;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.Role;
import com.springgateway.entity.RoleStag;
import com.springgateway.entity.UserEntity;
import com.springgateway.entity.UserStag;
import com.springgateway.mail.EmailServiceImpl;
import com.springgateway.model.RevisionReqModel;
import com.springgateway.model.UserModel;
import com.springgateway.service.AuditTrailService;

import com.springgateway.service.UserService;
import com.springgateway.service.UserStagService;
import com.springgateway.util.Authority;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/user")
public class userController {

	private static final Logger LOGGER = LogManager.getLogger("userController-log");

	@Autowired
	private UserService service;

	@Autowired
	private UserStagService stagService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuditTrailService auditTrailService;

	@Autowired
	private EmailServiceImpl mailSender;

	@Autowired
	Authority authUtil;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private RefreshTokenGenerator rTokGen;

	LocalDateTime date = LocalDateTime.now();

	public final String API_GROUP = "/api/user";

	@CrossOrigin
	@PostMapping(path = "/regis", produces = { "application/json" })
	public ResponseEntity<?> Registration(@RequestBody UserStag user, HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);

		}
		String username = tf.getUsername(request);

		try {
			AuditTrail auditData = new AuditTrail();

			UserEntity existingUser = service.findByUsername(user.getUsername());
			if (existingUser != null) {
				return new ResponseEntity<String>("Username is already used", HttpStatus.BAD_REQUEST);
			}

			user.setPassword(passwordEncoder.encode(user.getPassword()));
			LOGGER.info("New user data : " + String.valueOf(user));
			UserStag mUser = stagService.saveData(user);

			auditData.CreateAuditScheme(username, " Add new user", mUser.getUsername() + " Add new user", date, null,
					String.valueOf(mUser));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			String err = String.valueOf(e);
			System.out.println(err);
			if (err.contains("unique result")) {
				return new ResponseEntity<String>("Rolename is already used", HttpStatus.BAD_REQUEST);
			}
			LOGGER.info("Error : " + e);
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@GetMapping(path = "/listUser", produces = { "application/json" })
	public ResponseEntity<?> ListUser(HttpServletRequest request) {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);

		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access user list", date);
		auditTrailService.saveData(auditData);
		try {
			List<UserModel> mUser = service.getAllWithRole();
			List<UserModel> userStag = stagService.getAllWithRole();
			userStag.addAll(mUser);
			return ResponseEntity.ok().headers(header).body(userStag);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/{id}", produces = { "application/json" })
	public ResponseEntity<?> getById(@PathVariable int id, HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);

		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access user details page", date);
		auditTrailService.saveData(auditData);

		try {
			UserEntity mUser = service.getAllById(Long.valueOf(id));
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/stag/{id}", produces = { "application/json" })
	public ResponseEntity<?> getStagById(@PathVariable int id, HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);

		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access user details page", date);
		auditTrailService.saveData(auditData);

		try {
			UserStag mUser = stagService.getAllById(Long.valueOf(id));
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/approve/{id}", produces = { "application/json" })
	public ResponseEntity<?> approveUser(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String username = tf.getUsername(request);
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);

		}
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		UserStag reqUser = stagService.getAllById(id);
		Long oldUser = reqUser.getOldUserId();
		if (oldUser == 0) {
			System.out.println("new user");
			String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
			String pwd = RandomStringUtils.random(7, characters);
			System.out.println(pwd);

			UserEntity approvedUser = new UserEntity();
			// approvedUser.setActive(false);
			approvedUser.setApprovalStatus(1);
			approvedUser.setEmail(reqUser.getEmail());
			approvedUser.setPassword(passwordEncoder.encode(pwd));
			approvedUser.setTokenExpired(0);
			approvedUser.setUsername(reqUser.getUsername());
			approvedUser.setWrongPassCount(0);
			approvedUser.setRoles(reqUser.getRoles());
			approvedUser.setActive(true);
			approvedUser.setRandPass(true);
			String jwt = jwtTokenUtil.generateForceResetPass(approvedUser);
			approvedUser.setTokenResetPass(jwt);

			mailSender.sendUserPass(approvedUser, pwd);
			stagService.deleteById(id);

			service.saveData(approvedUser);

			auditData.CreateAuditScheme(username, " Approve User",
					username + " Approve New User " + approvedUser.getUsername(), date, null,
					String.valueOf(approvedUser));
		} else {
			System.out.println("Edit User");
			UserEntity approvedUser = new UserEntity();
			UserEntity oldDataAudit = new UserEntity();
			ArrayList<Role> newRoles = new ArrayList<Role>(reqUser.getRoles());

			approvedUser = service.getAllById(reqUser.getOldUserId());
			oldDataAudit = service.getAllById(reqUser.getOldUserId());

			// approvedUser.setApprovalStatus(0);
			approvedUser.setActive(true);
			approvedUser.setApprovalStatus(1);
			approvedUser.setEmail(reqUser.getEmail());
			approvedUser.setPassword(reqUser.getPassword());
			approvedUser.setTokenExpired(0);
			approvedUser.setUsername(reqUser.getUsername());
			approvedUser.setWrongPassCount(0);
			approvedUser.setRoles(newRoles);
			stagService.deleteById(id);
			service.saveData(approvedUser);
			auditData.CreateAuditScheme(username, " Approve User",
					username + " Approve Edit User " + approvedUser.getUsername(), date, String.valueOf(oldDataAudit),
					String.valueOf(approvedUser));
		}

		auditTrailService.saveData(auditData);
		return ResponseEntity.ok().headers(header).body("ok");

	}

	@CrossOrigin
	@PostMapping(path = "/resetPasswordToken/{id}", produces = { "application/json" })
	public ResponseEntity<?> resetPasswordToken(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String username = tf.getUsername(request);
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		UserStag reqUser = stagService.getAllById(id);

		System.out.println("new user!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
		String pwd = RandomStringUtils.random(7, characters);
		System.out.println(pwd);

		UserEntity approvedUser = new UserEntity();
		// approvedUser.setActive(false);
		approvedUser.setApprovalStatus(1);
		approvedUser.setEmail(reqUser.getEmail());
		approvedUser.setPassword(passwordEncoder.encode(pwd));
		approvedUser.setTokenExpired(0);
		approvedUser.setUsername(reqUser.getUsername());
		approvedUser.setWrongPassCount(0);
		approvedUser.setRoles(reqUser.getRoles());
		approvedUser.setActive(true);
		approvedUser.setRandPass(true);
		String jwt = jwtTokenUtil.generateForceResetPass(approvedUser);
		approvedUser.setTokenResetPass(jwt);

		mailSender.sendUserPass(approvedUser, pwd);
		stagService.deleteById(id);

		service.saveData(approvedUser);

		auditData.CreateAuditScheme(username, " Reset password token",
				username + " Reset password token " + approvedUser.getUsername(), date, null,
				String.valueOf(approvedUser));

		auditTrailService.saveData(auditData);
		return ResponseEntity.ok().headers(header).body("ok");

	}

	@CrossOrigin
	@PostMapping(path = "/declineStag", produces = { "application/json" })
	public ResponseEntity<?> decline(@RequestBody RevisionReqModel data, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);

		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {
			UserStag declinedUser = stagService.getAllById(data.getId());
			declinedUser.setComment(data.getComment());
			if (declinedUser.getOldUserId() != 0) {
				UserEntity oldUser = service.getAllById(declinedUser.getOldUserId());
				oldUser.setApprovalStatus(1);
				service.updateUser(oldUser);
				stagService.deleteById(data.getId());
				auditData.CreateAuditScheme(username, " Decline Edit User",
						username + " Decline edit user " + declinedUser.getUsername(), date, String.valueOf(oldUser),
						String.valueOf(declinedUser));
			} else {

				stagService.deleteById(declinedUser.getId());
				auditData.CreateAuditScheme(username, " Decline User",
						username + " Decline user " + declinedUser.getUsername(), date, null,
						String.valueOf(declinedUser));
			}

			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/revision", produces = { "application/json" })
	public ResponseEntity<?> revisionStag(@RequestBody RevisionReqModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
	
		try {

			UserStag userStag = stagService.getAllById(data.getId());
			userStag.setComment(data.getComment());
			userStag.setApprovalStatus(4);
			// roleStag.setDeleted(false);
			stagService.updateStat(userStag);
			// RoleStag newRoleStag = roleStagService.saveData(roleStag);

			auditData.CreateAuditScheme(username, " Ask for revision  User", username + " Ask for revision  user", date,
					null, String.valueOf(userStag));
			auditTrailService.saveData(auditData);

			return ResponseEntity.ok().headers(header).body("ok");

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/update", produces = { "application/json" })
	public ResponseEntity<?> updateUser(@RequestBody UserEntity user, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {

			UserEntity oldUser = service.findByUsername(user.getUsername());

			String oldUserAudit = String.valueOf(oldUser);

			UserStag mUser = new UserStag();
			mUser.setActive(user.isActive());
			mUser.setApprovalStatus(0);
			mUser.setEmail(user.getEmail());
			mUser.setPassword(user.getPassword());
			mUser.setRoles(user.getRoles());
			mUser.setTokenExpired(user.getTokenExpired());
			mUser.setUsername(user.getUsername());
			mUser.setWrongPassCount(user.getWrongPassCount());
			mUser.setOldUserId(oldUser.getId());
			mUser = stagService.saveData(mUser);

			oldUser.setApprovalStatus(5);
			service.updateUser(oldUser);

			// mUser = stagService.updateRoles(user);
			// mUser = stagService.updateUser(user);

			auditData.CreateAuditScheme(username, " Edit", username + " Edit user " + user.getUsername(), date,
					oldUserAudit, String.valueOf(mUser));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/resetAttempt", produces = { "application/json" })
	public ResponseEntity<?> resetAttempt(@RequestBody UserEntity user, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {

			UserEntity oldUser = service.findByUsername(user.getUsername());
			String oldUserAudit = String.valueOf(oldUser);
			oldUser.setWrongPassCount(0);
			service.updateUser(oldUser);

			if (oldUser.getApprovalStatus() == 5) {
				UserStag stagUser = stagService.findByUsername(user.getUsername());

				stagUser.setWrongPassCount(0);
				stagService.updatePassCount(stagUser);
			}
			auditData.CreateAuditScheme(username, " Reset Attempt", "Reset Login Attempt user " + user.getUsername(),
					date, oldUserAudit, null);
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body(oldUser);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/update/stag", produces = { "application/json" })
	public ResponseEntity<?> updateStagUser(@RequestBody UserStag user, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {
			UserStag oldUser = stagService.findByUsername(user.getUsername());
			String oldUserAudit = String.valueOf(oldUser);
			System.out.println("update STAG!!!");
			UserStag mUser = new UserStag();

			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user.setApprovalStatus(0);
			// mUser = stagService.updateRoles(user);
			mUser = stagService.updateUser(user);

			auditData.CreateAuditScheme(username, " Edit", username + " Edit user " + user.getUsername(), date,
					oldUserAudit, String.valueOf(mUser));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/delete", produces = { "application/json" })
	public ResponseEntity<?> deleteUser(@RequestBody UserEntity user, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		// auditData.CreateAudit(username, "update user ", username + " delete " +
		// user.getUsername() + " data", date);

		UserEntity mUser = service.findByUsername(user.getUsername());
		if (mUser != null) {
			mUser.setApprovalStatus(5);
			service.updateUser(mUser);

			UserStag uStag = new UserStag();
			uStag.setActive(mUser.isActive());
			uStag.setApprovalStatus(3);
			uStag.setEmail(mUser.getEmail());
			uStag.setPassword(mUser.getPassword());
			ArrayList<Role> newRoles = new ArrayList<Role>(mUser.getRoles());
			uStag.setRoles(newRoles);
			uStag.setTokenExpired(mUser.getTokenExpired());
			uStag.setUsername(mUser.getUsername());
			uStag.setWrongPassCount(mUser.getWrongPassCount());
			uStag.setOldUserId(mUser.getId());
			System.out.print("------");
			System.out.print(uStag.getId());
			stagService.deleteByUsername(uStag.getUsername());
			uStag = stagService.saveData(uStag);

		}
		if (mUser == null) {
			UserStag uStag = stagService.findByUsername(user.getUsername());
			if (uStag != null) {
				stagService.deleteById(uStag.getId());
			}
		}

		auditData.CreateAuditScheme(username, "Delete", username + " Delete user", date, String.valueOf(mUser), null);
		auditTrailService.saveData(auditData);
		return ResponseEntity.ok().headers(header).body(mUser);

	}

	@CrossOrigin
	@PostMapping(path = "/approveDelete/{id}", produces = { "application/json" })
	public ResponseEntity<?> approveDelete(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {
			UserStag uStag = stagService.getAllById(id);
			UserEntity mUser = service.getAllById(uStag.getOldUserId());

			if (uStag != null) {
				stagService.deleteById(id);
			}

			if (mUser != null) {
				service.delete(mUser.getUsername());
			}

			auditData.CreateAuditScheme(username, "Delete", username + " Delete user", date, String.valueOf(mUser),
					null);
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
