package com.springgateway.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class mainController {
	 @GetMapping("/portal")
	    public String index(Model model) {
	        return "index";
	    }
}
