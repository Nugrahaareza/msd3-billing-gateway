package com.springgateway.controller;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.mindrot.jbcrypt.BCrypt;
//import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.LoggedUser;
import com.springgateway.entity.Role;
import com.springgateway.entity.SystemProperties;
import com.springgateway.entity.UserEntity;
import com.springgateway.entity.UserStag;
import com.springgateway.mail.EmailServiceImpl;
import com.springgateway.model.AuthenticationRequest;
import com.springgateway.model.AuthenticationResponse;
import com.springgateway.model.LoginResponse;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.SystemPropertiesService;
import com.springgateway.service.UserService;
import com.springgateway.service.UserStagService;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.ResponsFormatter;
import com.springgateway.util.tokenFormater;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@RestController
@RequestMapping("/api/auth")
public class authController {
	ResponsFormatter response = new ResponsFormatter();

	// private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());
	private static final Logger LOGGER = LogManager.getLogger("authenticate-log");

	@Autowired
	private UserService service;

	@Autowired
	private UserStagService userStagService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuditTrailService auditTrailService;

	@Autowired
	private LoggedUserService loggedUserService;

	@Autowired
	private SystemPropertiesService sysPropsService;

	@Autowired
	private EmailServiceImpl mailSender;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@CrossOrigin(origins = "http://184.169.41.106:8080")
	@PostMapping(path = "/authenticate", produces = { "application/json" })
	public ResponseEntity<?> createAuthenticate(@RequestBody AuthenticationRequest user, HttpServletRequest request)
			throws Exception {
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		int attempt = Integer.parseInt(sysPropsService.getAllByName("PasswordAttempt"));
		auditData.CreateAudit(user.getUsername(), "Login", user.getUsername() + " Trying to Login", date);
		LOGGER.info("User login with username : " + user.getUsername());
		try {
			authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

		} catch (BadCredentialsException e) {
			UserEntity badUser = service.findByUsername(user.getUsername());

			if (badUser != null) {
				if (badUser.getApprovalStatus() != 5) {
					if (badUser.getWrongPassCount() >= attempt) {
						return new ResponseEntity<String>(HttpStatus.LOCKED);
					}
					badUser.setWrongPassCount(badUser.getWrongPassCount() + 1);
					service.updateUser(badUser);
				} else if (badUser.getApprovalStatus() == 5) {

					UserStag badUserStag = userStagService.findByUsername(user.getUsername());
					if (badUserStag.getWrongPassCount() >= attempt) {
						return new ResponseEntity<String>(HttpStatus.LOCKED);
					}
					badUserStag.setWrongPassCount(badUserStag.getWrongPassCount() + 1);
					userStagService.updatePassCount(badUserStag);
				}

			}
			LOGGER.error("Username or Password wrong!!!");
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		}

		UserEntity goodUser = service.findByUsername(user.getUsername());
		if (goodUser.getWrongPassCount() >= 3) {
			return new ResponseEntity<String>(HttpStatus.LOCKED);
		}
		goodUser.setWrongPassCount(0);
		service.updateUser(goodUser);

		UserDetails userDetails = service.loadUserByUsername(user.getUsername());
		LoginResponse lr = new LoginResponse();
		String jwt = jwtTokenUtil.generateToken(userDetails);
		lr.setJwt(jwt);
		if (goodUser.getTokenResetPass() != null) {
			lr.setIsRandPass(goodUser.isRandPass());
			lr.setResetPassToken(goodUser.getTokenResetPass());
		}
		LoggedUser logUs = new LoggedUser();
		logUs.setToken(jwt);

		loggedUserService.saveData(logUs);
		auditTrailService.saveData(auditData);
		return ResponseEntity.ok(lr);
	}

	@CrossOrigin
	@PostMapping(path = "/password/reset", produces = { "application/json" })
	public ResponseEntity<?> requestResetPassword(@RequestBody AuthenticationRequest userReq,
			HttpServletRequest request) throws Exception {
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(userReq.getUsername(), "Request reset password",
				userReq.getUsername() + " Request to reset passwrd", date);

		LOGGER.info("User with username: " + userReq.getUsername() + " request to reset password");
		UserEntity user = service.findByUsername(userReq.getUsername());

		if (user == null) {
			user = service.findByEmail(userReq.getUsername());
			if (user != null) {
				if (!user.isRandPass()) {
					System.out.println(user.getEmail());
					String jwt = jwtTokenUtil.generateForceResetPass(user);
					mailSender.sendSimpleMessage(jwt, user.getEmail());
					auditTrailService.saveData(auditData);
				} else if (user.isRandPass()) {
					return new ResponseEntity<String>("This user cannot reset password yet ", HttpStatus.BAD_REQUEST);
				}
			}
		} else if (user != null) {
			if (!user.isRandPass()) {
				System.out.println(user.getEmail());
				String jwt = jwtTokenUtil.generateToken(user);
				mailSender.sendSimpleMessage(jwt, user.getEmail());
				auditTrailService.saveData(auditData);
			} else if (user.isRandPass()) {
				return new ResponseEntity<String>("This user cannot reset password yet ", HttpStatus.BAD_REQUEST);
			}
		}
		return ResponseEntity.ok("oke");

	}

	@SuppressWarnings("unused")
	@CrossOrigin
	@PostMapping(path = "/password/update", produces = { "application/json" })
	public ResponseEntity<?> updatePassword(@RequestBody AuthenticationRequest userReq, HttpServletRequest request)
			throws Exception {

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(userReq.getUsername(), "Update Password", userReq.getUsername() + " Update Password",
				date);

		UserEntity user = service.findByUsername(userReq.getUsername());

		//LOGGER.info("User with username: " + user.getUsername() + " update Password");
		if (user != null) {
			user.setPassword(passwordEncoder.encode(userReq.getPassword()));
			if (user.isRandPass()) {
				user.setRandPass(false);
				user.setTokenResetPass(null);
			}
		}else if(user == null) {
			user = service.getAllById(Long.valueOf(userReq.getUsername()));
			if (user != null) {
				user.setPassword(passwordEncoder.encode(userReq.getPassword()));
				if (user.isRandPass()) {
					user.setRandPass(false);
					user.setTokenResetPass(null);
				}
			}
		}

		service.updatePassword(user);
		auditTrailService.saveData(auditData);
		return ResponseEntity.ok("oke");
	}

	@CrossOrigin
	@PostMapping(path = "/expiredToken", produces = { "application/json" })
	public ResponseEntity<?> expireToken(@RequestBody AuthenticationRequest userReq, HttpServletRequest request)
			throws Exception {

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(userReq.getUsername(), "Expire Token",
				userReq.getUsername() + " set status as expire token", date);

		UserEntity user = service.findByUsername(userReq.getUsername());
		if (user != null) {
			if (user.isRandPass()) {
				user.setApprovalStatus(5);
				user.setActive(false);
			}
		}
		service.delete(user.getUsername());
		// service.setExpireToken(user);
		UserStag stagUser = new UserStag();
		Collection<Role> lRole;
		// stagUser.setActive(false);
		stagUser.setApprovalStatus(6);
		stagUser.setEmail(user.getEmail());
		stagUser.setPassword(user.getPassword());
		// stagUser.setTokenExpired(0);
		// lRole = user.getRoles();
		stagUser.setUsername(user.getUsername());
		stagUser.setWrongPassCount(0);
		stagUser.setRoles(user.getRoles());
		stagUser.setActive(false);
		// stagUser.setRandPass(true);
		userStagService.saveData(stagUser);

		auditTrailService.saveData(auditData);
		return ResponseEntity.ok("oke");
	}

	@CrossOrigin
	@PostMapping(path = "/logout", produces = { "application/json" })
	public Map<String, Object> logout(@RequestBody String tokenReq, HttpServletRequest req) throws Exception {
		JwtUtil tokenUtil = new JwtUtil();
		tokenFormater tf = new tokenFormater();
		AuditTrail auditData = new AuditTrail();

		String token;
		JSONObject jsonObject = new JSONObject(tokenReq);
		String result = jsonObject.get("token").toString();
		token = tokenUtil.parseAuthHeader(result);
		System.out.println(token);

		loggedUserService.delete(token);
		String username = tf.getUsername(token);

		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Log Out", username + " Logout from apps", date);
		auditTrailService.saveData(auditData);

		return response.isValid(null, "Success");

	}

}
