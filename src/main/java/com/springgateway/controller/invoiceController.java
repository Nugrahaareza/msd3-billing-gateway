package com.springgateway.controller;

import java.io.File;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.BillingSubscriptionLog;
import com.springgateway.entity.Invoice;
import com.springgateway.mail.EmailServiceImpl;
import com.springgateway.repository.BillingSubscriptionLogRepository;
import com.springgateway.service.BillingSubscriptionLogService;
import com.springgateway.service.InvoiceService;
import com.springgateway.util.Authority;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.tokenFormater;
import com.springgateway.httpClient.*;

@RestController
@RequestMapping("/api/invoice")
public class invoiceController {

	@Autowired
	InvoiceService service;

	@Autowired
	BillingSubscriptionLogService invoiceLogService;

	@Autowired
	private RefreshTokenGenerator rTokGen;

	@Autowired
	Authority authUtil;

	public final String API_GROUP = "/api/invoice";

	@CrossOrigin
	@GetMapping(path = "/getinvoice/{id}", produces = { "application/json" })
	public ResponseEntity<?> getInvoice(@PathVariable int id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		HttpHeaders header = new HttpHeaders();
		header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=test.pdf");
		header.add("Cache-Control", "max-age=0");
		header.add("Pragma", "public");
		header.add("Expires", "0");
		String rTok = rTokGen.refreshTokenGenerator(request);
		if (rTok != null) {
			header.add("RTok", rTok);	
		}
		ByteArrayResource resource = new ByteArrayResource(HttpClient.downloadInvoice(id));

		auditData.CreateAudit(username, " Download invoices", username + "Downloading an invoice", date);

		return ResponseEntity.ok().headers(header).contentLength(resource.contentLength())
				.contentType(MediaType.parseMediaType("application/pdf")).body(resource);
	}

	@CrossOrigin
	@GetMapping(path = "/getperiodinvoice", produces = { "application/json" })
	public ResponseEntity<?> getPeriodInvoice(@RequestParam String startDate, @RequestParam String endDate,
			@RequestParam(required = false) String organizationName, @RequestParam(required = false) Boolean isPaid,
			HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);	
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		Gson gson = new Gson();
		List<Invoice> invoice = new ArrayList<Invoice>();
		auditData.CreateAudit(username, "Access  Page", username + " Accessing invoice List page", date);

		String resp = HttpClient.invoiceReq(startDate, endDate, organizationName, isPaid);

		return ResponseEntity.ok().headers(header).body(resp);
	}

	@CrossOrigin
	@GetMapping(path = "/getinvoicedetail/{id}", produces = { "application/json" })
	public ResponseEntity<?> getPeriodDetail(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);	
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		List<BillingSubscriptionLog> data = new ArrayList<BillingSubscriptionLog>();

		String resp = HttpClient.invoiceReqDetail(id);
		auditData.CreateAudit(username, "Access  Page", username + " Accessing invoice detail page with id: " + id,
				date);
		return ResponseEntity.ok().headers(header).body(resp);
	}

	/*
	 * @CrossOrigin
	 * 
	 * @PostMapping(path = "/notifyCustomer", produces = { "application/json" })
	 * public ResponseEntity<?> notifyCustomer(@RequestBody Invoice reqBody,
	 * HttpServletRequest request) throws Exception { try {
	 * mailSender.sendMessageToCustomer(reqBody.getOrganization()); return
	 * ResponseEntity.ok("ok"); } catch (Exception e) { return new
	 * ResponseEntity<String>("Something went wrong",
	 * HttpStatus.INTERNAL_SERVER_ERROR); } }
	 */

}
