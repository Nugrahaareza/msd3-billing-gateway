package com.springgateway.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.AuthorityPage;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.Pages;
import com.springgateway.entity.Role;
import com.springgateway.entity.RoleStag;
import com.springgateway.entity.UserEntity;
import com.springgateway.model.AddPage;
import com.springgateway.model.PageRole;
import com.springgateway.model.RevisionReqModel;
import com.springgateway.model.RoleModel;
import com.springgateway.model.SetBillRequest;
import com.springgateway.model.UserRoles;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.AuthorityPageService;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.PageService;
import com.springgateway.service.RolesService;
import com.springgateway.service.RolesStagService;
import com.springgateway.service.UserService;
import com.springgateway.util.Authority;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.ResponsFormatter;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/config")
public class configController {

	ResponsFormatter response = new ResponsFormatter();

	@Autowired
	private AuthorityPageService service;

	@Autowired
	private AuditTrailService auditTrailService;

	@Autowired
	private RolesService roleService;

	@Autowired
	private RolesStagService roleStagService;

	@Autowired
	private UserService userService;

	@Autowired
	private PageService pageService;

	@Autowired
	Authority authUtil;

	@Autowired
	private RefreshTokenGenerator rTokGen;

	public final String API_GROUP = "/api/config";

	@CrossOrigin
	@GetMapping(path = "/getpages", produces = { "application/json" })
	public ResponseEntity<?> getpages(HttpServletRequest req) throws Exception {
		JwtUtil tokenUtil = new JwtUtil();
		tokenFormater tf = new tokenFormater();

		String token = req.getHeader("Authorization");
		token = tokenUtil.parseAuthHeader(token);

		String role = tf.getRole(token);
		if (role != null) {
			List<PageRole> pr = service.getPages(role);

			return  ResponseEntity.ok().body(pr);
		} else {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/getallpages", produces = { "application/json" })
	public ResponseEntity<?> getAllPages(HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access user list", date);
		auditTrailService.saveData(auditData);
		try {
			List<Pages> roles = pageService.getAll();
			return ResponseEntity.ok().headers(header).body(roles);
		} catch (Exception e) {
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/rolelist", produces = { "application/json" })
	public ResponseEntity<?> roleList(HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access role list", date);
		auditTrailService.saveData(auditData);
		try {
			List<RoleModel> roles = roleService.getAllRoles();
			List<RoleModel> rolesStag = roleStagService.getAllRoles();
			rolesStag.addAll(roles);
			return ResponseEntity.ok().headers(header).body(rolesStag);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/approvedRole", produces = { "application/json" })
	public ResponseEntity<?> approvedRole(HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access role list", date);
		auditTrailService.saveData(auditData);
		try {
			List<RoleModel> roles = roleService.getAllRoles();
			// List<RoleModel> rolesStag = roleStagService.getAllRoles();
			// rolesStag.addAll(roles);
			return ResponseEntity.ok().headers(header).body(roles);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/roleDetail/{id}", produces = { "application/json" })
	public ResponseEntity<?> roleDetail(@PathVariable int id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access user list", date);
		auditTrailService.saveData(auditData);
		try {
			List<PageRole> roles = service.getRolesDetail(id);
			if (roles.size() < 1) {
				Role roleDetail = roleService.getAllById(id);
				return ResponseEntity.ok().headers(header).body(roleDetail);
			}
			return ResponseEntity.ok(roles);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/roleStagDetail/{id}", produces = { "application/json" })
	public ResponseEntity<?> roleStagDetail(@PathVariable int id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access Role detail", date);
		auditTrailService.saveData(auditData);
		try {

			List<PageRole> roles = service.getRolesStagDetail(id);
			System.out.println(roles.size());
			if (roles.size() < 1) {
				System.out.println("!!!!!!!!");
				RoleStag roleDetail = roleStagService.getAllById(id);
				return ResponseEntity.ok(roleDetail);
			}
			return ResponseEntity.ok().headers(header).body(roles);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/addRoles", produces = { "application/json" })
	public ResponseEntity<?> addRole(@RequestBody AddPage req, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {
			// Role savedrole = roleService.saveData(req.getRole());
			Role existingRole = roleService.getAllByRoleName(req.getRole().getRoleName());
			if (existingRole != null) {
				return new ResponseEntity<String>("Rolename is already used", HttpStatus.BAD_REQUEST);
			}
			RoleStag savedRole = roleStagService.saveData(req.getRole());
			savedRole.setApprovalStatus(0);

			List<AuthorityPage> authpge = new ArrayList<AuthorityPage>();
			// List<Pages> page = req.getPage();
			if (req.getPage().size() > 0) {
				for (Pages p : req.getPage()) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(p.getId());
					ap.setRolePk(savedRole.getId());
					authpge.add(ap);
				}
				service.saveDatas(authpge);
				auditData.CreateAuditScheme(username, " Add new user", username + " Add new Role", date, null,
						String.valueOf(req.getRole()));
				auditTrailService.saveData(auditData);
			}
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			String err = String.valueOf(e);
			System.out.println(err);
			if (err.contains("unique result")) {
				return new ResponseEntity<String>("Rolename is already used", HttpStatus.BAD_REQUEST);
			}
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unused")
	@CrossOrigin
	@PostMapping(path = "/approveRole", produces = { "application/json" })
	public ResponseEntity<?> approveRole(@RequestBody RevisionReqModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {

			RoleStag roleStag = roleStagService.getAllById(data.getId());
			roleStag.setComment(data.getComment());
			Long oldRoleId = roleStag.getOldRoleId();

			if (oldRoleId != 0) {
				Role rl = roleService.getAllById(oldRoleId);
				List<AuthorityPage> authpge = new ArrayList<AuthorityPage>();
				List<AuthorityPage> stagAuthPage = service.getAllByRolePk(data.getId());

				rl.setApprovalStatus(1);
				roleService.update(rl);

				List<Pages> stagPage = new ArrayList<Pages>();
				for (AuthorityPage aps : stagAuthPage) {
					Pages page = pageService.getAllById(aps.getPagesPk());
					stagPage.add(page);
				}

				if (stagAuthPage.size() > 0) {
					service.deleteByRole(rl.getId());
					for (Pages p : stagPage) {
						AuthorityPage ap = new AuthorityPage();
						ap.setPagesPk(p.getId());
						ap.setRolePk(rl.getId());
						authpge.add(ap);
					}
					service.saveDatas(authpge);
				}
				auditData.CreateAuditScheme(username, " Approve edit role", username + " Approve edit Role", date,
						String.valueOf(rl), String.valueOf(roleStag));
				auditTrailService.saveData(auditData);
				roleStagService.deleteByRole(data.getId());
				service.deleteByRole(data.getId());
			} else if (oldRoleId == null || oldRoleId == 0) {
				List<AuthorityPage> authpge = new ArrayList<AuthorityPage>();
				List<AuthorityPage> stagAuthPage = service.getAllByRolePk(data.getId());

				Role savedRole = new Role();
				savedRole.setApprovalStatus(1);
				savedRole.setRoleName(roleStag.getRoleName());
				savedRole = roleService.saveData(savedRole);

				List<Pages> stagPage = new ArrayList<Pages>();
				for (AuthorityPage aps : stagAuthPage) {
					Pages page = pageService.getAllById(aps.getPagesPk());
					stagPage.add(page);
				}

				if (stagPage.size() > 0) {
					for (Pages p : stagPage) {
						AuthorityPage ap = new AuthorityPage();
						ap.setPagesPk(p.getId());
						ap.setRolePk(savedRole.getId());
						authpge.add(ap);
					}
					service.saveDatas(authpge);
				}
				roleStagService.deleteByRole(data.getId());
				service.deleteByRole(data.getId());
				auditData.CreateAuditScheme(username, " Approve new role", username + " Approve new Role", date,
						String.valueOf(roleStag), String.valueOf(savedRole));
				auditTrailService.saveData(auditData);
			}
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unused")
	@CrossOrigin
	@PostMapping(path = "/approveDeleteRole", produces = { "application/json" })
	public ResponseEntity<?> approveDeleteRole(@RequestBody RevisionReqModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		// auditData.CreateAudit(username, "Access page", username + " Update role",
		// date);
		auditTrailService.saveData(auditData);
		try {
			RoleStag roleStag = roleStagService.getAllById(data.getId());
			roleStag.setComment(data.getComment());
			if (roleStag.getOldRoleId() != 0) {
				roleService.deleteByRole(roleStag.getOldRoleId());
				service.deleteByRole(roleStag.getOldRoleId());
				roleStagService.deleteByRole(data.getId());
				service.deleteByRole(data.getId());

			} else {
				roleStagService.deleteByRole(data.getId());
				service.deleteByRole(data.getId());
			}
			auditData.CreateAuditScheme(username, " Delete  role", username + " Approve to delete role", date, null,
					String.valueOf(roleStag));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/deleteRole/{id}", produces = { "application/json" })
	public ResponseEntity<?> deteleRole(@PathVariable int id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role", date);
		auditTrailService.saveData(auditData);
		try {
			Role role = roleService.getAllById(id);
			List<UserRoles> ue = userService.findByRole(id);
			System.out.println(ue);
			System.out.println(ue.size());
			if (ue.size() > 0) {
				return new ResponseEntity<String>("Role is already in use", HttpStatus.BAD_REQUEST);
			}
			RoleStag roleStag = new RoleStag();

			if (role != null) {
				role.setApprovalStatus(5);
				roleService.update(role);

				roleStag.setOldRoleId(role.getId());
				roleStag.setApprovalStatus(3);
				roleStag.setDeleted(false);
				roleStag.setRoleName(role.getRoleName());
				RoleStag newRoleStag = roleStagService.saveData(roleStag);

				List<AuthorityPage> authpge = new ArrayList<AuthorityPage>();
				List<AuthorityPage> oldAuthAPge = service.getAllByRolePk(id);
				if (oldAuthAPge.size() > 0) {
					for (AuthorityPage aps : oldAuthAPge) {
						AuthorityPage ap = new AuthorityPage();
						ap.setPagesPk(aps.getPagesPk());
						ap.setRolePk(newRoleStag.getId());
						authpge.add(ap);
					}
					service.saveDatas(authpge);
					auditData.CreateAuditScheme(username, " Delete  role", username + "Request fot delete  role", date,
							null, String.valueOf(roleStag));
				}
			} else if (role == null) {
				roleStag = roleStagService.getAllById(id);
				roleStag.setApprovalStatus(3);
				auditData.CreateAuditScheme(username, " Delete  role", username + "Request fot delete  role", date,
						null, String.valueOf(roleStag));
				roleStagService.update(roleStag);
			}
			// roleStag.setOldRoleId(id);

			return ResponseEntity.ok().headers(header).body("ok");

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/declineStag", produces = { "application/json" })
	public ResponseEntity<?> declineStag(@RequestBody RevisionReqModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role", date);
		auditTrailService.saveData(auditData);
		try {
			RoleStag roleStag = roleStagService.getAllById(data.getId());
			roleStag.setComment(data.getComment());
			if (roleStag.getOldRoleId() != 0) {
				Role oldRole = roleService.getAllById(roleStag.getOldRoleId());
				oldRole.setApprovalStatus(1);
				roleService.update(oldRole);
				service.deleteByRole(data.getId());
				roleStagService.deleteByRole(data.getId());
				auditData.CreateAuditScheme(username, " Decline  role", username + " Decline  Role", date,
						String.valueOf(oldRole), String.valueOf(roleStag));
			} else {

				roleStagService.deleteByRole(data.getId());
				auditData.CreateAuditScheme(username, " Decline  role", username + " Decline  Role", date, null,
						String.valueOf(roleStag));
			}

			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/revision/{id}", produces = { "application/json" })
	public ResponseEntity<?> revisionStag(@RequestBody RevisionReqModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role", date);
		auditTrailService.saveData(auditData);
		try {

			RoleStag roleStag = roleStagService.getAllById(data.getId());
			roleStag.setApprovalStatus(4);
			roleStag.setComment(data.getComment());
			roleStagService.revision(roleStag);
			RoleStag newRoleStag = roleStagService.saveData(roleStag);

			auditData.CreateAuditScheme(username, " Ask for revision  role", username + " Ask for revision  Role", date,
					null, String.valueOf(newRoleStag));
			auditTrailService.saveData(auditData);

			return ResponseEntity.ok().headers(header).body("ok");

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/updateRoles", produces = { "application/json" })
	public ResponseEntity<?> updateRole(@RequestBody List<AuthorityPage> req, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role authority", date);
		auditTrailService.saveData(auditData);
		try {
			AuthorityPage data = req.get(0);
			long rolepk = data.getRolePk();
			// Boolean delete = service.deleteByRole(rolepk);
			Role role = roleService.getAllById(rolepk);
			role.setApprovalStatus(5);
			roleService.update(role);

			RoleStag roleStag = new RoleStag();
			roleStag.setApprovalStatus(0);
			roleStag.setDeleted(false);
			roleStag.setRoleName(role.getRoleName());
			roleStag.setOldRoleId(rolepk);
			RoleStag newRoleStag = roleStagService.saveData(roleStag);

			List<AuthorityPage> authpge = new ArrayList<AuthorityPage>();
			// List<Pages> page = req.getPage();
			if (req.size() > 0) {
				for (AuthorityPage aps : req) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(aps.getPagesPk());
					ap.setRolePk(newRoleStag.getId());
					authpge.add(ap);
				}
				service.saveDatas(authpge);
			}

			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/updateRolesStag", produces = { "application/json" })
	public ResponseEntity<?> updateRoleStag(@RequestBody List<AuthorityPage> req, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role authority", date);
		auditTrailService.saveData(auditData);
		try {
			AuthorityPage data = req.get(0);
			long rolepk = data.getRolePk();
			Boolean delete = service.deleteByRole(rolepk);
			if (delete) {
				List<AuthorityPage> resp = service.saveDatas(req);
				RoleStag rs = roleStagService.getAllById(rolepk);
				rs.setApprovalStatus(0);
				roleStagService.update(rs);
				return ResponseEntity.ok().headers(header).body(resp);

			} else {
				return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
