package com.springgateway.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpHeaders;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.PaymentMessageAuditTrail;
import com.springgateway.entity.UserEntity;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.PaymentMessageAuditTrailService;
import com.springgateway.service.UserService;
import com.springgateway.util.Authority;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.ResponsFormatter;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/paymentTrail")
public class paymentTrailController {

	ResponsFormatter response = new ResponsFormatter();
	@Autowired
	private PaymentMessageAuditTrailService payAuditService;
	
	@Autowired
	private AuditTrailService service;

	@Autowired
	private RefreshTokenGenerator rTokGen;

	@Autowired
	Authority authUtil;

	public final String API_GROUP = "/api/paymentTrail";

	@CrossOrigin(allowedHeaders = "*")
	@GetMapping(path = "/getall", produces = { "application/json" })
	public ResponseEntity<?> getById(@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate, HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);

		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access audit payment page", date);
		service.saveData(auditData);
		List<PaymentMessageAuditTrail> data = new ArrayList<PaymentMessageAuditTrail>();
		if(startDate !=null && endDate!=null) {
			LocalDate startLocalDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			LocalDate endLocalDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			LocalDateTime startLocalDateTime = startLocalDate.atStartOfDay();
			LocalDateTime endLocalDateTime = endLocalDate.atStartOfDay();
			data= payAuditService.getAllWithDate(startLocalDateTime,endLocalDateTime);
		}else {
			data = payAuditService.getAll();
		}

		return ResponseEntity.ok().headers(header).body(data);

	}

}
