package com.springgateway.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.Pages;
import com.springgateway.entity.SystemProperties;
import com.springgateway.entity.UserEntity;
import com.springgateway.httpClient.HttpClient;
import com.springgateway.model.SysPropResp;
import com.springgateway.model.UserModel;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.PageService;
import com.springgateway.service.RolesService;
import com.springgateway.service.SystemPropertiesService;
import com.springgateway.service.UserService;
import com.springgateway.util.Authority;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/properties")
public class propertiesController {
	@Autowired
	private SystemPropertiesService service;

	@Autowired
	private UserService userService;

	@Autowired
	private AuditTrailService auditTrailService;

	
	@Autowired
	private RefreshTokenGenerator rTokGen;

	@Autowired
	Authority authUtil;

	LocalDateTime date = LocalDateTime.now();

	public final String API_GROUP = "/api/properties";

	@CrossOrigin
	@GetMapping(path = "/getAll", produces = { "application/json" })
	public ResponseEntity<?> listProp(HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);	
		}


		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access system properties menu", date);
		auditTrailService.saveData(auditData);
		try {
			List<SystemProperties> datas = service.getAll();
			List<SysPropResp> resp  = new ArrayList<SysPropResp>();
			for (SystemProperties sp : datas) {
				SysPropResp spr = new SysPropResp();
				spr.setId(sp.getId());
				spr.setLastModified(sp.getLastModified());
				spr.setPropertyDescription(sp.getPropertyDescription());
				spr.setPropertyName(sp.getPropertyName());
				spr.setPropertyValue(sp.getPropertyValue());
				spr.setLastModifiedBy(sp.getLastModifiedBy());
				resp.add(spr);
			}
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/update", produces = { "application/json" })
	public ResponseEntity<?> update(@RequestBody SystemProperties reqData, HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);	
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {
			UserEntity user = userService.findByUsername(username);
			reqData.setLastModifiedBy(user.getUsername());

			SystemProperties oldDatas = service.getAllById(reqData.getId());
			String oldProp = String.valueOf(oldDatas);

		

			///return ResponseEntity.ok(resp);
			
			System.out.print(reqData);
			SystemProperties datas = service.updateProperties(reqData);
			String resp = HttpClient.resyncProps();
			auditData.CreateAuditScheme(username, "Upadate system properties",
					username + " updating system properties data", date, oldProp, String.valueOf(datas));
			auditTrailService.saveData(auditData);

			return ResponseEntity.ok().headers(header).body(reqData);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
