package com.springgateway.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.JobTrail;
import com.springgateway.entity.SyncJob;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.JobTrailService;
import com.springgateway.service.OrganizationService;
import com.springgateway.service.SyncJobService;
import com.springgateway.util.Authority;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/logjob")
public class logJobController {

	@Autowired
	private JobTrailService service;

	@Autowired
	private AuditTrailService auditTrailService;

	@Autowired
	Authority authUtil;
	
	@Autowired
	private RefreshTokenGenerator rTokGen;

	LocalDateTime date = LocalDateTime.now();

	public final String API_GROUP = "/api/logjob";

	@CrossOrigin
	@GetMapping(path = "/all", produces = { "application/json" })
	public ResponseEntity<?> ListJob(HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);	
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access Sync Job page ", date);
		auditTrailService.saveData(auditData);
		try {
			List<JobTrail> data = service.getTop100Desc();
			return ResponseEntity.ok().headers(header).body(data);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/bydate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getInvoice(@RequestParam String startDate, @RequestParam String endDate,
			HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);	
		}
		
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access Sync Job page ", date);
		auditTrailService.saveData(auditData);

		try {
			LocalDateTime startLocalDate = LocalDateTime.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			LocalDateTime endLocalDate = LocalDateTime.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
			System.out.println(startLocalDate);
			System.out.println(endLocalDate);
			System.out.println(startDate);
			System.out.println(endDate);
			List<JobTrail> data = service.getAllByDate(startLocalDate, endLocalDate);
			return ResponseEntity.ok().headers(header).body(data);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
