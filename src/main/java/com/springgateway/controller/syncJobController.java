package com.springgateway.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.Organization;
import com.springgateway.entity.SyncJob;
import com.springgateway.httpClient.HttpClient;
import com.springgateway.model.SyncJobRequest;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.OrganizationService;
import com.springgateway.service.SyncJobService;
import com.springgateway.util.Authority;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/syncjob")
public class syncJobController {
	@Autowired
	private SyncJobService service;
	
	@Autowired
	private OrganizationService orgService;

	@Autowired
	private AuditTrailService auditTrailService;

	@Autowired
	Authority authUtil;
	
	@Autowired
	private RefreshTokenGenerator rTokGen;

	LocalDateTime date = LocalDateTime.now();

	public final String API_GROUP = "/api/syncjob";
	
	
	@CrossOrigin
	@GetMapping(path = "/listjob", produces = { "application/json" })
	public ResponseEntity<?> ListJob(HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access Sync Job page ", date);
		auditTrailService.saveData(auditData);
		try {
			List<SyncJob> data = service.getAll();
			return ResponseEntity.ok().headers(header).body(data);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin
	@GetMapping(path = "/listorg", produces = { "application/json" })
	public ResponseEntity<?> ListOrg(HttpServletRequest request) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access Sync Job page ", date);
		auditTrailService.saveData(auditData);
		try {
			List<Organization> org  = orgService.getAll();
			return ResponseEntity.ok().headers(header).body(org);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin
	@PostMapping(path = "/syncdata", produces = { "application/json" })
	public ResponseEntity<?> synData(HttpServletRequest request,@RequestBody SyncJobRequest reqData) {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
			
		}
		String username = tf.getUsername(request);
		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Sync Data ", username + " Synchronizing "+ reqData.getJobId(), date);
		auditTrailService.saveData(auditData);
		try {
			SyncJob sj = service.getAllById(reqData.getJobId());
			String resp = HttpClient.syncData(reqData,sj);
			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
