package com.springgateway.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.entity.RoleStag;
import com.springgateway.entity.Subscription;
import com.springgateway.entity.UserEntity;
import com.springgateway.model.ApproveBillingModel;
import com.springgateway.model.BillingTemplateModel;
import com.springgateway.model.BillingTemplateRequest;
import com.springgateway.model.RevisionReqModel;
import com.springgateway.service.AuditTrailService;
import com.springgateway.service.BillingSchemeService;
import com.springgateway.service.BillingSchemeTemplateChargeService;
import com.springgateway.service.BillingSchemeTemplateChargeServiceStag;
import com.springgateway.service.BillingSchemeTemplateService;
import com.springgateway.service.BillingSchemeTemplateServiceStag;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.SubscriptionService;
import com.springgateway.util.Authority;
import com.springgateway.util.JwtUtil;
import com.springgateway.util.RefreshTokenGenerator;
import com.springgateway.util.ResponsFormatter;
import com.springgateway.util.tokenFormater;

@RestController
@RequestMapping("/api/billingSchemeTemplate")
public class billingTemplateController {

	ResponsFormatter response = new ResponsFormatter();
	@Autowired
	private BillingSchemeTemplateService service;
	@Autowired
	private BillingSchemeTemplateServiceStag stagService;

	@Autowired
	private BillingSchemeTemplateChargeService chargeservice;
	@Autowired
	private BillingSchemeTemplateChargeServiceStag stagChargesService;

	@Autowired
	private BillingSchemeService billSchemeService;
	@Autowired
	private SubscriptionService subsService;

	@Autowired
	private RefreshTokenGenerator rTokGen;

	@Autowired
	Authority authUtil;

	@Autowired
	private AuditTrailService auditTrailService;

	public final String API_GROUP = "/api/billingSchemeTemplate";

	@CrossOrigin
	@GetMapping(path = "/getall", produces = { "application/json" })
	public ResponseEntity<?> getall(HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing tempalte list page", date);
		auditTrailService.saveData(auditData);

		try {
			List<?> resp = null;
			List<BillingSchemeTemplate> data = service.getAllNoDeleted();
			List<BillingSchemeTemplateStag> dataStag = stagService.getAllNoDeleted();
			resp = Stream.concat(dataStag.stream(), data.stream()).collect(Collectors.toList());

			return ResponseEntity.ok().headers(header).body(resp);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/getallconnectedapps/{id}", produces = { "application/json" })
	public ResponseEntity<?> getallconnected(@PathVariable long id, HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing tempalte list page", date);
		auditTrailService.saveData(auditData);

		try {
			List<Subscription> subs = new ArrayList<Subscription>();
			BillingSchemeTemplate existingTemplate = service.getAllById(id);
			if (existingTemplate != null) {
				List<BillingScheme> existingScheme = billSchemeService.getAllBybillingSchemeTemplate(existingTemplate);
				if (existingScheme != null) {
					for (BillingScheme bs : existingScheme) {
						Subscription newSubs = subsService.getAllByBillingScheme(bs);
						if (newSubs != null) {
							subs.add(newSubs);
						}
					}
				}
			}

			return ResponseEntity.ok().headers(header).body(subs);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/allappspackage", produces = { "application/json" })
	public ResponseEntity<?> getallappspackage(HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing tempalte list page", date);
		auditTrailService.saveData(auditData);

		try {
			List<Subscription> subs = new ArrayList<Subscription>();
			List<BillingSchemeTemplate> existingTemplate = service.getAll();
			if (existingTemplate != null) {
				for (BillingSchemeTemplate bst : existingTemplate) {
					List<BillingScheme> existingScheme = billSchemeService.getAllBybillingSchemeTemplate(bst);
					if (existingScheme != null) {
						for (BillingScheme bs : existingScheme) {
							Subscription newSubs = subsService.getAllByBillingScheme(bs);
							if (newSubs != null) {
								subs.add(newSubs);
							}
						}
					}
				}
			}

			return ResponseEntity.ok().headers(header).body(subs);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/{id}", produces = { "application/json" })
	public ResponseEntity<?> getById(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing template with id: " + id + " page",
				date);
		auditTrailService.saveData(auditData);

		try {
			BillingSchemeTemplate data = service.getAllById(id);
			if (data == null) {
				System.out.println("----");
				// BillingSchemeTemplateStag stagData = stagService.getAllById(id);
				// return ResponseEntity.ok(stagData);
			}
			return ResponseEntity.ok().headers(header).body(data);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/stag/{id}", produces = { "application/json" })
	public ResponseEntity<?> getStagByOldId(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing template with id: " + id + " page",
				date);
		auditTrailService.saveData(auditData);

		try {
			System.out.println("----");
			// BillingSchemeTemplateStag stagData = stagService.getAllByOldBillingId(id);
			BillingSchemeTemplateStag stagData = stagService.getAllById(id);
			return ResponseEntity.ok().headers(header).body(stagData);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "detail/{id}", produces = { "application/json" })
	public ResponseEntity<?> getDetailById(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing tempalte charge " + id + " page",
				date);
		auditTrailService.saveData(auditData);

		try {
			List<?> mUser = chargeservice.getAllByPKId(id);
			if (mUser.size() <= 0) {
				System.out.println("-----");
				mUser = stagChargesService.getAllByPkId(id);
			}
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping(path = "/stag/detail/{id}", produces = { "application/json" })
	public ResponseEntity<?> getStagDetailById(@PathVariable long id, HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Access billing tempalte charge " + id + " page",
				date);
		auditTrailService.saveData(auditData);

		try {
			List<?> mUser = stagChargesService.getAllByPkId(id);
			return ResponseEntity.ok().headers(header).body(mUser);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/add", produces = { "application/json" })
	public ResponseEntity<?> addScheme(@RequestBody List<BillingSchemeTemplateChargeStag> reqData,
			HttpServletRequest request) throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}

		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {
			System.out.println("-------!!!!--------");
			System.out.println(reqData);
			if (reqData.get(0).getBillingschemetemplate().getBillingSchemeTemplateType().toUpperCase().equals("TIER")) {
				BillingSchemeTemplateStag bst = stagService.saveData(reqData.get(0).getBillingschemetemplate());
				for (BillingSchemeTemplateChargeStag bstc : reqData) {
					bstc.setBillingschemetemplate(bst);
				}
				List<BillingSchemeTemplateChargeStag> bilTempCharge = stagChargesService.saveDatas(reqData);
			}
			List<BillingSchemeTemplateChargeStag> bilTempCharge = stagChargesService.saveDatas(reqData);

			auditData.CreateAuditScheme(username, "Add new billing template",
					username + " Add new billing Tempalte named  "
							+ bilTempCharge.get(0).getBillingschemetemplate().getBillingSchemeTemplateName(),
					date, null, String.valueOf(bilTempCharge));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body(bilTempCharge);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/update", produces = { "application/json" })
	public ResponseEntity<?> updateTemplate(@RequestBody List<BillingSchemeTemplateCharge> data,
			HttpServletRequest request) throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		List<BillingSchemeTemplateCharge> bstc = chargeservice
				.getAllByPKId(data.get(0).getBillingschemetemplate().getBillingTemplateId());

		long id = data.get(0).getBillingschemetemplate().getBillingTemplateId();
		BillingSchemeTemplate bst = data.get(0).getBillingschemetemplate();
		BillingSchemeTemplateStag bsts = stagService
				.getAllById(data.get(0).getBillingschemetemplate().getBillingTemplateId());

		String oldBst = String.valueOf(bst);

		Long oldId = bsts != null ? bsts.getOldBillingId() : 0;
		/* no charge data it means data at staggin now */
		if (bstc.size() <= 0) {
			// BillingSchemeTemplateStag getoldId = new BillingSchemeTemplateStag();
			bsts.setBillingSchemeTemplateName(bst.getBillingSchemeTemplateName());
			bsts.setBillingTemplateApprovalStatus(0);
			bsts.setBillingTemplateId(bst.getBillingTemplateId());
			bsts.setBillingSchemeTemplateType(bst.getBillingSchemeTemplateType());
			bsts.setOldBillingId(oldId);
			List<BillingSchemeTemplateChargeStag> bscs = new ArrayList<BillingSchemeTemplateChargeStag>();
			if (bsts.getBillingSchemeTemplateType().toUpperCase().equals("TIER")) {
				stagChargesService.deleteByBillingScheme(bsts);
				stagService.delete(bsts.getBillingTemplateId());
				BillingSchemeTemplateStag nbsts = stagService.saveData(bsts);
				for (BillingSchemeTemplateCharge bstcl : data) {
					BillingSchemeTemplateChargeStag nbscs = new BillingSchemeTemplateChargeStag();
					nbscs.setBillingTemplateChargeId(bstcl.getBillingTemplateChargeId());
					nbscs.setBillingTemplateCharge(bstcl.getBillingTemplateCharge());
					nbscs.setBillingTemplateLvl(bstcl.getBillingTemplateLvl());
					nbscs.setBillingschemetemplate(nbsts);
					nbscs.setMaxCall(bstcl.getMaxCall());
					nbscs.setMinCall(bstcl.getMinCall());
					bscs.add(nbscs);
				}
				stagChargesService.saveDatas(bscs);
				System.out.print(data.get(0).getBillingschemetemplate().getBillingTemplateId() + "aaaaaaaaaaaaaaa");
				// service.deleteApproved(data.get(0).getBillingschemetemplate().getBillingTemplateId());
				auditData.CreateAuditScheme(username, "Edit billing template", username + " Edit  billing tempalte ",
						date, oldBst, String.valueOf(bscs));
				auditTrailService.saveData(auditData);

				return ResponseEntity.ok().headers(header).body("ok");
			}
			List<BillingSchemeTemplateChargeStag> tmp = stagChargesService
					.getAllByPkId(data.get(0).getBillingschemetemplate().getBillingTemplateId());
			stagChargesService.delete(tmp.get(0).getBillingTemplateChargeId());
			for (BillingSchemeTemplateCharge bstcl : data) {
				BillingSchemeTemplateChargeStag nbscs = new BillingSchemeTemplateChargeStag();
				nbscs.setBillingTemplateChargeId(bstcl.getBillingTemplateChargeId());
				nbscs.setBillingTemplateCharge(bstcl.getBillingTemplateCharge());
				nbscs.setBillingTemplateLvl(bstcl.getBillingTemplateLvl());
				nbscs.setBillingschemetemplate(bsts);
				nbscs.setMaxCall(bstcl.getMaxCall());
				nbscs.setMinCall(bstcl.getMinCall());
				bscs.add(nbscs);
			}
			stagChargesService.update(bscs);
			System.out.print(data.get(0).getBillingschemetemplate().getBillingTemplateId() + "sssssss");
			auditData.CreateAuditScheme(username, "Edit billing template", username + " Edit  billing tempalte ", date,
					oldBst, String.valueOf(bscs));
			auditTrailService.saveData(auditData);

			return ResponseEntity.ok().headers(header).body("ok");
		} else if (bstc.size() > 0) {
			// BillingSchemeTemplate bst = data.get(0).getBillingschemetemplate();
			service.approveOrUnApproveTemplate(bst.getBillingTemplateId(), 5);
			BillingSchemeTemplateStag nbsts = new BillingSchemeTemplateStag();
			nbsts.setBillingSchemeTemplateName(bst.getBillingSchemeTemplateName());
			nbsts.setBillingTemplateApprovalStatus(0);
			nbsts.setBillingSchemeTemplateType(bst.getBillingSchemeTemplateType());
			nbsts.setOldBillingId(bst.getBillingTemplateId());
			// bsts.setBillingTemplateApprovalStatus(5);
			stagService.saveData(nbsts);
			System.out.print(data.get(0).getBillingschemetemplate().getBillingTemplateId() + "cccccc");
			List<BillingSchemeTemplateChargeStag> bscs = new ArrayList<BillingSchemeTemplateChargeStag>();
			for (BillingSchemeTemplateCharge bstcl : data) {
				BillingSchemeTemplateChargeStag nbscs = new BillingSchemeTemplateChargeStag();
				nbscs.setBillingTemplateChargeId(bstcl.getBillingTemplateChargeId());
				nbscs.setBillingTemplateCharge(bstcl.getBillingTemplateCharge());
				nbscs.setBillingTemplateLvl(bstcl.getBillingTemplateLvl());
				nbscs.setBillingschemetemplate(nbsts);
				nbscs.setMaxCall(bstcl.getMaxCall());
				nbscs.setMinCall(bstcl.getMinCall());
				bscs.add(nbscs);
			}
			stagChargesService.saveDatas(bscs);
			auditData.CreateAuditScheme(username, "Edit billing template", username + " Edit  billing tempalte ", date,
					oldBst, String.valueOf(bscs));
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		}
		return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@CrossOrigin
	@PostMapping(path = "/approve", produces = { "application/json" })
	public ResponseEntity<?> approveScheme(@RequestBody BillingSchemeTemplate data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Approve Billing Template",
				username + " approve billing template with id: " + data.getBillingTemplateId(), date);
		auditTrailService.saveData(auditData);
		try {
			data.setBillingTemplateApprovalStatus(1);
			List<BillingSchemeTemplateChargeStag> bscs = stagChargesService.getAllByBillingScheme(data);

			List<BillingSchemeTemplateCharge> bsc = new ArrayList<BillingSchemeTemplateCharge>();
			BillingSchemeTemplate bs = service.saveData(data);
			for (BillingSchemeTemplateChargeStag bscsl : bscs) {
				BillingSchemeTemplateCharge nbsc = new BillingSchemeTemplateCharge();
				nbsc.setBillingTemplateChargeId(bscsl.getBillingTemplateChargeId());
				nbsc.setBillingTemplateCharge(bscsl.getBillingTemplateCharge());
				nbsc.setBillingTemplateLvl(bscsl.getBillingTemplateLvl());
				nbsc.setBillingschemetemplate(bs);
				nbsc.setMaxCall(bscsl.getMaxCall());
				nbsc.setMinCall(bscsl.getMinCall());
				bsc.add(nbsc);
			}
			chargeservice.updateChargeTempalate(bsc);

			stagChargesService.deleteByBillingScheme(bscs.get(0).getBillingschemetemplate());
			stagService.delete(bscs.get(0).getBillingschemetemplate().getBillingTemplateId());

			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/approveEdit", produces = { "application/json" })
	public ResponseEntity<?> approveEditScheme(@RequestBody ApproveBillingModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}

		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		auditData.CreateAudit(username, "Approve Billing Template", username + " approve billing template with id: ",
				date);
		auditTrailService.saveData(auditData);

		try {

			List<BillingSchemeTemplateChargeStag> bscs = stagChargesService.getAllByBillingSchemeId(data.getStagId());

			BillingSchemeTemplate bs = new BillingSchemeTemplate();
			bs = service.approveOrUnApproveTemplate(data.getBillingId(), 1);

			List<BillingSchemeTemplateCharge> bsc = new ArrayList<BillingSchemeTemplateCharge>();
			for (BillingSchemeTemplateChargeStag bscsl : bscs) {
				BillingSchemeTemplateCharge nbsc = new BillingSchemeTemplateCharge();
				nbsc.setBillingTemplateChargeId(bscsl.getBillingTemplateChargeId());
				nbsc.setBillingTemplateCharge(bscsl.getBillingTemplateCharge());
				nbsc.setBillingTemplateLvl(bscsl.getBillingTemplateLvl());
				nbsc.setBillingschemetemplate(bs);
				nbsc.setMaxCall(bscsl.getMaxCall());
				nbsc.setMinCall(bscsl.getMinCall());
				bsc.add(nbsc);
			}
			// chargeservice.updateChargeTempalate(bsc);
			if (bs.getBillingSchemeTemplateType().toUpperCase().equals("TIER")) {
				chargeservice.deleteBySchemeId(bs);
				BillingSchemeTemplate bst = service.saveData(bs);
				for (BillingSchemeTemplateCharge bstc : bsc) {
					bstc.setBillingschemetemplate(bst);
				}
				chargeservice.saveDatas(bsc);
			} else {
				System.out.print(bsc.get(0).getBillingTemplateCharge());
				chargeservice.deleteBySchemeId(bs);
				chargeservice.saveData(bsc.get(0));
			}
			stagChargesService.deleteByBillingScheme(bscs.get(0).getBillingschemetemplate());
			stagService.delete(bscs.get(0).getBillingschemetemplate().getBillingTemplateId());

			System.out.print(data.getBillingId());
			System.out.print(data.getStagId());
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/delete", produces = { "application/json" })
	public ResponseEntity<?> deleteScheme(@RequestBody BillingSchemeTemplate data, HttpServletRequest request)
			throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();

		try {
			List<Subscription> subs = new ArrayList<Subscription>();
			BillingSchemeTemplate existingTemplate = service.getAllById(data.getBillingTemplateId());
			if (existingTemplate != null) {
				List<BillingScheme> existingScheme = billSchemeService.getAllBybillingSchemeTemplate(existingTemplate);
				if (existingScheme != null) {
					for (BillingScheme bs : existingScheme) {
						Subscription newSubs = subsService.getAllByBillingScheme(bs);
						if (newSubs != null) {
							subs.add(newSubs);
						}
					}
				}
			}
			if (!subs.isEmpty()) {
				return ResponseEntity.badRequest().headers(header).body("There are connected apps to this package");
			}

			BillingSchemeTemplateStag bsts = stagService.getAllById(data.getBillingTemplateId());
			if (bsts != null) {
				// bsts.setDeleted(true);
				bsts.setBillingTemplateApprovalStatus(3);
				stagService.delete(bsts);
				auditData.CreateAuditScheme(username, "Delete data",
						username + " Approve delete billing template with id: " + data.getBillingTemplateId(), date,
						String.valueOf(bsts), null);
				auditTrailService.saveData(auditData);
			} else {
				service.delete(data.getBillingTemplateId());
				auditData.CreateAudit(username, " Request Delete data",
						username + " requesting to delete billing template with id: " + data.getBillingTemplateId(),
						date);
				auditTrailService.saveData(auditData);
			}
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "/delete/approve", produces = { "application/json" })
	public ResponseEntity<?> approveDeleteScheme(@RequestBody BillingSchemeTemplate data, HttpServletRequest request)
			throws Exception {

		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		try {
			BillingSchemeTemplate bst = service.getAllById(data.getBillingTemplateId());
			service.deleteApproved(data.getBillingTemplateId());

			auditData.CreateAuditScheme(username, "Delete data",
					username + " Approve delete billing template with id: " + data.getBillingTemplateId(), date,
					String.valueOf(bst), null);
			auditTrailService.saveData(auditData);
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unused")
	@CrossOrigin
	@PostMapping(path = "/decline", produces = { "application/json" })
	public ResponseEntity<?> declineStag(@RequestBody RevisionReqModel data, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role", date);
		auditTrailService.saveData(auditData);
		try {
			BillingSchemeTemplateStag bsts = stagService.getAllById(data.getId());
			
			if (bsts != null) {
				bsts.setComment(data.getComment());
				bsts.setDeleted(true);
				// bsts.setBillingTemplateApprovalStatus(2);
				stagService.delete(bsts);
				if (bsts.getOldBillingId() != 0) {
					service.approveOrUnApproveTemplate(bsts.getOldBillingId(), 1);
					auditData.CreateAuditScheme(username, "Decline new billing package",
							username + "  Decline billing template with id: " + data.getId(), date,
							String.valueOf(bsts), null);
					auditTrailService.saveData(auditData);
				}
				auditData.CreateAuditScheme(username, "Decline edit billing package",
						username + "  Decline billing template with id: " + data.getId(), date, String.valueOf(bsts),
						null);
				auditTrailService.saveData(auditData);
			} 
		
			else {
				service.decline(data.getId());
				auditData.CreateAuditScheme(username, "Decline data",
						username + "  Decline billing template with id: " + data.getId(), date, String.valueOf(bsts),
						null);
				auditTrailService.saveData(auditData);
			}
			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@PostMapping(path = "/revision", produces = { "application/json" })
	public ResponseEntity<?> revisionStag(@RequestBody RevisionReqModel dt, HttpServletRequest request)
			throws Exception {
		tokenFormater tf = new tokenFormater();
		int authStat = authUtil.isAuthority(request, API_GROUP);
		if (authStat == 0) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
		} else if (authStat == 1) {
			return new ResponseEntity<String>("Unauthorized", HttpStatus.FORBIDDEN);
		}
		String rTok = rTokGen.refreshTokenGenerator(request);
		HttpHeaders header = new HttpHeaders();
		if (rTok != null) {
			header.add("RTok", rTok);
		}
		String username = tf.getUsername(request);

		AuditTrail auditData = new AuditTrail();
		LocalDateTime date = LocalDateTime.now();
		auditData.CreateAudit(username, "Access page", username + " Update role", date);
		auditTrailService.saveData(auditData);
		try {
			BillingSchemeTemplateStag bsts = stagService.getAllById(dt.getId());
			// bsts.setDeleted(true);
			bsts.setBillingTemplateApprovalStatus(4);
			bsts.setComment(dt.getComment());
			stagService.updateStat(bsts);
			auditData.CreateAuditScheme(username, "Ask for revision",
					username + " Approve delete billing template with id: " + dt.getId(), date, String.valueOf(bsts),
					null);
			auditTrailService.saveData(auditData);

			return ResponseEntity.ok().headers(header).body("ok");
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
