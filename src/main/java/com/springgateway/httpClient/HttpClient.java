package com.springgateway.httpClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;

import com.springgateway.entity.SyncJob;
import com.springgateway.model.SyncJobRequest;

public class HttpClient {
	CloseableHttpClient httpClient = HttpClients.createDefault();
	
	@Value("${http.auth}")
	static String auth;
	@Value("${http.monetizing.endpoint}")
	static String invoicePath;
	@Value("${http.engine.endpoint}")
	static String syncPath;

	public static StringBuilder proccessRequest(HttpURLConnection myURLConnection) {
		try {
			int responseCode = myURLConnection.getResponseCode();
			InputStream inputStream;
			System.out.println("response Code " + responseCode);
			if (200 <= responseCode && responseCode <= 299) {
				inputStream = myURLConnection.getInputStream();
			} else {
				inputStream = myURLConnection.getErrorStream();
			}
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

			StringBuilder response = new StringBuilder();
			String currentLine;

			while ((currentLine = in.readLine()) != null)
				response.append(currentLine);

			in.close();
			return response;
		} catch (Exception ex) {

		}
		return null;
	}

	public static byte[] proccessBlobRequest(HttpURLConnection myURLConnection) {
		try {
			int responseCode = myURLConnection.getResponseCode();
			InputStream inputStream;

			if (200 <= responseCode && responseCode <= 299) {
				inputStream = myURLConnection.getInputStream();
			} else {
				inputStream = myURLConnection.getErrorStream();
			}

			byte[] rs = IOUtils.toByteArray(inputStream);

			return rs;
		} catch (Exception ex) {

		}
		return null;
	}

	public static String invoiceReq(String startDate, String endDate, String organization, Boolean paid)
			throws IOException {

		URIBuilder builder;
		String urlString = "";
		try {
			System.out.print(paid);
			// builder = new
			// URIBuilder("http://localhost:8083/api/billing/datastore/invoice");
			builder = new URIBuilder(invoicePath + "/invoice");
			if (paid == null) {
				builder.setParameter("startDate", startDate).setParameter("endDate", endDate)
						.setParameter("organizationName", organization);
			} else {
				builder.setParameter("startDate", startDate).setParameter("endDate", endDate)
						.setParameter("organizationName", organization).addParameter("isPaid", String.valueOf(paid));
			}
			urlString = builder.toString();
			URL url = new URL(urlString);

			HttpURLConnection myURLConnection = (HttpURLConnection) url.openConnection();
			myURLConnection.setDoOutput(true);
			myURLConnection.setInstanceFollowRedirects(false);
			myURLConnection.setRequestProperty("Authorization", auth);
			myURLConnection.setRequestProperty("Content-Type", "application/json");
			myURLConnection.setRequestMethod("GET");
			StringBuilder response = proccessRequest(myURLConnection);

			return response.toString();
		} catch (URISyntaxException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;

	}

	public static String invoiceReqDetail(long id) throws IOException {

		URIBuilder builder;
		String urlString = "";
		try {
			// builder = new
			// URIBuilder("http://localhost:8083/api/billing/datastore/invoice/detail");
			builder = new URIBuilder(invoicePath + "/invoice/detail");
			builder.setParameter("id", String.valueOf(id));
			urlString = builder.toString();
			URL url = new URL(urlString);

			HttpURLConnection myURLConnection = (HttpURLConnection) url.openConnection();
			myURLConnection.setDoOutput(true);
			myURLConnection.setInstanceFollowRedirects(false);
			myURLConnection.setRequestProperty("Authorization", auth);
			myURLConnection.setRequestProperty("Content-Type", "application/json");
			myURLConnection.setRequestMethod("GET");
			StringBuilder response = proccessRequest(myURLConnection);

			return response.toString();
		} catch (URISyntaxException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}
	
	public static String resyncProps() throws IOException {

		URIBuilder builder;
		String urlString = "";
		try {
			// builder = new
//			builder = new URIBuilder("http://184.169.41.106/api-billing-engine/rest/reload/parameter");
			builder = new URIBuilder("http://localhost:8081/rest/reload/parameter");
			//builder = new URIBuilder(invoicePath + "/invoice/detail");
			//builder.setParameter("id", String.valueOf(id));
			urlString = builder.toString();
			URL url = new URL(urlString);
			HttpURLConnection myURLConnection = (HttpURLConnection) url.openConnection();
			myURLConnection.setDoOutput(true);
			myURLConnection.setInstanceFollowRedirects(false);
			myURLConnection.setRequestProperty("Authorization", auth);
			myURLConnection.setRequestProperty("Content-Type", "application/json");
			myURLConnection.setRequestMethod("POST");
			StringBuilder response = proccessRequest(myURLConnection);
			return response.toString();
		} catch (URISyntaxException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}
	
	
	
	
	public static byte[] downloadInvoice(int id) throws IOException {
		URIBuilder builder;
		String urlString = "";
		try {
			// builder = new
			// URIBuilder("http://localhost:8083/api/billing/datastore/invoice/download");
			builder = new URIBuilder(invoicePath + "/invoice/download");
			builder.setParameter("id", String.valueOf(id));
			urlString = builder.toString();
			URL url = new URL(urlString);

			HttpURLConnection myURLConnection = (HttpURLConnection) url.openConnection();
			myURLConnection.setDoOutput(true);
			myURLConnection.setInstanceFollowRedirects(false);
			myURLConnection.setRequestProperty("Authorization", auth);

			myURLConnection.setRequestMethod("GET");
			byte[] response = proccessBlobRequest(myURLConnection);

			return response;
		} catch (URISyntaxException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	public static String syncData(SyncJobRequest reqData, SyncJob sj) throws IOException {
		URIBuilder builder;
		String urlString = "";
		String service = sj.getJobUrl();
		try {
			// builder = new
			// URIBuilder("http://localhost:8083/api/billing/datastore/invoice/detail");
			builder = new URIBuilder(syncPath + service);
			urlString = builder.toString();
			URL url = new URL(urlString);
			String params = sj.getParams();
			String urlParameters = "";
			byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
			int postDataLength = postData.length;
			if (params != null) {
				if (reqData.getBefore() == null) {
					if (reqData.getAfter() != null) {
						urlParameters = urlParameters + "date=" + reqData.getAfter() + "&";
					} else if (reqData.getMonth() != null) {
						urlParameters = urlParameters + "month=" + reqData.getMonth() + "&year=" + reqData.getYear()
								+ "&";
					}
					urlParameters = urlParameters + "orgId=" + reqData.getOrgId();
				} else {
					urlParameters = "before=" + reqData.getBefore() + "&after=" + reqData.getAfter() + "&orgId="
							+ reqData.getOrgId();
				}
				postData = urlParameters.getBytes(StandardCharsets.UTF_8);
				postDataLength = postData.length;
			}
			System.out.print(urlParameters);
			HttpURLConnection myURLConnection = (HttpURLConnection) url.openConnection();
			myURLConnection.setDoOutput(true);
			myURLConnection.setInstanceFollowRedirects(false);
			myURLConnection.setRequestProperty("Authorization", auth);
			myURLConnection.setRequestMethod("POST");
			myURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			myURLConnection.setRequestProperty("charset", "utf-8");
			myURLConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));

			OutputStream os = myURLConnection.getOutputStream();
			// byte[] input = reqbody.getBytes("utf-8");
			os.write(postData);
			os.flush();
			os.close();
			int responseCode = myURLConnection.getResponseCode();
			System.out.println("POST Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				// print result
				System.out.println(response.toString());
				return String.valueOf(responseCode);
			} else {
				System.out.println("POST request failed");
				return String.valueOf(responseCode);
			}

		} catch (URISyntaxException e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

}
