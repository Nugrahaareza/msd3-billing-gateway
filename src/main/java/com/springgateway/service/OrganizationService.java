package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.Organization;
import com.springgateway.repository.AuditTrailRepository;
import com.springgateway.repository.OrganizationRepository;

@Service
public class OrganizationService  {
	
	@Autowired
	private OrganizationRepository repository;

	public List<Organization> getAll(int stat) {
		return repository.findByApprovalStatusNot(stat);
	}
	public List<Organization> getAll() {
		return repository.findAll();
	}

	public Organization getAllById(long id) {
		return repository.findByorganizationId(id);
	}

	public Organization saveData(Organization data) {
		return repository.save(data);
	}

	public List<Organization> saveDatas(List<Organization> data) {
		return repository.saveAll(data);
	}

	public void updateStatus(Organization org) {
		Organization existingOrg = repository.findByorganizationId(org.getOrganizationId());
		existingOrg.setApprovalStatus(org.getApprovalStatus());
		repository.save(existingOrg);
		
	}

	public void approveData(Organization org) {
		Organization existingOrg = repository.findByorganizationId(org.getOrganizationId());
		existingOrg.setNotificationEmail(org.getNotificationEmail());
		existingOrg.setNotificationName(org.getNotificationName());
		existingOrg.setApprovalStatus(1);
		repository.save(existingOrg);	
	}

}
