package com.springgateway.service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springgateway.entity.Role;
import com.springgateway.entity.SystemProperties;
import com.springgateway.entity.UserEntity;
import com.springgateway.model.UserModel;
import com.springgateway.repository.SystemPropertiesRepository;
import com.springgateway.repository.UserRepository;

@Service
public class SystemPropertiesService {

	@Autowired
	private  SystemPropertiesRepository repository;
	
	//private static LinkedHashMap<String,String> propsData = new LinkedHashMap<String,String>();
	
	
	
	//public static LinkedHashMap<String, String> getPropsData() {
	//	return propsData;
	//}

	public List<SystemProperties> getAll() {
		return repository.findAll();
	}

	public SystemProperties getAllById(Long long1) {
		return repository.findById(long1);
	}
	
	public  String getAllByName(String name) {
		SystemProperties sp = repository.findByPropertyName(name);
		return sp.getPropertyValue();
	}

	public SystemProperties saveData(SystemProperties data) {
		return repository.save(data);
	}

	public List<SystemProperties> saveDatas(List<SystemProperties> data) {
		return repository.saveAll(data);
	}
	
	public SystemProperties updateProperties(SystemProperties data) {
		SystemProperties existingData = repository.findById(data.getId());
		//existingData.setPropertyName(existingData.getPropertyName());
		existingData.setPropertyValue(data.getPropertyValue());
		existingData.setLastModifiedBy(data.getLastModifiedBy());
		LocalDateTime date = LocalDateTime.now();
		existingData.setLastModified(date);
		return repository.save(existingData);
	}
	
	//@PostConstruct
	//public void getProp() {
	//	SystemProperties existingData = repository.findByPropertyName("JWTTime");
	//	propsData.put("tokenlife", existingData.getPropertyValue());
//		
	//}


	
}
