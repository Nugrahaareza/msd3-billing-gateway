package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.BillingSchemeChargeStag;
import com.springgateway.entity.BillingSchemeStag;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;
import com.springgateway.model.SetBillRequest;
import com.springgateway.repository.BillingSchemeChargeRepository;
import com.springgateway.repository.BillingSchemeChargeStagRepository;
import com.springgateway.repository.BillingSchemeRepository;

@Service
@org.springframework.transaction.annotation.Transactional
public class BillingSchemeChargeStagService {
	@Autowired
	private BillingSchemeChargeStagRepository repository;

	public List<BillingSchemeChargeStag> getAll() {
		return repository.findAll();
	}

	public BillingSchemeChargeStag getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public BillingSchemeChargeStag saveData(BillingSchemeChargeStag user) {
		return repository.save(user);
	}
	
	public List<BillingSchemeChargeStag> saveDatas(List<BillingSchemeChargeStag> user) {
		return repository.saveAll(user);
	}

	public Boolean delete(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public Boolean deleteByBillingId(SetBillRequest req) {
		try {
			BillingSchemeStag bs = req.getBillingSchemeStag();
		
			repository.deleteBybilllingschemeId(bs.getId());
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public List<BillingSchemeChargeStag> getAllBySchemeId(long id) {
	
		return repository.findAllBybilllingschemeId(id);
	}

	public void deleteByBillingSchemeId(long id) {
		repository.deleteBybilllingschemeId(id);

	}

	public void deleteByBillingId(long id) {
	

			repository.deleteBybilllingschemeId(id);

	

	}



}
