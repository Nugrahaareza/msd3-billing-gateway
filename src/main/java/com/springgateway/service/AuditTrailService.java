package com.springgateway.service;

import org.springframework.stereotype.Service;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.BillingScheme;
import com.springgateway.repository.AuditTrailRepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Service
public class AuditTrailService {
	@Autowired
	private AuditTrailRepository repository;

	public List<AuditTrail> getAll() {
		//return repository.findAll();
		Page data = repository.findAll(PageRequest.of(0,10000,Sort.by(Sort.Direction.DESC, "date")));
		if(data != null && data.hasContent()) {
			return data.getContent();
		}
		return null;
	}

	public AuditTrail getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public AuditTrail saveData(AuditTrail data) {
		return repository.save(data);
	}

	public List<AuditTrail> saveDatas(List<AuditTrail> data) {
		return repository.saveAll(data);
	}

	public List<AuditTrail> getAllWithDate(LocalDateTime startLocalDateTime, LocalDateTime endLocalDateTime) {
		
		return repository.findTop10000ByDateBetweenOrderByDateDesc(startLocalDateTime,endLocalDateTime);
	}
	
	

}
