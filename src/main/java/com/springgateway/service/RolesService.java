package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.DetailAppsOrg;
import com.springgateway.entity.Role;
import com.springgateway.entity.Subscription;
import com.springgateway.model.RoleModel;
import com.springgateway.model.SetBillRequest;
import com.springgateway.repository.RoleRepository;

@Service
public class RolesService {

	@Autowired
	private RoleRepository repository;

	public List<Role> getAll() {
		return repository.findAll();
	}

	public Role getAllById(long id) {
		return repository.findById(id);
	}

	public Role saveData(Role data) {
		return repository.save(data);
	}

	public List<Role> saveDatas(List<Role> data) {
		return repository.saveAll(data);
	}

	public void deleteByRole(Long long1) {

		repository.deleteById(long1);

	}

	public List<RoleModel> getAllRoles() {
		return repository.getAllRoles();
	}

	public void update(Role role) {
		System.out.println(")))))");
		System.out.println(role.getId());
		Role existingRole = repository.findById(role.getId());
		existingRole.setApprovalStatus(role.getApprovalStatus());
		repository.save(existingRole);
	}

	public Role getAllByRoleName(String roleName) {
		return repository.findByRoleName(roleName);
	}

}
