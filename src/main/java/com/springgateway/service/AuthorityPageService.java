package com.springgateway.service;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.AuthorityPage;
import com.springgateway.entity.Pages;
import com.springgateway.entity.Role;
import com.springgateway.model.PageRole;
import com.springgateway.repository.AuthorityPageRepository;
import com.springgateway.repository.PageRepository;
import com.springgateway.repository.RoleRepository;

@Service
public class AuthorityPageService {
	
	@Autowired
	private AuthorityPageRepository repository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	public List<AuthorityPage> getAll() {
		return repository.findAll();
	}

	public AuthorityPage getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public AuthorityPage saveData(AuthorityPage data) {
		return repository.save(data);
	}

	public List<AuthorityPage> saveDatas(List<AuthorityPage> data) {
		return repository.saveAll(data);
	}

	public List<PageRole> getPages(String role) {
		System.out.println(role);
		Role rl = roleRepository.findByRoleName(role);
		return repository.getPagesRole(rl.getId());
	}
	
	public List<PageRole> getRolesDetail(int role) {
		System.out.println(role);
		return repository.getRolesDetail(role);
	}
	
	public String[] getRolesGroupAPI(String group){
		
		List<String> roleList = repository.getRolesDetail(group);
		String[] roles = new String[roleList.size()];	
		roles = roleList.toArray(roles);
		return roles;
	}
	
	
	public Boolean deleteByRole(long id) {
	
		Boolean deleted = true;
		try {
			repository.deleteByrolePk(id);
		}
		catch (Exception ex){
			System.out.println(ex);
			deleted = false;
		}
		return deleted;
	}

	public List<PageRole> getRolesStagDetail(int id) {
		System.out.println(id);
		return repository.getRoleStagDetail(id);
	}

	public List<AuthorityPage> getAllByRolePk(long id) {
		return repository.findByRolePk(id);
	}
	

}
