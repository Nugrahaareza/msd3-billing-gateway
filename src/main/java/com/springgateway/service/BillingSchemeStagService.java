package com.springgateway.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeStag;
import com.springgateway.model.MyUserDetails;
import com.springgateway.model.SetBillRequest;
import com.springgateway.repository.BillingSchemeRepository;
import com.springgateway.repository.BillingSchemeStagRepository;

@Service
public class BillingSchemeStagService {
	@Autowired
	private BillingSchemeRepository repository;

	@Autowired
	private BillingSchemeStagRepository stagRepository;

	public List<BillingScheme> getAll() {
		return repository.findAll();
	}

	public BillingSchemeStag getAllById(long l) {
		return stagRepository.findById(l).orElse(null);
	}

	public BillingSchemeStag getAllStagById(long l) {
		return stagRepository.findById(l).orElse(null);
	}

	public BillingSchemeStag saveData(BillingSchemeStag bsst) {
		return stagRepository.save(bsst);
	}

	public List<BillingScheme> saveDatas(List<BillingScheme> user) {
		return repository.saveAll(user);
	}

	public Boolean delete(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public BillingScheme updateProductType(SetBillRequest req) {
		BillingScheme bsreq = req.getBillingScheme();

		BillingScheme existingProduct = repository.findAllByid(bsreq.getId());

		existingProduct.setSchemeType(bsreq.getSchemeType());

		return repository.save(existingProduct);
	}

	public Boolean approve(BillingScheme req) {
		BillingScheme existingProduct = repository.findAllByid(req.getId());
		existingProduct.setApprovalStatus(1);
		;
		try {
			repository.save(existingProduct);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}

	}

	public void deleteByBillingSchemeId(long id) {
		repository.deleteById(id);

	}

	public void deleteById(long id) {
		stagRepository.deleteById(id);

	}

	public void decline(BillingSchemeStag data) {
		BillingSchemeStag existingProduct = stagRepository.findAllByid(data.getId());
		existingProduct.setApprovalStatus(data.getApprovalStatus());

		try {
			stagRepository.save(existingProduct);

		} catch (Exception err) {
			System.out.println(err);
		}
	}

	public void updateStatus(BillingSchemeStag data) {
		BillingSchemeStag existingProduct = stagRepository.findAllByid(data.getId());
		existingProduct.setOldBillingId(data.getOldBillingId());
		existingProduct.setApprovalStatus(data.getApprovalStatus());
		existingProduct.setComment(data.getComment());
		existingProduct.setSchemeType(data.getSchemeType());
		existingProduct
				.setBillingSchemeTemplate(data.getBillingSchemeTemplate() == null ? data.getBillingSchemeTemplate()
						: existingProduct.getBillingSchemeTemplate());
		try {
			stagRepository.save(existingProduct);

		} catch (Exception err) {
			System.out.println(err);
		}

	}

	public BillingSchemeStag getAllByOldId(long id) {
		return stagRepository.findAllByOldBillingId(id);
	}

	public void updateResource(BillingSchemeStag bss) {
		BillingSchemeStag existingProduct = stagRepository.findAllByid(bss.getId());
		existingProduct.setApprovalStatus(0);
		existingProduct.setBillingSchemeTemplate(bss.getBillingSchemeTemplate());
		stagRepository.save(existingProduct);

	}

}
