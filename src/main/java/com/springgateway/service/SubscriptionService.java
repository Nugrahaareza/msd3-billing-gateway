package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeStag;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.DetailAppsOrg;
import com.springgateway.entity.Subscription;
import com.springgateway.model.ApprovalStatus;
import com.springgateway.model.SetBillRequest;
import com.springgateway.model.SubscriptionStatus;
import com.springgateway.repository.SubscriptionRepository;

@Service
public class SubscriptionService {

	@Autowired
	private SubscriptionRepository repository;

	private SubscriptionStatus SubStat;

	private ApprovalStatus AppStat;

	public List<Subscription> getAll() {
		return repository.findAll();
	}

	public Subscription getAllById(String string) {
		return repository.findById(string).orElse(null);
	}

	public Subscription saveData(Subscription data) {
		return repository.save(data);
	}

	public List<Subscription> saveDatas(List<Subscription> data) {
		return repository.saveAll(data);
	}

	public Subscription addBillSchemeStag(SetBillRequest req) {
		Subscription existingProduct = repository.findAllBysubscriptionId(req.getSubsId());
		existingProduct.setBillingschemeStag(req.getBillingSchemeStag());
		existingProduct.setSubscriptionStatus(req.getBillingStatus());

		return repository.save(existingProduct);
	}

	public Subscription addBillScheme(SetBillRequest req) {
		Subscription existingProduct = repository.findAllBysubscriptionId(req.getSubsId());

		existingProduct.setBillingscheme(req.getBillingScheme());
		return repository.save(existingProduct);
	}

	public List<Subscription> getAllByOrgId(long id) {

		return repository.findAllByOrgPk(id);
	}

	public Subscription getAppDetail(String id) {

		return repository.findAllBysubscriptionId(id);
	}

	public void moveStagToScheme(long id, BillingScheme bs) {
		Subscription existingProduct = repository.findAllBybillingschemeStagId(id);
		existingProduct.setBillingschemeStag(null);
		existingProduct.setBillingscheme(bs);
		try {
			repository.save(existingProduct);
		} catch (Exception err) {
			System.out.println(err);

		}
	}

	public Subscription getAllBySubsId(String subscriptionId) {
		return repository.findAllBysubscriptionId(subscriptionId);
	}

	public Subscription updateAllData(Subscription subs) {
		Subscription existingProduct = repository.findAllBysubscriptionId(subs.getSubscriptionId());
		existingProduct.setBillingschemeStag(subs.getBillingschemeStag());
		existingProduct.setBillingscheme(subs.getBillingscheme());
		try {
			repository.save(existingProduct);

		} catch (Exception err) {
			System.out.println(err);

		}
		return null;
	}

	public void decline(BillingScheme data) {
		Subscription existingProduct = repository.findAllBybillingschemeStagId(data.getId());
		if (data.getApprovalStatus() == 3) {
			existingProduct.setSubscriptionStatus(SubStat.NOT_SET);
			existingProduct.setBillingschemeStag(null);
		} else {
			System.out.println("---------------");
			existingProduct.setSubscriptionStatus(SubStat.SET);
		}

		try {
			repository.save(existingProduct);

		} catch (Exception err) {
			System.out.println(err);

		}

	}

	public Subscription updateBillSchemeStag(SetBillRequest req) {
		Subscription existingProduct = repository.findAllBysubscriptionId(req.getSubsId());

		existingProduct.setBillingscheme(req.getBillingScheme());
		return repository.save(existingProduct);
	}

	public void removeBillStagService(String subsId) {
		Subscription existingProduct = repository.findAllBysubscriptionId(subsId);
		existingProduct.setBillingschemeStag(null);
		return;

	}

	public Subscription getAllByBillingScheme(BillingScheme bs) {
		return repository.findBybillingscheme(bs);
	}

	public Subscription updateSubsStatus(Subscription existingSubs) {
		Subscription existingProduct = repository.findAllBysubscriptionId(existingSubs.getSubscriptionId());
		existingProduct.setSubscriptionStatus(existingSubs.getSubscriptionStatus());
		existingProduct.setApprovalStatus(existingSubs.getApprovalStatus());
		repository.save(existingProduct);
		return existingProduct;
	}

	public Subscription getAllByBillingSchemeOrBillingSchemeStag(long id) {
		Subscription subs = repository.findBybillingschemeId(id);
		if(subs !=null) {
			return subs;
		}else {
			subs = repository.findBybillingschemeStagId(id);
			return subs;
		}
	}

	

	public void updateApproval(Subscription subs) {
		Subscription existingProduct = repository.findAllBysubscriptionId(subs.getSubscriptionId());
		existingProduct.setApprovalStatus(subs.getApprovalStatus());
		repository.save(existingProduct);
	}

}
