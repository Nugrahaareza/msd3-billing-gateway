package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.Organization;
import com.springgateway.entity.OrganizationStag;
import com.springgateway.repository.AuditTrailRepository;
import com.springgateway.repository.OrganizationRepository;
import com.springgateway.repository.OrganizationStagRepository;

@Service
public class OrganizationStagService  {
	
	@Autowired
	private OrganizationStagRepository repository;

	public List<OrganizationStag> getAll(int stat) {
		return repository.findByApprovalStatusNot(stat);
	}

	public OrganizationStag getAllById(long id) {
		return repository.findByorganizationId(id);
	}

	public OrganizationStag saveData(OrganizationStag orgStag) {
		return repository.save(orgStag);
	}

	public List<OrganizationStag> saveDatas(List<OrganizationStag> data) {
		return repository.saveAll(data);
	}

	public void delete(OrganizationStag orgStag) {
		repository.deleteByorganizationId(orgStag.getOrganizationId());
		
	}

	public void updateData(OrganizationStag orgStag) {
		OrganizationStag existingData = repository.findByorganizationId(orgStag.getOrganizationId());
		existingData.setComment(orgStag.getComment());
		existingData.setApprovalStatus(orgStag.getApprovalStatus());
		repository.save(existingData);
		
	}

	public void updateStagData(OrganizationStag orgStag) {
		OrganizationStag existingData = repository.findByorganizationId(orgStag.getOrganizationId());
		existingData.setNotificationEmail(orgStag.getNotificationEmail());
		existingData.setNotificationName(orgStag.getNotificationName());
		repository.save(existingData);
		
	}

}
