package com.springgateway.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.springgateway.entity.AuditTrail;
import com.springgateway.entity.PaymentMessageAuditTrail;
import com.springgateway.repository.AuditTrailRepository;
import com.springgateway.repository.PaymentMessageAuditTrailRepository;

@Service
public class PaymentMessageAuditTrailService {
	@Autowired
	private PaymentMessageAuditTrailRepository repository;

	public List<PaymentMessageAuditTrail> getAll() {
		//return repository.findAll();
		Page data = repository.findAll(PageRequest.of(0,10000,Sort.by(Sort.Direction.DESC, "requestTimestamp")));
		if(data != null && data.hasContent()) {
			return data.getContent();
		}
		return null;
	}

	public PaymentMessageAuditTrail getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public PaymentMessageAuditTrail saveData(PaymentMessageAuditTrail data) {
		return repository.save(data);
	}

	public List<PaymentMessageAuditTrail> saveDatas(List<PaymentMessageAuditTrail> data) {
		return repository.saveAll(data);
	}

	public List<PaymentMessageAuditTrail> getAllWithDate(LocalDateTime startLocalDateTime, LocalDateTime endLocalDateTime) {
		
		return repository.findTop10000ByRequestTimestampBetweenOrderByRequestTimestampDesc(startLocalDateTime,endLocalDateTime);
	}
}
