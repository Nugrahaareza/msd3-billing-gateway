package com.springgateway.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springgateway.entity.Privilege;
import com.springgateway.entity.Role;
import com.springgateway.entity.UserEntity;
import com.springgateway.entity.UserStag;
import com.springgateway.model.MyUserDetails;
import com.springgateway.model.UserModel;
import com.springgateway.repository.UserRepository;
import com.springgateway.repository.UserStagRepository;

@Service
public class UserStagService {
	@Autowired
	private UserStagRepository repository;

	public List<UserStag> getAll() {
		return repository.findAll();
	}

	public UserStag saveData(UserStag user) {

		return repository.save(user);
	}

	public List<UserStag> saveDatas(List<UserStag> user) {
		return repository.saveAll(user);
	}

	public List<UserModel> getAllWithRole() {
		return repository.getAllWithRoles();
	}

	public UserStag getAllById(Long id) {
		return repository.findById(id);
	}

	public void deleteById(long id) {
		repository.deleteById(id);

	}

	public UserStag updatePassCount(UserStag userData) {
		UserStag existingUser = repository.findByUsername(userData.getUsername());
		existingUser.setWrongPassCount(userData.getWrongPassCount());
		return repository.save(existingUser);
	}
	
	public UserStag updateUser(UserStag userData) {
		UserStag existingUser = repository.findByUsername(userData.getUsername());
		existingUser.setWrongPassCount(userData.getWrongPassCount());
		existingUser.setActive(userData.isActive() == null ? existingUser.isActive() : userData.isActive());
		existingUser.setEmail(userData.getEmail() == null ? existingUser.getEmail() : userData.getEmail());
		existingUser.setApprovalStatus(userData.getApprovalStatus());
		if(userData.getRoles()!=null) {
		existingUser.getRoles().removeAll(existingUser.getRoles());
		existingUser.getRoles().addAll(userData.getRoles());
		}
		return repository.save(existingUser);
	}

	public UserStag findByUsername(String username) {
		return repository.findByUsername(username);
	}

	public UserStag updateRoles(UserStag user) {
		UserStag data = repository.findByUsername(user.getUsername());
		data.getRoles().removeAll(data.getRoles());
		data.getRoles().addAll(user.getRoles());

		return repository.save(data);
	}

	public void deleteByUsername(String username) {
		repository.deleteByUsername(username);
		
	}

	public void updateStat(UserStag userStag) {
	
		UserStag data = repository.findById(userStag.getId());
		data.setApprovalStatus(userStag.getApprovalStatus());
		 repository.save(data);
	}



}
