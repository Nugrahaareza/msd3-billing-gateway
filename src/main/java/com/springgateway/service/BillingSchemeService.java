package com.springgateway.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.model.MyUserDetails;
import com.springgateway.model.SetBillRequest;
import com.springgateway.repository.BillingSchemeRepository;

@Service
public class BillingSchemeService {
	@Autowired
	private BillingSchemeRepository repository;

	public List<BillingScheme> getAll() {
		return repository.findAll();
	}

	public BillingScheme getAllById(long l) {
		return repository.findById(l).orElse(null);
	}

	public BillingScheme saveData(BillingScheme user) {
		return repository.save(user);
	}

	public List<BillingScheme> saveDatas(List<BillingScheme> user) {
		return repository.saveAll(user);
	}

	public Boolean delete(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public BillingScheme updateProductType(SetBillRequest req) {
		BillingScheme bsreq = req.getBillingScheme();
		System.out.println("bsreq");
		System.out.println(bsreq);
		BillingScheme existingProduct = repository.findAllByid(bsreq.getId());

		existingProduct.setSchemeType(bsreq.getSchemeType());

		return repository.save(existingProduct);
	}

	public Boolean approve(BillingScheme req) {
		BillingScheme existingProduct = repository.findAllByid(req.getId());
		existingProduct.setApprovalStatus(1);
		try {
			repository.save(existingProduct);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}

	}

	public void unapprove(BillingScheme bilsch) {
		BillingScheme existingProduct = repository.findAllByid(bilsch.getId());
		existingProduct.setApprovalStatus(0);
		try {
			repository.save(existingProduct);

		} catch (Exception err) {
			System.out.println(err);

		}

	}

	public void deleteById(long id) {

		repository.deleteById(id);

	}

	public List<BillingScheme> getAllBybillingSchemeTemplate(BillingSchemeTemplate existingTemplate) {
		return repository.findByBillingSchemeTemplate(existingTemplate);
	}

}
