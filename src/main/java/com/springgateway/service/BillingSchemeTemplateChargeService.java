package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.repository.BillingSchemeRepository;
import com.springgateway.repository.BillingSchemeTemplateChargeRepository;

@Transactional
@Service
public class BillingSchemeTemplateChargeService {

	@Autowired
	private BillingSchemeTemplateChargeRepository repository;

	Boolean isSuccess = false;

	public List<BillingSchemeTemplateCharge> getAll() {
		return repository.findAll();
	}

	public BillingSchemeTemplateCharge getAllById(int l) {
		return repository.findById(l).orElse(null);
	}

	public List<BillingSchemeTemplateCharge> getAllByPKId(long id) {
		return repository.findBybillingschemetemplateBillingTemplateId(id);
	}

	public Boolean updateChargeTempalate(List<BillingSchemeTemplateCharge> schemes) {
		try {
			schemes.forEach(scheme -> {
				BillingSchemeTemplateCharge existingProduct = repository
						.findBybillingTemplateChargeId(scheme.getBillingTemplateChargeId());
				if (existingProduct != null) {
					existingProduct.setBillingTemplateLvl(scheme.getBillingTemplateLvl());
					existingProduct.setBillingTemplateCharge(scheme.getBillingTemplateCharge());
					existingProduct.setMaxCall(scheme.getMaxCall());
					existingProduct.setMinCall(scheme.getMinCall());
					repository.save(existingProduct);
				} else if (existingProduct == null) {
					repository.save(scheme);
				}
				isSuccess = true;
			});
		} catch (Exception e) {
			System.out.println(e);
			isSuccess = false;
		}
		return null;
	}

	@Transactional
	public void updateChargeTempalateByScheme(List<BillingSchemeTemplateCharge> bsc) {
		try {
			if (bsc.get(0).getBillingschemetemplate().getBillingSchemeTemplateType().toUpperCase().equals("TIER")) {
				deleteBySchemeId(bsc.get(0).getBillingschemetemplate());
				saveDatas(bsc);
			} else {
				bsc.forEach(scheme -> {
					BillingSchemeTemplateCharge existingProduct = repository
							.findBybillingschemetemplate(scheme.getBillingschemetemplate());
					if (existingProduct != null) {
						existingProduct.setBillingTemplateLvl(scheme.getBillingTemplateLvl());
						existingProduct.setBillingTemplateCharge(scheme.getBillingTemplateCharge());
						existingProduct.setMaxCall(scheme.getMaxCall());
						existingProduct.setMinCall(scheme.getMinCall());
						repository.save(existingProduct);
					} else if (existingProduct == null) {
						repository.save(scheme);
					}
					isSuccess = true;
				});
			}
		} catch (Exception e) {
			System.out.println(e);
			isSuccess = false;
		}

	}

	public BillingSchemeTemplateCharge saveData(BillingSchemeTemplateCharge user) {
		return repository.save(user);
	}

	public List<BillingSchemeTemplateCharge> saveDatas(List<BillingSchemeTemplateCharge> user) {
		return repository.saveAll(user);
	}

	public Boolean delete(BillingSchemeTemplate id) {
		try {
			BillingSchemeTemplateCharge existingProduct = repository
					.findBybillingschemetemplate(id);
			//repository.deleteById(existingProduct.getBillingTemplateChargeId());
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public Boolean delete(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public void deleteBySchemeId(BillingSchemeTemplate billingSchemeTemplate) {
		try {
			repository.deleteBybillingschemetemplate(billingSchemeTemplate);
		} catch (Exception err) {
			System.out.println(err);

		}
	}

}
