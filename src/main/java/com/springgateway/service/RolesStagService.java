package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.DetailAppsOrg;
import com.springgateway.entity.RoleStag;
import com.springgateway.entity.Subscription;
import com.springgateway.model.RoleModel;
import com.springgateway.model.SetBillRequest;
import com.springgateway.repository.RoleRepository;
import com.springgateway.repository.RoleStagRepository;


@Service
public class RolesStagService {

	@Autowired
	private RoleStagRepository repository;
	
	public List<RoleStag> getAll() {
		return repository.findAll();
	}

	public RoleStag getAllById(long id) {
		return repository.findById(id);
	}

	public RoleStag saveData(RoleStag data) {
		return repository.save(data);
	}

	public List<RoleStag> saveDatas(List<RoleStag> data) {
		return repository.saveAll(data);
	}
	public Boolean deleteByRole(Long long1) {
		Boolean deleted = true;
		try {
			repository.deleteById(long1);
		}
		catch (Exception ex){
			System.out.println(ex);
			deleted = false;
		}
		return deleted;
	}

	public List<RoleModel> getAllRoles() {
		return repository.getAllRoles();
	}

	public void update(RoleStag rs) {
		RoleStag rsExist = repository.findById(rs.getId());
		rsExist.setApprovalStatus(rs.getApprovalStatus());
		repository.save(rsExist);
		
	}

	public void revision(RoleStag roleStag) {
		RoleStag rsExist = repository.findById(roleStag.getId());
		rsExist.setComment(roleStag.getComment());
		rsExist.setApprovalStatus(roleStag.getApprovalStatus());
		repository.save(rsExist);
		
	}



}
