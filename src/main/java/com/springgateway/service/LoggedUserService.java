package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.LoggedUser;
import com.springgateway.repository.LoggedUserRepository;



@Service
public class LoggedUserService {

	@Autowired
	private LoggedUserRepository repository;

	public List<LoggedUser> getAll() {
		return repository.findAll();
	}

	public LoggedUser getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public LoggedUser saveData(LoggedUser data) {
		return repository.save(data);
	}

	public List<LoggedUser> saveDatas(List<LoggedUser> data) {
		return repository.saveAll(data);
	}

	public void delete(String token) {
		repository.deleteByToken(token);
		
	}

	public Boolean getByToken(String token) {
		Boolean isFound = false;
		LoggedUser  logus =repository.findByToken(token);
		
		if(logus != null) {
			return isFound = true;
		}
		return isFound;
	}
	
}
