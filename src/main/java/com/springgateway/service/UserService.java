package com.springgateway.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springgateway.entity.Privilege;
import com.springgateway.entity.Role;
import com.springgateway.entity.UserEntity;
import com.springgateway.model.MyUserDetails;
import com.springgateway.model.UserModel;
import com.springgateway.model.UserRoles;
import com.springgateway.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {
	@Autowired
	private UserRepository repository;

	public List<UserEntity> getAll() {
		return repository.findAll();
	}

	public UserEntity getAllById(Long long1) {
		return repository.findById(long1).orElse(null);
	}

	public UserEntity getAllEmailByChecker() {
		return repository.findByEmailChecker();
	}

	public UserEntity findByEmail(String username) {
		return repository.findByEmail(username);
	}

	public UserEntity findByUsernameandPassword(String username, String password) {
		return repository.findByUsernameandPassword(username, password);
	}

	public UserEntity findByUsername(String username) {
		return repository.findByUsername(username);
	}

	public UserEntity saveData(UserEntity user) {

		return repository.save(user);
	}

	public List<UserEntity> saveDatas(List<UserEntity> user) {
		return repository.saveAll(user);
	}

	public UserEntity updateRoles(UserEntity user) {
		UserEntity data = repository.findByUsernameOnly(user.getUsername());
		data.getRoles().removeAll(data.getRoles());
		data.getRoles().addAll(user.getRoles());

		return repository.save(data);
	}

	public void delete(String username) {
		UserEntity existData = repository.findByUsernameOnly(username);
		//existData.setActive(false);
		//repository.save(existData);
		repository.deleteByUsername(username);
	}

	public List<UserModel> getAllWithRole() {
		return repository.getAllWithRoles();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = repository.findByUsername(username);
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.isActive(),
				true, true, true, getAuthorities(user.getRoles()));
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {

		return getGrantedAuthorities(getPrivileges(roles));
	}

	private List<String> getPrivileges(Collection<Role> roles) {

		List<String> privileges = new ArrayList<>();
		List<Privilege> collection = new ArrayList<>();
		for (Role role : roles) {
			collection.addAll(role.getPrivileges());
		}
		for (Privilege item : collection) {
			privileges.add(item.getName());
		}
		return privileges;
	}

	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (String privilege : privileges) {
			authorities.add(new SimpleGrantedAuthority(privilege));
		}
		return authorities;
	}

	public UserEntity updateUser(UserEntity user) {
		UserEntity existingUser = repository.findByUsernameOnly(user.getUsername());
		existingUser.setWrongPassCount(user.getWrongPassCount());
		existingUser.setActive(user.isActive() == null ? existingUser.isActive() : user.isActive());
		existingUser.setEmail(user.getEmail() == null ? existingUser.getEmail() : user.getEmail());
		existingUser.setApprovalStatus(
				user.getApprovalStatus() <= 0 ? existingUser.getApprovalStatus() : user.getApprovalStatus());
		return repository.save(existingUser);
	}

	public void updatePassword(UserEntity user) {
		UserEntity existingUser = repository.findByUsernameOnly(user.getUsername());
		existingUser.setPassword(user.getPassword());
		existingUser.setTokenResetPass(user.getTokenResetPass());
		existingUser.setRandPass(user.isRandPass());
		repository.save(existingUser);
	}

	public List<UserRoles> findByRole(long id) {
		System.out.println(id);
		return repository.findByRoles(id);
	}

	public void resetAttempt(UserEntity user) {
		UserEntity existingUser = repository.findByUsernameOnly(user.getUsername());
		existingUser.setWrongPassCount(0);
		repository.save(existingUser);
		
	}

	public void setExpireToken(UserEntity user) {
		UserEntity existingUser = repository.findByUsernameOnly(user.getUsername());
		existingUser.setApprovalStatus(user.getApprovalStatus());
		existingUser.setActive(user.isActive());
		repository.save(existingUser);
		
	}

}
