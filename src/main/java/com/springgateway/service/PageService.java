package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.Pages;
import com.springgateway.entity.Role;
import com.springgateway.repository.PageRepository;
import com.springgateway.repository.RoleRepository;

@Service
public class PageService {

	@Autowired
	private PageRepository repository;
	
	public List<Pages> getAll() {
		return repository.findAll();
	}

	public Pages getAllById(long l) {
		return repository.findById(l).orElse(null);
	}

	public Pages saveData(Pages data) {
		return repository.save(data);
	}

	public List<Pages> saveDatas(List<Pages> data) {
		return repository.saveAll(data);
	}

	public Pages findbyLabel(String label) {
		return repository.findBylabel(label);
		
	}
	
}
