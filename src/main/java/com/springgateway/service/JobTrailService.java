package com.springgateway.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.springgateway.entity.JobTrail;

import com.springgateway.repository.JobTrailRepository;


@Service
public class JobTrailService {
	@Autowired
	private JobTrailRepository repository;

	public List<JobTrail> getAll() {
		return repository.findAll();
	}

	public JobTrail getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public List<JobTrail> getAllByDate(LocalDateTime startLocalDate, LocalDateTime endLocalDate) {
		
		return repository.findAllByStartDateBetween(startLocalDate,endLocalDate);
	}

	public List<JobTrail> getTop100Desc() {
		Page data = repository.findAll(PageRequest.of(0,100,Sort.by(Sort.Direction.DESC, "startDate")));
		if(data != null && data.hasContent()) {
			return data.getContent();
		}
		return null;
	}

	
}
