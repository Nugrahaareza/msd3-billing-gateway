package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.PaymentTransaction;
import com.springgateway.repository.BillingSchemeTemplateRepository;
import com.springgateway.repository.PaymentTransactionRepository;
import com.springgateway.repository.PaymentTransactionRepository;

@Service
public class PaymentTransactionService {
	@Autowired
	private PaymentTransactionRepository repository;

	public List<PaymentTransaction> getAll() {
		return repository.findAll();
	}

	public PaymentTransaction saveData(PaymentTransaction user) {
		return repository.save(user);
	}

	public List<PaymentTransaction> saveDatas(List<PaymentTransaction> user) {
		return repository.saveAll(user);
	}

	

	

	// public List<PaymentTransaction> getAllBySchemeId(long id) {
	// return repository.findByorganizationOrganizationId(id);
	// }

}
