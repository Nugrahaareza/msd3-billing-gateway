package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.repository.BillingSchemeRepository;
import com.springgateway.repository.BillingSchemeTemplateRepository;

@Service
@Transactional
public class BillingSchemeTemplateService {
	@Autowired
	private BillingSchemeTemplateRepository repository;

	public List<BillingSchemeTemplate> getAll() {
		return repository.findAll();
	}

	public List<BillingSchemeTemplate> getAllNoDeleted() {
		return repository.findAllNotDeleted();
	}

	public BillingSchemeTemplate getAllById(long id) {
		return repository.findBybillingTemplateId(id).orElse(null);
	}

	public BillingSchemeTemplate saveData(BillingSchemeTemplate user) {
		return repository.save(user);
	}

	public List<BillingSchemeTemplate> saveDatas(List<BillingSchemeTemplate> user) {
		return repository.saveAll(user);
	}

	public BillingSchemeTemplate approveOrUnApproveTemplate(long id, int stat) {

		BillingSchemeTemplate existingProduct = repository.findBybillingTemplateId(id).orElse(null);
		existingProduct.setBillingTemplateApprovalStatus(stat);
		return repository.save(existingProduct);
	}

	public void delete(long l) {
		BillingSchemeTemplate existingProduct = repository.findBybillingTemplateId(l).orElse(null);
		existingProduct.setBillingTemplateApprovalStatus(3);
		repository.save(existingProduct);

	}

	public void deleteApproved(long billingTemplateId) {
		BillingSchemeTemplate existingProduct = repository.findBybillingTemplateId(billingTemplateId).orElse(null);
		
		existingProduct.setDeleted(true);
		repository.save(existingProduct);
	}

	public void decline(long id) {
		BillingSchemeTemplate existingProduct = repository.findBybillingTemplateId(id).orElse(null);
		existingProduct.setBillingTemplateApprovalStatus(1);
		repository.save(existingProduct);
		
	}


}
