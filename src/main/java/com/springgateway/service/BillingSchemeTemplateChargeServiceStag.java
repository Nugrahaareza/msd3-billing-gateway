package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateCharge;
import com.springgateway.entity.BillingSchemeTemplateChargeStag;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.repository.BillingSchemeRepository;
import com.springgateway.repository.BillingSchemeTemplateChargeRepository;
import com.springgateway.repository.BillingSchemeTemplateChargeRepositoryStag;

@Transactional
@Service
public class BillingSchemeTemplateChargeServiceStag {

	@Autowired
	private BillingSchemeTemplateChargeRepositoryStag repository;

	Boolean isSuccess = false;

	public List<BillingSchemeTemplateChargeStag> getAll() {
		return repository.findAll();
	}

	public BillingSchemeTemplateChargeStag getAllById(int id) {
		return repository.findById(id).orElse(null);
	}


	public List<BillingSchemeTemplateChargeStag> saveDatas(List<BillingSchemeTemplateChargeStag> user) {
		return repository.saveAll(user);
	}

	public Boolean delete(long l) {
		try {
			repository.deleteByBillingTemplateChargeId(l);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public List<BillingSchemeTemplateChargeStag> getAllByPkId(long id) {
		return repository.findBybillingschemetemplatestagBillingTemplateId(id);
	}

	public void deleteByBillingScheme(BillingSchemeTemplateStag bss) {
		 repository.deleteBybillingschemetemplatestag(bss);
		return;
	}

	public List<BillingSchemeTemplateChargeStag> getAllByBillingScheme(BillingSchemeTemplate data) {
		
		 	return repository.findBybillingschemetemplatestagBillingTemplateId(data.getBillingTemplateId());
	}

	
	@Transactional
	public void update(List<BillingSchemeTemplateChargeStag> bscs) {
		try {
			bscs.forEach(scheme -> {
				BillingSchemeTemplateChargeStag existingProduct = repository
						.findBybillingTemplateChargeId(scheme.getBillingTemplateChargeId());
				if (existingProduct != null) {
					existingProduct.setBillingTemplateLvl(scheme.getBillingTemplateLvl());
					existingProduct.setBillingTemplateCharge(scheme.getBillingTemplateCharge());
					existingProduct.setMaxCall(scheme.getMaxCall());
					existingProduct.setMinCall(scheme.getMinCall());
					repository.save(existingProduct);
				} else if (existingProduct == null) {
					repository.save(scheme);
				}
				isSuccess = true;
			});
		} catch (Exception e) {
			System.out.println(e);
			isSuccess = false;
		}

		
	}

	public List<BillingSchemeTemplateChargeStag> getAllByBillingSchemeId(long billingtemplateid) {
		return repository.findBybillingschemetemplatestagBillingTemplateId(billingtemplateid);
	}

	public void updateStag(BillingSchemeTemplateChargeStag nbscs) {
		BillingSchemeTemplateChargeStag existingProduct = repository
				.findBybillingTemplateChargeId(nbscs.getBillingTemplateChargeId());
		
		if (existingProduct != null) {
			existingProduct.setBillingTemplateLvl(nbscs.getBillingTemplateLvl());
			existingProduct.setBillingTemplateCharge(nbscs.getBillingTemplateCharge());
			existingProduct.setMaxCall(nbscs.getMaxCall());
			existingProduct.setMinCall(nbscs.getMinCall());
			repository.save(existingProduct);
		} else if (existingProduct == null) {
			repository.save(nbscs);
		}
		
	}


}
