package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.SyncJob;
import com.springgateway.repository.SyncJobRepository;


@Service
public class SyncJobService {

	@Autowired
	private SyncJobRepository repository;

	public List<SyncJob> getAll() {
		return repository.findAll();
	}
	
	public SyncJob saveData(SyncJob data) {
		return repository.save(data);
	}

	public List<SyncJob> saveDatas(List<SyncJob> data) {
		return repository.saveAll(data);
	}

	public SyncJob getAllById(long jobId) {
		return repository.findById(jobId);
	}

}
