package com.springgateway.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.BillingSchemeTemplateStag;
import com.springgateway.repository.BillingSchemeRepository;
import com.springgateway.repository.BillingSchemeTemplateRepository;
import com.springgateway.repository.BillingSchemeTemplateRepositoryStag;

@Service
@Transactional
public class BillingSchemeTemplateServiceStag {
	@Autowired
	private BillingSchemeTemplateRepositoryStag repository;

	public List<BillingSchemeTemplateStag> getAll() {
		return repository.findAllAndOldBillingIdIsNull();
	}

	
	  public List<BillingSchemeTemplateStag> getAllNoDeleted() {
	        return repository.findAllNotDeleted();
	    }
	public BillingSchemeTemplateStag saveData(BillingSchemeTemplateStag user) {
		return repository.save(user);
	}

	public List<BillingSchemeTemplateStag> saveDatas(List<BillingSchemeTemplateStag> user) {
		return repository.saveAll(user);
	}

	public BillingSchemeTemplateStag getAllById(long id) {
		return repository.findBybillingTemplateId(id);
	}

	public Boolean delete(long id) {
		try {
			repository.deleteBybillingTemplateId(id);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}

	}

	public BillingSchemeTemplateStag getAllByOldBillingId(long id) {
		return repository.findAllByOldBillingId(id);
	}

	public void delete(BillingSchemeTemplateStag billTemplateStag) {
		BillingSchemeTemplateStag existingTemplate = repository
				.findBybillingTemplateId(billTemplateStag.getBillingTemplateId());
		existingTemplate.setDeleted(billTemplateStag.isDeleted());
		//existingTemplate.setBillingTemplateApprovalStatus(billTemplateStag.getBillingTemplateApprovalStatus());
		repository.save(existingTemplate);
	}


	public void updateStat(BillingSchemeTemplateStag newBsts) {
		
		
		BillingSchemeTemplateStag existingTemplate = repository.findBybillingTemplateId(newBsts.getBillingTemplateId());
		
		existingTemplate.setBillingTemplateApprovalStatus(newBsts.getBillingTemplateApprovalStatus());
		repository.save(existingTemplate);
		
	}




}
