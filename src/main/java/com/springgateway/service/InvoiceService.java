package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.entity.BillingSchemeTemplate;
import com.springgateway.entity.Invoice;
import com.springgateway.repository.BillingSchemeTemplateRepository;
import com.springgateway.repository.InvoiceRepository;

@Service
public class InvoiceService {
	@Autowired
	private InvoiceRepository repository;

	public List<Invoice> getAll() {
		return repository.findAll();
	}

	public Invoice saveData(Invoice user) {
		return repository.save(user);
	}

	public List<Invoice> saveDatas(List<Invoice> user) {
		return repository.saveAll(user);
	}

	public Invoice findbyorganizationId(long id) {

		return repository.findById(id);
	}

	public List<Invoice> findbyorganizationIdAndPeriod(Invoice req) {
		System.out.println("======");
		System.out.println(req);
		return repository.findByorganizationOrganizationIdAndMonthAndYear(req.getOrganization().getOrganizationId(),req.getMonth(),req.getYear());
	}

	public List<Invoice> findbyPeriod(Invoice req) {
		// TODO Auto-generated method stub
		return repository.findByMonthAndYear(req.getMonth(),req.getYear());
	}

	// public List<Invoice> getAllBySchemeId(long id) {
	// return repository.findByorganizationOrganizationId(id);
	// }

}
