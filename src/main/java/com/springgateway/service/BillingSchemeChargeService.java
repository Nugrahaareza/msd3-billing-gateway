package com.springgateway.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingScheme;
import com.springgateway.entity.BillingSchemeCharge;
import com.springgateway.model.SetBillRequest;
import com.springgateway.repository.BillingSchemeChargeRepository;
import com.springgateway.repository.BillingSchemeRepository;

@Service
public class BillingSchemeChargeService {
	@Autowired
	private BillingSchemeChargeRepository repository;

	public List<BillingSchemeCharge> getAll() {
		return repository.findAll();
	}

	public BillingSchemeCharge getAllById(int id) {
		return repository.findById(id).orElse(null);
	}

	public BillingSchemeCharge saveData(BillingSchemeCharge user) {
		return repository.save(user);
	}

	public List<BillingSchemeCharge> saveDatas(List<BillingSchemeCharge> user) {
		return repository.saveAll(user);
	}

	public Boolean delete(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public Boolean deleteByBillingId(SetBillRequest req) {
		try {
			BillingScheme bs = req.getBillingScheme();

			repository.deleteByBillingId(bs.getId());
			return true;
		} catch (Exception err) {
			System.out.println(err);
			return false;
		}
	}

	public List<BillingSchemeCharge> getAllBySchemeId(long id) {
		System.out.println(repository.findAllBybilllingschemeId(id));
		return repository.findAllBybilllingschemeId(id);
	}

	public void deleteByBillingId(long id) {
	
			repository.deleteByBillingId(id);

		

	}

}
