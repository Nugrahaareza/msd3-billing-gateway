package com.springgateway.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springgateway.entity.BillingSubscriptionLog;
import com.springgateway.entity.Privilege;
import com.springgateway.entity.Role;

import com.springgateway.model.MyUserDetails;
import com.springgateway.model.UserModel;
import com.springgateway.repository.BillingSubscriptionLogRepository;
import com.springgateway.repository.UserRepository;

@Service
public class BillingSubscriptionLogService {
	@Autowired
	private BillingSubscriptionLogRepository repository;

	public List<BillingSubscriptionLog> getAll() {
		return repository.findAll();
	}

	
	public BillingSubscriptionLog saveData(BillingSubscriptionLog user) {

		return repository.save(user);
	}

	public List<BillingSubscriptionLog> saveDatas(List<BillingSubscriptionLog> user) {
		return repository.saveAll(user);
	}


	public List<BillingSubscriptionLog> getByInvoiceId(Long id) {
		
		return repository.findAllByinvoiceId(id);
	}

	
	
	
	
	
	
	
	
	
	
	
	

	
}
