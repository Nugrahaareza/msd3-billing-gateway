package com.springgateway.util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.springgateway.entity.AuthorityPage;
import com.springgateway.entity.Pages;
import com.springgateway.entity.Privilege;
import com.springgateway.entity.Role;
import com.springgateway.entity.SystemProperties;
import com.springgateway.entity.UserEntity;
import com.springgateway.repository.AuthorityPageRepository;
import com.springgateway.repository.PageRepository;
import com.springgateway.repository.PrivilegeRepository;
import com.springgateway.repository.RoleRepository;
import com.springgateway.repository.SystemPropertiesRepository;
import com.springgateway.repository.UserRepository;
import com.springgateway.service.PageService;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

	boolean alreadySetup = false;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PrivilegeRepository privilegeRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SystemPropertiesRepository sysPropsRepo;

	@Autowired
	private PageRepository pageRepo;

	@Autowired
	private AuthorityPageRepository apRepo;

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (alreadySetup)
			return;
		Privilege readPrivilege = createPrivilegeIfNotFound("READ_PRIVILEGE");
		Privilege writePrivilege = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
		List<Privilege> adminPrivileges = Arrays.asList(readPrivilege, writePrivilege);
		createRoleIfNotFound("SysAdmin", adminPrivileges);
		createRoleIfNotFound("Billing Officer", adminPrivileges);
		createRoleIfNotFound("Billing Manager", adminPrivileges);
		createRoleIfNotFound("Security Officer", Arrays.asList(readPrivilege));
		createRoleIfNotFound("Security Manager", Arrays.asList(readPrivilege));
		createPropIfNotFound("JWTTime", "900000", "JWT token lifetime");
		createPropIfNotFound("PasswordAttempt", "3", "User Login max attempt");

		createPageIfNotFound("/api/logjob", "Job Trail", "/jobTrail");
		createPageIfNotFound("/api/billingschemetemplate", "Billing Package", "/billingscheme");
		createPageIfNotFound("/api/organization", "Customer", "/organization");
		createPageIfNotFound("/api/user", "User List", "/userList");
		createPageIfNotFound("/api/auditTrail", "Audit Trail", "/auditTrail");
		createPageIfNotFound("/api/config", "Role Config", "/roleconfig");
		createPageIfNotFound("/api/invoice", "Invoice", "/invoice");
		createPageIfNotFound("/api/properties", "System Properties", "/systemProperties");
		createPageIfNotFound("/api/syncjob", "Run Job", "/syncJob");
		createPageIfNotFound("/api/paymentTrail", "Payment Trail", "/paymentTrail");
		createAdmin();
		createSecuritys();
		createAuthorityPage();
		alreadySetup = true;
	}

	private void createAuthorityPage() {
		Role admin = roleRepository.findByRoleName("SysAdmin");

		Role secoffc = roleRepository.findByRoleName("Security Officer");
		Role secmene = roleRepository.findByRoleName("Security Manager");
		
		Role billoffc = roleRepository.findByRoleName("Billing Officer");
		Role billmene = roleRepository.findByRoleName("Billing Manager");
	
		
		Pages pageLogjob = pageRepo.findAllByGroupPage("/api/logjob");
		Pages pageAuditTrail = pageRepo.findAllByGroupPage("/api/auditTrail");
		Pages pageProp = pageRepo.findAllByGroupPage("/api/properties");
		Pages pageSyncJob = pageRepo.findAllByGroupPage("/api/syncjob");
		Pages[] pAdmin = { pageLogjob, pageAuditTrail, pageSyncJob, pageProp };

		Pages pagePack = pageRepo.findAllByGroupPage("/api/billingschemetemplate");
		Pages pageInvoice = pageRepo.findAllByGroupPage("/api/invoice");
		Pages pageOrg = pageRepo.findAllByGroupPage("/api/organization");
		Pages[] pBill = { pagePack, pageInvoice, pageOrg };
		
		Pages pageConfig = pageRepo.findAllByGroupPage("/api/config");
		Pages pageUser = pageRepo.findAllByGroupPage("/api/user");
		Pages[] pSec = { pageUser, pageConfig };

		if (admin != null) {
			ArrayList<AuthorityPage> lrl = new ArrayList<AuthorityPage>();
			if (apRepo.findByRolePk(admin.getId()).size() == 0) {
				for (Pages pga : pAdmin) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(pga.getId());
					ap.setRolePk(admin.getId());
					lrl.add(ap);
				}
			}
			apRepo.saveAll(lrl);
		}
		
		
		if (secoffc != null) {
			ArrayList<AuthorityPage> lrl = new ArrayList<AuthorityPage>();
			if (apRepo.findByRolePk(secoffc.getId()).size() == 0) {
				for (Pages pga : pSec) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(pga.getId());
					ap.setRolePk(secoffc.getId());
					lrl.add(ap);
				}
			}
			apRepo.saveAll(lrl);
		}

		if (secmene != null) {
			ArrayList<AuthorityPage> lrl = new ArrayList<AuthorityPage>();
			if (apRepo.findByRolePk(secmene.getId()).size() == 0) {
				for (Pages pga : pSec) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(pga.getId());
					ap.setRolePk(secmene.getId());
					lrl.add(ap);
				}
			}
			apRepo.saveAll(lrl);
		}

		
		
		if (billoffc != null) {
			ArrayList<AuthorityPage> lrl = new ArrayList<AuthorityPage>();
			if (apRepo.findByRolePk(billoffc.getId()).size() == 0) {
				for (Pages pga :  pBill) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(pga.getId());
					ap.setRolePk(billoffc.getId());
					lrl.add(ap);
				}
			}
			apRepo.saveAll(lrl);
		}

		
		if (billmene != null) {
			ArrayList<AuthorityPage> lrl = new ArrayList<AuthorityPage>();
			if (apRepo.findByRolePk(billmene.getId()).size() == 0) {
				for (Pages pga :  pBill) {
					AuthorityPage ap = new AuthorityPage();
					ap.setPagesPk(pga.getId());
					ap.setRolePk(billmene.getId());
					lrl.add(ap);
				}
			}
			apRepo.saveAll(lrl);
		}

		
		
		
	}

	@Transactional
	private void createAdmin() {
		if (userRepository.findByUsername("Admin") == null) {
			Role adminRole = roleRepository.findByRoleName("SysAdmin");
			UserEntity user = new UserEntity();
			user.setUsername("admin");
			user.setPassword(passwordEncoder.encode("password"));
			user.setEmail("test@test.com");
			user.setRoles(Arrays.asList(adminRole));
			user.setActive(true);
			user.setApprovalStatus(1);
			userRepository.save(user);
		}

	}

	@Transactional
	private void createPageIfNotFound(String group, String label, String path) {
		if (pageRepo.findAllByGroupPage(group) == null) {
			Pages pg = new Pages();
			pg.setGroupPage(group);
			pg.setLabel(label);
			pg.setPath(path);
			pageRepo.save(pg);
		}

	}

	@Transactional
	private void createSecuritys() {
		if (userRepository.findByUsername("security_officer") == null) {
			Role adminRole = roleRepository.findByRoleName("Security Officer");
			UserEntity user = new UserEntity();
			user.setUsername("security_officer");
			user.setPassword(passwordEncoder.encode("P@ssw0rd.1"));
			user.setEmail("drianhartanta@gmail.com");
			user.setRoles(Arrays.asList(adminRole));
			user.setActive(true);
			user.setApprovalStatus(1);
			userRepository.save(user);
			
		}
		if (userRepository.findByUsername("security_manager") == null) {
			Role adminRole = roleRepository.findByRoleName("Security Manager");
			UserEntity user = new UserEntity();
			user.setUsername("security_manager");
			user.setPassword(passwordEncoder.encode("P@ssw0rd.1"));
			user.setEmail("drianhartanta@gmail.com");
			user.setRoles(Arrays.asList(adminRole));
			user.setApprovalStatus(1);
			user.setActive(true);
			userRepository.save(user);
		}
		//createAuthorityPage();
	}

	@Transactional
	private void createPropIfNotFound(String name, String value, String desc) {
		if (sysPropsRepo.findByPropertyName(name) == null) {
			SystemProperties sp = new SystemProperties();
			sp.setPropertyName(name);
			LocalDateTime lastModified = LocalDateTime.now();
			sp.setLastModified(lastModified);
			sp.setPropertyDescription(desc);
			sp.setPropertyValue(value);
			sp.setLastModifiedBy(null);
			sysPropsRepo.save(sp);
		}

	}

	@Transactional
	Privilege createPrivilegeIfNotFound(String name) {
		Privilege privilege = privilegeRepository.findByName(name);
		if (privilege == null) {
			privilege = new Privilege();
			privilege.setName(name);
			privilegeRepository.save(privilege);
		}
		return privilege;
	}

	@Transactional
	Role createRoleIfNotFound(String name, Collection<Privilege> privileges) {
		Role role = roleRepository.findByRoleName(name);
		if (role == null) {
			Role newRole = new Role();
			newRole.setRoleName(name);
			newRole.setPrivileges(privileges);
			newRole.setApprovalStatus(1);
			roleRepository.save(newRole);
		}
		return role;
	}

}
