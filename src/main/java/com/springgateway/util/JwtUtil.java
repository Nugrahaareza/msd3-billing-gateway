package com.springgateway.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.springgateway.entity.Role;
import com.springgateway.entity.UserEntity;
import com.springgateway.repository.UserRepository;
import com.springgateway.service.SystemPropertiesService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import static java.util.concurrent.TimeUnit.*;

@Service
public class JwtUtil {

	//@Value("${token.email.addtionalTime}")
	//String additionalTime;

	//@Value("${token.jwt.lifetime}")
	//private String tokenLifeTime;

	@Value("${token.secret.key}")
	private String secret;

	@Autowired
	SystemPropertiesService spService;
	
	@Autowired
	private UserRepository repository;
	private String SECRET_KEY = "secret";

	public String extractUsername(String token) {
		if (extractClaim(token, Claims::getSubject) != null) {
			return extractClaim(token, Claims::getSubject);
		} else {
			return null;
		}
	}

	public Date extractExpiration(String token) {
		if (extractClaim(token, Claims::getExpiration) != null) {
			return extractClaim(token, Claims::getExpiration);
		} else {
			return null;
		}
	}

	public Claims decodeToken(String token) {
		try {
			Claims claimRes = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
			return claimRes;
		} catch (ExpiredJwtException e) {
		    System.out.println(" Token expired ");
		    return null;
		}
	}

	public String parseAuthHeader(String token) {
		if (token != null) {
			return token.substring(7);
		} else {
			return null;
		}
	}
	
	

	public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = extractAllClaims(token);
		if (claims != null) {
			return claimsResolver.apply(claims);
		} else {
			return null;
		}
	}

	

	private Claims extractAllClaims(String token) {
		try {
			return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
		} catch (ExpiredJwtException e) {
			System.out.println(" Token expired ");
			return null;
		}

	}

	public Boolean isTokenExpiredSoon(String token) {
		String intervalRTok = spService.getAllByName("IntervalRTOK");
		long MAX_DURATION = MILLISECONDS.convert(Integer.valueOf(intervalRTok), MINUTES);
		long duration = extractExpiration(token).getTime() - new Date().getTime();
		if(duration <= MAX_DURATION) {
			return true;
		}else {
		return false;
		}
	}
	
	private Boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		UserEntity user = repository.findByUsername(userDetails.getUsername());
		return createToken(claims, userDetails.getUsername(), Roles(user.getRoles()), userDetails.getAuthorities());
	}

	public String generateToken(UserEntity user) {
		Map<String, Object> claims = new HashMap<>();
		return createToken(claims, user.getId(), null, null);
	}

	public String generateForceResetPass(UserEntity user) {
		Map<String, Object> claims = new HashMap<>();
		return createTokenForceReset(claims, user.getUsername(), null, null);
	}
	
	private String createTokenForceReset(Map<String, Object> claims, String username, Object roles, Object collection) {
		{
			String additionalTime = spService.getAllByName("ForceResetPassToken");
			return Jwts.builder().setClaims(claims).setSubject(String.valueOf(username))
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + Integer.parseInt(additionalTime)))
					.signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
		}
	}

	
	private String createToken(Map<String, Object> claims, long id, Object roles, Object collection) {
		{
			String additionalTime = spService.getAllByName("ResetPasswordToken");
			return Jwts.builder().setClaims(claims).setSubject(String.valueOf(id))
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + Integer.parseInt(additionalTime)))
					.signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
		}
	}

	private List<String> Roles(Collection<Role> role) {
		List<Role> data = new ArrayList<Role>(role);
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < role.size(); i++) {
			list.add(data.get(i).getRoleName());
		}
		return list;

	}

	private String createToken(Map<String, Object> claims, String subject, Collection<?> roles,
			Collection<?> collection) {
		String additionalTime = spService.getAllByName("JWTTime");
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.claim("role", roles).claim("privilege", collection)
				.setExpiration(new Date(System.currentTimeMillis() + Integer.parseInt(additionalTime)))
				.signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
	}

	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = extractUsername(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}



}