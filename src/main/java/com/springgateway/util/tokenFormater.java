package com.springgateway.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.springgateway.service.LoggedUserService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class tokenFormater {

	private LoggedUserService loggedUserService;

	public String getUsername(HttpServletRequest request) {
		JwtUtil jwtUtil = new JwtUtil();
		final String authorizationHeader = request.getHeader("Authorization");
		String username = null;
		String jwt = null;
		jwt = authorizationHeader.substring(7);
	
		username = jwtUtil.extractUsername(jwt);
		return username;
	}

	public String getUsername(String tokenReq) {
		JwtUtil jwtUtil = new JwtUtil();
		//final String authorizationHeader = request.getHeader("Authorization");
		System.out.println(tokenReq);
		String username = null;
	
		username =jwtUtil.extractUsername(tokenReq);
		return username;
		//return null;
	}
	@SuppressWarnings("unchecked")
	public String getRole(String token) {
		JwtUtil jwtUtil = new JwtUtil();

		String role = null;

		Claims claim = jwtUtil.decodeToken(token);
		if (claim != null) {
			List<String> str = (List<String>) claim.get("role");
			// ArrayList<?> str = (ArrayList<?>) claim.get("role");
			// LinkedHashMap<String,String> authority = new LinkedHashMap<String,String>();
			// authority= (LinkedHashMap<String, String>) str.get(0);
			// role =authority.get("authority");
			role = str.get(0);
			return role;
		} else {
			return null;
		}
	}

	public Boolean haveAuthority(String token, List roles) {
		JwtUtil tokenUtil = new JwtUtil();

		tokenFormater userFormater = new tokenFormater();

		String role = userFormater.getRole(token);
		if (!roles.contains(role)) {
			return false;
		}
		return true;
	}

	public String getUsernameWithToken(String token) {
		JwtUtil jwtUtil = new JwtUtil();
		String username = null;
		String jwt = null;
		jwt = token.substring(7);
		username = jwtUtil.extractUsername(jwt);
		return username;
	}

}
