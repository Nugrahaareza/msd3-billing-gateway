package com.springgateway.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

public class Utility {

	@Autowired
	private JwtUtil jwtUtil;
	
	public String getUsername(HttpServletRequest request) {
		final String authorizationHeader = request.getHeader("Authorization");
		String username = null;
		String jwt = null;
		jwt = authorizationHeader.substring(7);
		username = jwtUtil.extractUsername(jwt);
		return username;
	}
}
