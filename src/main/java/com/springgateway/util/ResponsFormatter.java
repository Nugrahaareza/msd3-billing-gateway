package com.springgateway.util;

import java.util.*;

import com.springgateway.util.Constants;

public class ResponsFormatter {

	public Map<String, Object> dataString(String data, String message) {
        Map<String, Object> response = new HashMap<>();
        response.put(Constants.CODE, 200);
        response.put(Constants.DATA, data);
        response.put(Constants.MESSAGE, message);
        response.put(Constants.TIME_STAMP, new Date());

        return response;
    }
	
    public Map<String, Object> isValid(Object data, String message) {
        Map<String, Object> response = new HashMap<>();

        response.put(Constants.CODE, 200);
        response.put(Constants.DATA, data);
        response.put(Constants.MESSAGE, message);
        response.put(Constants.TIME_STAMP, new Date());

        return response;
    }
    

    public Map<String, Object> isValidList(List data, String message) {
        Map<String, Object> response = new HashMap<>();

        response.put(Constants.CODE, 200);
        response.put(Constants.DATA, data);
        response.put(Constants.MESSAGE, message);
        response.put(Constants.TIME_STAMP, new Date());

        return response;
    }

    public Map<String, Object> isNotValid(int code, String message) {
        Map<String, Object> response = new HashMap<>();

        response.put(Constants.CODE, code);
        response.put(Constants.MESSAGE, message);
        response.put(Constants.TIME_STAMP, new Date());

        return response;
    }
}
