package com.springgateway.util;

import org.springframework.beans.factory.annotation.Autowired;

import com.springgateway.entity.AuditTrail;
import com.springgateway.service.AuditTrailService;

public class AuditTrailUtil {
	@Autowired
    private AuditTrailService auditTrailService;
	
	public void Audit(AuditTrail auditTrail) {
		auditTrailService.saveData(auditTrail);
	}
}
