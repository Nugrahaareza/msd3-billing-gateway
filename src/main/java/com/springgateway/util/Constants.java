package com.springgateway.util;



public class Constants {
    public static final String LIST = "list";
    public static final String TOTAL = "total";
    public static final String DATA = "data";

    public static final String META = "meta";
    public static String MESSAGE = "message"; //Menampilkan pesan di API
    public static String CODE = "code";
    public static String TIME_STAMP = "timestamp";
}
