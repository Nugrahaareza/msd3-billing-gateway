package com.springgateway.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.springgateway.entity.LoggedUser;
import com.springgateway.service.LoggedUserService;
import com.springgateway.service.UserService;

@Service
public class RefreshTokenGenerator {
	@Autowired
	private JwtUtil jwtTokenUtil;
	@Autowired
	private UserService userService;
	@Autowired
	private LoggedUserService loggedUserService;

	public String refreshTokenGenerator(HttpServletRequest request) {
		final String authorizationHeader = request.getHeader("Authorization");
		String jwt = null;
		String username = null;

		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			jwt = authorizationHeader.substring(7);
			if (jwt != null) {
				username = jwtTokenUtil.extractUsername(jwt);
			}
		}

		System.out.println(jwtTokenUtil.isTokenExpiredSoon(jwt));
		if (jwtTokenUtil.isTokenExpiredSoon(jwt)) {
			if (username != null) {
				UserDetails userDetails = userService.loadUserByUsername(username);
				String rTok = jwtTokenUtil.generateToken(userDetails);
				// token = jwtTokenUtil.parseAuthHeader(request.getHeader("Authorization"));
				LoggedUser lu = new LoggedUser();
				lu.setToken(rTok);
				loggedUserService.saveData(lu);
				//loggedUserService.delete(jwt);
				return rTok;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}
}
