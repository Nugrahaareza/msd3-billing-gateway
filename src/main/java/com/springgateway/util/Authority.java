package com.springgateway.util;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.springgateway.service.AuthorityPageService;
import com.springgateway.service.LoggedUserService;

@Service
public class Authority {
	@Autowired
	private LoggedUserService loggedUserService;

	@Autowired
	private AuthorityPageService authorityService;

	public String[] getRoleList(String pageGroup) {
		return authorityService.getRolesGroupAPI(pageGroup);
	}

	public int isAuthority(HttpServletRequest request, String groupApi) {
		JwtUtil tokenUtil = new JwtUtil();
		tokenFormater tf = new tokenFormater();
		int hasAuthority = 2;

		String token = request.getHeader("Authorization");
		token = tokenUtil.parseAuthHeader(token);

		String role = tf.getRole(token);
		String[] roleList = getRoleList(groupApi);

		boolean isLoggedIn = loggedUserService.getByToken(token);
		// if (isLoggedIn) {
		if (!Arrays.asList(roleList).contains(role)) {
			hasAuthority = 1;
		}
		// }else if(!isLoggedIn) {
		// return hasAuthority;
		// }

		return hasAuthority;
	}
}
