package com.springgateway.mail;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.http.HttpMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.springgateway.entity.Invoice;
import com.springgateway.entity.Organization;
import com.springgateway.entity.Subscription;
import com.springgateway.entity.UserEntity;
import com.springgateway.service.OrganizationService;
import com.springgateway.service.SystemPropertiesService;
import com.springgateway.service.UserService;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

@Component
public class EmailServiceImpl {

	//@Value("${spring.mail.sender}")
	

	@Autowired
	private JavaMailSender emailSender;
	
	@Autowired
	private  SystemPropertiesService sysPropsService;

	
	@Autowired
	private UserService userService;

	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private FreeMarkerConfigurer freemarkerConfigurer;

	public void sendSimpleMessage(Subscription data) {
		System.out.println(data);
		SimpleMailMessage smm = new SimpleMailMessage();
		UserEntity user = userService.getAllEmailByChecker();
		smm.setTo(user.getEmail());
		smm.setSubject("Billing Not Set Notification");
		smm.setText("Subscription of " + data.getAppName() + " request to be set");
		emailSender.send(smm);
	}

	public void sendMessageToCustomer(Organization reqBody) {
		// System.out.println(data);
		SimpleMailMessage smm = new SimpleMailMessage();
		Organization user = organizationService.getAllById(reqBody.getOrganizationId());
		smm.setTo(user.getOwnerEmail());
		smm.setSubject("Billing Payment  Notification");
		smm.setText("Please pay your bill");
		emailSender.send(smm);

	}

	public void sendSimpleMessage(String jwt, String mailTarget) throws MessagingException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		Template freemarkerTemplate = freemarkerConfigurer.createConfiguration().getTemplate("template-freemarker.ftl");
		InetAddress IP = InetAddress.getLocalHost();
		String url = "http://" + IP.getHostAddress() + "/mbs-ui/#/resetPassword/" + jwt;
		//String sender = sysPropsService.getAllByName("simas.mail.sender.system");
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("link", url);

		String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateModel);
	    helper.setText(htmlBody, true);
		//helper.setFrom(sender);	
		helper.setTo(mailTarget);
		helper.setSubject("Request to reset password");
		emailSender.send(message);
	}

	public void sendUserPass(UserEntity user, String pass)  throws MessagingException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		
		
		
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		Template freemarkerTemplate = freemarkerConfigurer.createConfiguration().getTemplate("template-new-user.ftl");
		//InetAddress IP = InetAddress.getLocalHost();
		//String url = "http://" + IP.getHostAddress() + "/mbs-ui/#/resetPassword/" + jwt;

		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("username", user.getUsername());
		templateModel.put("password", pass);
		//String sender = sysPropsService.getAllByName("simas.mail.sender.system");
		String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateModel);
	    helper.setText(htmlBody, true);
		//helper.setFrom(sender);	
		helper.setTo(user.getEmail());
		helper.setSubject("Request to reset password");
		emailSender.send(message);
		
		
		/*
		//String sender = sysPropsService.getAllByName("simas.mail.sender.system") ;
		SimpleMailMessage smm = new SimpleMailMessage();
		InetAddress IP = InetAddress.getLocalHost();
		//smm.setFrom(sender);
		smm.setTo(user.getEmail());
		smm.setSubject("New user for billing management");
		smm.setText("Hi,you user is created with username:  " + user.getUsername() + " and password: "
				+ pass);
		emailSender.send(smm);
		*/
	}
}