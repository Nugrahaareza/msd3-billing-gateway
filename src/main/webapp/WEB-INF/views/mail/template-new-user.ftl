<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
    <p>Hi ${username},</p>
     <br/>
     
    <p>Your Account has been available. Here are your account information: </p>
	<p>Username: ${username}</p>
	<p>Password: ${password}</p>
	<br/>
    <p>After Log In for the first time, you will be asked to change the password.</p>
	<p>Please make sure your password is maintained well and is only used by yourself.</p>
  	  <br/>
  	<p>Regards, </p>
  	<p>Sinarmas</p>
  </body>
</html>
